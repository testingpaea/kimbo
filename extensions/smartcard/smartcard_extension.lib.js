const ScStore = require('kimbo/scenarioStore');
const {execSync} = require('child_process');

var SmartCardManager=class SmartCardManager{
	constructor(){
		var self=this;
		self.enabled=false;
	}
	execOpensc(){
		var result="";
		var bException=false;
		try{
			result = execSync("opensc-tool -a");
		} catch(error){
			debugger;
			console.log(error);
			result=error.stderr.toString().trim();
			bException=true;
		}
		if (!bException){ 
			result="SmartCard Driver Loaded and Card is in slot.";
		}
		var sResult=result.toString('utf8').trim();
		return sResult;
	}
	isLoadedSmartCard(){
		var result=this.execOpensc();
		if (result=="No smart card readers found."){
			console.log("SmartCard: pcscd is not loaded");
			return false;   
		}
		return true;
	}
	isCardPresent(){
		var result=this.execOpensc();
		if (result=="SmartCard Driver Loaded and Card is in slot."){
			return true;   
		}
		return false;
	}
	async enableSmartCard(){
		if (ScStore.isExtensionLoaded("exclusions")){
			await ScStore.getExclusions().startLocalExclusionSection();
		}
		if (!this.isLoadedSmartCard()) {
			console.log("SmartCard: loading smart card driver");
			var result= execSync ('echo '+process.env.ENV_PASSWORD+' | sudo -S service pcscd start');
			this.bWithSmartcard=true;
		}
	}
	async disableSmartCard(){
		if (ScStore.isExtensionLoaded("exclusions")){
			await ScStore.getExclusions().startLocalExclusionSection();
		}
		if (this.isLoadedSmartCard()) {
			console.log("SmartCard: unloading smart card driver");
			var result= execSync ( 'echo '+process.env.ENV_PASSWORD+' | sudo -S pkill pcscd');
			this.bWithSmartcard=false;
		}
	}

}

var smartcardManager=new SmartCardManager();
module.exports=smartcardManager;