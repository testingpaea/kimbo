const smartcardManager = require('./smartcard_extension.lib');
const ScStore = require('kimbo/scenarioStore');


const stepDefinitions = [
  { stepMethod: 'Given', stepPattern: 'disable smartcard and reload browser', 
	stepFunction: async function(){
	  await smartcardManager.disableSmartCard();   //reload browser with smartcard disabled
	  ScStore.getSelenium().reloadBrowser();
	}
  },{ stepMethod: 'Given', stepPattern: 'enable smartcard and reload browser', 
    stepInfo:'info about the step',
	stepFunction: async function(){
	  debugger;
	  await smartcardManager.enableSmartCard();   //reload browser with smartcard enabled
	  ScStore.getSelenium().reloadBrowser();
	}
  },{ stepMethod: 'Given', stepPattern: 'enable smartcard', 
	stepFunction: async function(){
	  await smartcardManager.enableSmartCard();   //reload browser with smartcard enabled
	}
  },{ stepMethod: 'Given', stepPattern: 'free smartcard system lock', 
	stepFunction: async function(){
		if (ScStore.isExtensionLoaded("exclusions")){
			await ScStore.getExclusions().endLocalExclusionSection();
		}
	}
	
  },{ stepMethod: 'Given', stepPattern: 'disable smartcard', 
	stepFunction: async function(){
	  await smartcardManager.disableSmartCard();   //reload browser with smartcard enabled
	}
  },{ stepMethod: 'Given', stepPattern: 'end smartcard use section', 
	stepFunction: async function(){
		if (ScStore.isExtensionLoaded("exclusions")){
			await ScStore.getExclusions().endLocalExclusionSection();
		}
	}
  }
];
module.exports=stepDefinitions;