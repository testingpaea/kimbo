const interfaceManager = require('./interfaceManager');
const interfaceFunc = require('./interfaceFunctions');

interfaceFunc(true);

module.exports = interfaceManager;