const ScStore = require('../../scenarioStore');
debugger;
ScStore.enableExtension("storable");
const AppInterface= require('./AppInterface');
const InterfaceElement= require('./AppInterfaceElement');
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;


var interfaceManager=class interfaceManager extends StorableObjectListClass{
	constructor(){
		super();
		var self=this;
		self.addIndexField("id");
		this.enabled=false;
		this.actualInterface="";
		ScStore.mapNoRootTags.set("@Interface","Interface");
	}
	setActual(actualInterface){
		console.log("Setting Actual interface:"+actualInterface.id+" previous was:"+this.actualInterface);
		this.actualInterface=actualInterface;
	}
	getActual(){
		console.log("get Actual interface:"+this.actualInterface.id);
		return this.actualInterface;
	}
	newInstance(tags,jsonInterface){
		var self=this;
		var appInterface;
//		console.log("new InterfaceInstance:"+JSON.stringify(jsonInterface));
		if (typeof jsonInterface.interfaceType==="undefined"){
			console.log("New Interface. No Type");
			appInterface= new AppInterface(self,tags);
		} else {
	//		console.log("New Interface. Type:"+jsonInterface.interfaceType);
			var oManager=ScStore.getManager(jsonInterface.interfaceType); // the type is a extension
			appInterface=oManager.newInterface(self,tags);
		}
		self.actual=appInterface;
		appInterface.loadFromJson(jsonInterface);
		this.enabled=true;
		return appInterface;
	}
	
	async listInteractiveElements(extension){
		var theExtension=ScStore.getManager(extension);
		await theExtension.listInteractiveElements();
	}
}
var interfaceManager=new interfaceManager();
storableObjectManager["loadInterfaceListManager"]=function(){
	return interfaceManager;
};

module.exports={interfaceManager,AppInterface,InterfaceElement};
