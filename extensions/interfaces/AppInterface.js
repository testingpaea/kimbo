const ScStore = require('../../scenarioStore');
const InterfaceElement = require('./AppInterfaceElement');
var StorableObjectClass=ScStore.getStorable().StorableObjectClass;
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;


var AppInterface= class AppInterface extends StorableObjectClass{
	constructor(manager,tags){
		super("interface",tags);
		var self=this;
		self.id="";
		self.interfaceType="";
		self.testLoadedInfo="";
		self.elements=new Map();
		self.elementsOrderedByFocus=[];
		self.focusDirect=false;
		self.focused="";
		self.initialFocusedElement="";
		self.relatedInterfaces=[];
		self.extendsInterface="";
		self.manager=manager;
	}
	newElement(id,extension){
		var self=this;
		var auxType=extension;
		if (typeof auxType==="undefined"){
			if (self.interfaceType===""){
				return new InterfaceElement(id,this);
			} else {
				auxType=self.interfaceType;
			}
		} 
		console.log("New Element:"+auxType);
		var oManager=ScStore.getManager(auxType);
		console.log("Manager:"+JSON.stringify(oManager));
		return oManager.newElement(id);
	}
	async existsElement(elmName){
		var self=this;
		if (self.elements.has(elmName)){
			var oElem=await self.getElement(elmName);
			var bExists=await oElem.exists();
			return bExists;
		} 
		return false; 
	}
	async hasElement(elmName){
		var self=this;
		if (self.elements.has(elmName)){
			return true;
		}
		//console.log(self.extendsInterface);
		if ((self.extendsInterface==="")||(typeof self.extendsInterface==="undefined")){
			return false;
		}
		var itfAux=self.manager.get(self.extendsInterface);
		return await itfAux.hasElement(elmName);
		
	}
	async getElement(elmName){
		var self=this;
		if (self.elements.has(elmName)){
			return self.elements.get(elmName);
		}
		//console.log(self.extendsInterface);
		if ((self.extendsInterface==="")||(typeof self.extendsInterface==="undefined")){
			throw new Error("The element '"+ elmName+ "' does not exist in "+self.id+" interface... or parents");
		}
		var itfAux=self.manager.get(self.extendsInterface);
		return await itfAux.getElement(elmName);
	}
	loadFromJson(jsonInterface){
		var self=this;
		debugger;
		self.id=jsonInterface.interfaceId;
		self.interfaceType=jsonInterface.type;
		self.extendsInterface=jsonInterface.extendsInterface;
		
		if (typeof jsonInterface.elements!=="undefined"){
			for (const element of jsonInterface.elements){
				var interfaceElement=self.newElement(element.id,self.interfaceType);
				interfaceElement.loadFromJson(element);
				self.elements.set(element.id,interfaceElement);
				if (interfaceElement.focusable){
					if (interfaceElement.focused){
						self.focused=interfaceElement;
						console.log(self.id+" Focused Marked:"+interfaceElement.id);
//					} else {
//						console.log(self.id+" NOT Focused Marked:"+interfaceElement.id);
					}
					self.elementsOrderedByFocus.push(interfaceElement);
				}
			};
		}
		if (typeof jsonInterface.relatedInterfaces!=="undefined"){
			self.relatedInterfaces=jsonInterface.relatedInterfaces;
		}
		
		if (typeof jsonInterface.tags!=="undefined"){
			self.tags=jsonInterface.tags;
		}
		if (typeof jsonInterface.testLoaded!=="undefined"){
			var testInfo={waitsecs:0,element:jsonInterface.testLoaded.element,testWay:[]};
			if (typeof jsonInterface.testLoaded.testWay==="string"){
				testInfo.testWay.push(jsonInterface.testLoaded.testWay);
			} else {
				testInfo.testWay=jsonInterface.testLoaded.testWay;
			}
			if (typeof jsonInterface.testLoaded.waitsecs!=="undefined"){
				testInfo.waitsecs=jsonInterface.testLoaded.waitsecs;
			}
			self.testLoadedInfo=testInfo;
		}
		self.elementsOrderedByFocus.sort(function(a,b){
			//console.log("Ordering " + a.id + " " + a.focusOrder+ " <-> " + b.id + " " + b.focusOrder);
			if (a.focusOrder<b.focusOrder){
				return -1;
			} else if (a.focusOrder>b.focusOrder){ 
				return 1;
			} else {
				return 0;
			}
		});
		var elem=self.elementsOrderedByFocus[0];
		for (var i=0;i<self.elementsOrderedByFocus.length;i++){
			var elem=self.elementsOrderedByFocus[i];
			elem.focusArrayIndex=i;
			//console.log("Elem:"+elem.id+" -> " + elem.focusArrayIndex);
		}
		elem=self.focused;
		if (typeof elem!=="undefined"){
			console.log(self.id+" Focused Elem[0]:"+elem.id+" -> " + elem.focusArrayIndex);
		}
	}
	generateInterfaceJsonFromWebDriverWindow(wdWindow){
	}
	findElement(elementId){
	}	
	async isLoaded(bResetFocus){
		var self=this;
		if (self.testLoadedInfo===""){
			return false;
		}
		var bRefocus=(typeof bResetFocus!=="undefined"?bResetFocus:true);
		if (self.initialFocusedElement==""){
			var firstFocusElem="";
			if (self.elementsOrderedByFocus.length){
				firstFocusElem=self.elementsOrderedByFocus[0];
				self.initialFocusedElement=firstFocusElem;
			}
		}
		if (self.focused!==""){
			self.focused.focused=false;
		}
		self.focused=self.initialFocusedElement;
		if (self.focused!==""){
			self.focused.focused=false;
		}
		var bFound=false;
		console.log("Load Info:"+JSON.stringify(self.testLoadedInfo));
		if ((typeof self.testLoadedInfo.waitsecs!=="undefined")&&(self.testLoadedInfo.waitsecs>0)){
			console.log("Load Info says to Waiting  "+self.testLoadedInfo.waitsecs+" secs");
			await ScStore.waitSecs(self.testLoadedInfo.waitsecs);
		}
		var elmName=self.testLoadedInfo.element;
		var oElem=await self.getElement(elmName);
		bFound=await oElem.isLoaded(self.testLoadedInfo.testWay);
		console.log("Check the Load of element "+self.id+" returns:"+bFound/*+ " Manager:"+self.manager+" in json:"+JSON.stringify(self.manager)*/);
		return bFound;
		
	}
	async waitForLoad(bResetFocus){
		var self=this;
		if (self.testLoadedInfo===""){
			return;
		}
		var bFound=false;
		while (!bFound){
			bFound=await self.isLoaded(bResetFocus);
			console.log("Wait For Load of element "+self.id+" returns:"+bFound/*+ " Manager:"+self.manager+" in json:"+JSON.stringify(self.manager)*/);
		}
		ScStore.getInterfaces().setActual(self);
		return bFound;
	}
	async waitForFullLoad(){
		var self=this;
		
	}
	async changeFocus(elmId){
		var self=this;
		var tgtElm=await self.getElement(elmId);
		if (self.focused!==""){
			self.focused.focused=false;
		}
		self.focused=tgtElm;
		tgtElm.focused=true;
	}
	async clickOn(elmName,nonBlocking=false){
		var self=this;
		console.log("Interface:"+self.id+" ClickOn:"+elmName);
		var oElem=await self.getElement(elmName);
		if ((typeof oElem.focusable!=="undefined")&&(!oElem.focusable)){
			console.log("no focusable");
		} else {
			oElem=await self.setFocus(elmName);
		}

		//console.log("Element:"+JSON.stringify(oElem));
		if ((typeof nonBlocking!=="undefined")&&(nonBlocking)){
			oElem.click();
		} else {
			await oElem.click();
		}
	}
	async clickOnItemNumber(elmName,nItem,nonBlocking=false){
		debugger;
		var self=this;
		console.log("Interface:"+self.id+" clickOnItemNumber:"+elmName+"["+nItem+"]");
		var oElem=await self.getElement(elmName);
		if ((typeof nonBlocking!=="undefined")&&(nonBlocking)){
			oElem.clickOnItemNumber(nItem);
		} else {
			await oElem.clickOnItemNumber(nItem);
		}
	}	
	async selectOptionNumberOf(itmNumber,elmName){
		var self=this;
		var oElem=await self.setFocus(elmName,false);
		await oElem.selectOptionNumber(itmNumber);
	}
	async selectOptionValueOf(optionValue,elmName){
		var self=this;
		var oElem=await self.setFocus(elmName,false);
		await oElem.selectOptionValue(optionValue);
	}
	async selectOptionTextOf(optionText,elmName){
		var self=this;
		var oElem=await self.setFocus(elmName,false);
		await oElem.selectOptionText(optionText);
	}
	
	async pressArrow(idArrow,nTimes,elmName){
		var self=this;
		debugger;
		console.log("Interface pressArrows");
		var oElem=await self.setFocus(elmName);
		await oElem.pressArrow(idArrow,nTimes);
	}
	async pressEnter(nTimes,elmName){
		var self=this;
		debugger;
		console.log("Interface pressEnter");
		var oElem=await self.setFocus(elmName);
		await oElem.pressEnter(nTimes);
	}
	async writeText(sText,elmName){
		var self=this;
		debugger;
		console.log("Interface writeText");
		var oElem=await self.setFocus(elmName);
		await oElem.writeText(sText);
	}
	async clear(elmName){
		var self=this;
		debugger;
		console.log("Interface writeText");
		var oElem=await self.setFocus(elmName);
		await oElem.clear();
	}
	
	async getValue(elmName){
		var self=this;
		debugger;
		console.log("Interface getValueText");
		var oElem=await self.getElement(elmName);
		var vValue=await oElem.getValue();
		return vValue;	
	}
	async checkClass(elmName,className){
		var self=this;
		debugger;
		console.log("Interface checkClass");
		var oElem=await self.getElement(elmName);
		var vValue=await oElem.checkClass(className);
		return vValue;	
	}

	async isEnabled(elmName){
		var self=this;
		debugger;
		console.log("Interface isEnabled");
		var oElem=await self.getElement(elmName);
		var vValue=await oElem.isEnabled();
		return vValue;	
	}
	
	async getValuesOfTable(elmName,elementNextPage,waitSeconds){
		var self=this;
		debugger;
		console.log("Interface getValuesOfTable");
		var oElem=await self.getElement(elmName);
		var vValues=await oElem.getTableRows();
		if (typeof elementNextPage!=="undefined"){ //multiple pages
			var isEnabledNextPage=await self.isEnabled(elementNextPage);
			while (isEnabledNextPage){
				await self.clickOn(elementNextPage);
				await ScStore.waitSecs(waitSeconds);
				var oAuxElem=await self.getElement(elmName);
				var vAuxValues=await oAuxElem.getTableRows();
				for (var i=0;i<vAuxValues.rows.length;i++){
					vValues.rows.push(vAuxValues.rows[i]);
				}
				isEnabledNextPage=await self.isEnabled(elementNextPage);
			}
		}
		return vValues;
		
	}
	async isVisible(elmName,bWithCaption){
		var self=this;
		debugger;
		console.log("Interface getValueText");
		var oElem=await self.getElement(elmName);
		var vValue=await oElem.isVisible(bWithCaption);
		return vValue;
	}
	getFocused(){
		var self=this;
		return self.focused;
	}
	nextFocusable(){
		var self=this;
		var objFocused=self.getFocused();
		if (objFocused===""){
			console.log("Not focused element..... returns item 0");
			return self.elementsOrderedByFocus[0];
		}
		console.log("Focused "+objFocused.id+" with index "+objFocused.focusArrayIndex);
		var iPosition=objFocused.focusArrayIndex;
		iPosition++;
		console.log("next index:"+iPosition);
		if (iPosition>=self.elementsOrderedByFocus.length){
			console.log("The next index:"+iPosition+ " is bigger than:"+self.elementsOrderedByFocus.length);
			iPosition=0;
		}
		console.log("Next element position:"+iPosition);
		var oAux=self.elementsOrderedByFocus[iPosition];
		console.log("Next Element  "+oAux.id+" with index "+oAux.focusArrayIndex);
		return oAux;
	}
	async focusNext(){
		var self=this;
		var oActElem=self.nextFocusable();
		await oActElem.focus();
		await self.changeFocus(oActElem.id);
		return oActElem;
	}
	async setFocus(elmId,withCaption){
		var self=this;
		console.log("Interface:"+self.id+" Set focus to:"+elmId+" focus direct mode:"+self.focusDirect);
		var oElem=await self.getElement(elmId);
		if ((self.elementsOrderedByFocus.length==0)||(oElem.focused)){
			console.log("Is focused now:"+oElem.focused);
			await self.changeFocus(elmId);
		} else if (!self.focusDirect){
			for (const item of self.elementsOrderedByFocus){
				console.log("Order List:"+item.id);
			};
			var oActElem=await self.focusNext();
			console.log("Next to CHECK FOR Focus:"+oActElem.id+" FOCUSED:"+oActElem.focused);
			while (oActElem.id!=elmId){
				oActElem=await self.focusNext();
				console.log("Check to Focus now("+oActElem.id+"):"+oActElem.focused);
				oElem=oActElem;
			}
		} else {
			console.log("Focussing direct to element:"+oElem.id);
			await oElem.focus(withCaption);
		}
		console.log("Located!!!.. Focus now("+oElem.id+"):"+oElem.focused);
		return oElem;
	}
	async loadObjectAttributes(sTypeObject,sIdObject){
		debugger;
		var self=this;
		console.log("Interface:"+self.id+" load all attributes of object:"+sTypeObject+" with id:" +sIdObject);
		var tgtObj=storableObjectManager.get(sTypeObject,"id",sIdObject);
		var arrAttrs=tgtObj.getAttributeNames();
		for (var i=0;i<arrAttrs.length;i++){
			var theAttr=arrAttrs[i];
			if (self.elements.has(theAttr)){
				//check exists in interface definition and exists in active screen
				var bExists=await self.existsElement(theAttr); 
				if (bExists) {
					tgtObj[theAttr]=await self.getValue(theAttr);
				}
			}
		}
	}


}
module.exports=AppInterface;