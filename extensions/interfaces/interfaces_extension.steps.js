var {interfaceManager,AppInterface,InterfaceElement} = require('./interfaceManager');
const ScStore = require('../../scenarioStore');

const stepDefinitions = [{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'add {string} interface {string} with',
    stepFunction: async function(interfaceType,interfaceName,interfaceJsonString){
			//console.log("Adding "+interfaceType +" interface "+interfaceName +" ###"+ interfaceJsonString+"###");
			var objTags=ScStore.getActualTags();
			var interfaceJson=await ScStore.processExpression(interfaceJsonString);
			var oInputJson=await ScStore.processExpression(interfaceJson);
			oInputJson.interfaceType=await ScStore.processExpression(interfaceType);
			oInputJson.interfaceId=await ScStore.processExpression(interfaceName);
			interfaceManager.addFromJson(objTags,oInputJson);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'actual interface is {string}',
		stepFunction: async function(interfaceName){
			var itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return interfaceManager.setActual(itf);
		}
	},{ stepMethod: 'Given', stepTimeout: 120000,
		stepPattern: 'wait for load of interface {string}',
		stepFunction: async function(interfaceName){
			//console.log("ScS: "+ScStore);
			//console.log("ScS: "+ScStore.getInterfaces);
			//console.log("ScS: "+ScStore.getInterfaces());
			//console.log("ScS: "+JSON.stringify(ScStore.getInterfaces()));
			var itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return await itf.waitForLoad();
		}
	},{ stepMethod: 'Given', stepTimeout: 240000,
		stepPattern: 'wait for load one of interfaces {string}',
		stepFunction: async function(interfaceNames){
			debugger;
			var sInterfaceNames=await ScStore.processExpression(interfaceNames);
			var arrInterfaceNames=sInterfaceNames.split(",");
			var arrItfs=[];
			for (itfName of arrInterfaceNames){
				var itf=interfaceManager.getBestMatch(itfName);
				arrItfs.push(itf);
			}
			var bIsLoaded=false;
			while (!bIsLoaded){
				for (itf of arrItfs){
					var bIsLoadedAux=await itf.isLoaded();
					if (bIsLoadedAux) {
						  bIsLoaded=true;
						  var closures=ScStore.getClosures();
						  return await closures.fncVariableOperation(itf,closures.setReturnValue);						
					}
				}
			}
		}
		
/*	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'wait for full load of ad-hoc {string} interface',
		stepFunction: async function(extension){
			var itf=ScStore.getInterfaces().new(extension);
			return await itf.waitForFullLoad();
		}
*/	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on {string}',
		stepFunction: async function(elementName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.clickOn(await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on {string} and wait for load of interface {string}',
		stepFunction: async function(elementName,interfaceName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.clickOn(await ScStore.processExpression(elementName));
			itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return await itf.waitForLoad();
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on {string} where variables {string} and wait for load of interface {string}',
		stepFunction: async function(elementName,theVars,interfaceName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			var sElement=await ScStore.processExpression(elementName);
			var closures=ScStore.getClosures();
			if (typeof theVars!=="undefined"){
				var theVariables=await ScStore.processExpression(theVars);
				closures.newClosure("clickOn with variables:"+JSON.stringify(theVariables));
				closures.extractAttributes(theVariables);
			}
			await itf.clickOn(sElement,false);
			if (typeof theVars!=="undefined"){
				ScStore.getClosures().popClosure();				
			}
			itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return await itf.waitForLoad();
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on item {string} of {string} and wait for load of interface {string}',
		stepFunction: async function(nItem,elementName,interfaceName){
			//console.log ("Click on "+elementName);
			debugger;
			var itf=interfaceManager.getActual();
			var sItem=await ScStore.processExpression(nItem);
			var sElement=await ScStore.processExpression(elementName);
			await itf.clickOnItemNumber(sElement,sItem,false);
			itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return await itf.waitForLoad();
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on {string} non blocking',
		stepFunction: async function(elementName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.clickOn(await ScStore.processExpression(elementName),true);
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'click on {string} non blocking and wait for load of interface {string}',
		stepFunction: async function(elementName,interfaceName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.clickOn(await ScStore.processExpression(elementName),true);
			itf=interfaceManager.getBestMatch(await ScStore.processExpression(interfaceName));
			return await itf.waitForLoad();
		}
		
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'select item number {string} of {string}',
		stepFunction: async function(optionNumber,elementName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.selectOptionNumberOf(
							await ScStore.processExpression(optionNumber)
							,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'select {string} option of {string}',
		stepFunction: async function(optionValue,elementName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.selectOptionValueOf(
							await ScStore.processExpression(optionValue)
							,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'select {string} of {string}',
		stepFunction: async function(optionText,elementName){
			//console.log ("Click on "+elementName);
			var itf=interfaceManager.getActual();
			await itf.selectOptionTextOf(
							await ScStore.processExpression(optionText)
							,await ScStore.processExpression(elementName));
			return;
		}
		
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'write {string} in {string}',
		stepFunction: async function(sText,elementName){
			console.log ("Click on "+elementName);
			ScStore.traceClosures();
			var itf=interfaceManager.getActual();
			await itf.writeText(
						await ScStore.processExpression(sText)
						,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'clear input {string}',
		stepFunction: async function(elementName){
			console.log ("Click on "+elementName);
			ScStore.traceClosures();
			var itf=interfaceManager.getActual();
			await itf.clear(await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'press arrow {string} in {string}',
		stepFunction: async function(idArrow,elementName){
			console.log ("press arrow "+idArrow+" in "+elementName);
			var itf=interfaceManager.getActual();
			await itf.pressArrows(
						await ScStore.processExpression(idArrow)
						,1
						,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'press arrow {string} {string} times in {string}',
		stepFunction: async function(idArrow,nTimes,elementName){
			console.log ("press arrow "+idArrow+" in "+elementName);
			var itf=interfaceManager.getActual();
			await itf.pressArrow(
						await ScStore.processExpression(idArrow)
						,await ScStore.processExpression(nTimes)
						,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'press enter in {string}',
		stepFunction: async function(elementName){
			console.log ("press Enter in "+elementName);
			var itf=interfaceManager.getActual();
			await itf.pressEnter(1,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'press enter {string} times in {string}',
		stepFunction: async function(nTimes,elementName){
			console.log ("press Enter in "+elementName);
			var itf=interfaceManager.getActual();
			await itf.pressEnter(
						await ScStore.processExpression(nTimes)
						,await ScStore.processExpression(elementName));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'generate interface for {string}',
		stepFunction: async function(extension){
			//console.log ("Click on "+elementName);
			await interfaceManager.getActual().listInteractiveElements(await ScStore.processExpression(extension));
			return;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'get value of element {string}',
		stepFunction: async function(elementId){
			debugger;
			//console.log ("Click on "+elementName);
			var sElementId=await ScStore.processExpression(elementId);
			var itf=interfaceManager.getActual();
			var vValue=await itf.getValue(sElementId);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'check class of element {string} includes {string}',
		stepFunction: async function(elementId,className){
			debugger;
			//console.log ("Click on "+elementName);
			var sElementId=await ScStore.processExpression(elementId);
			var sClassName=await ScStore.processExpression(className);
			var itf=interfaceManager.getActual();
			var vValue=await itf.checkClass(sElementId,sClassName);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'check is enabled element {string}',
		stepFunction: async function(elementId){
			debugger;
			//console.log ("Click on "+elementName);
			var sElementId=await ScStore.processExpression(elementId);
			var itf=interfaceManager.getActual();
			var vValue=await itf.isEnabled(sElementId);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'get all table values of {string}',
		stepFunction: async function(elementId){
			debugger;
			console.log ("get all table values of "+elementId);
			var sElementId=await ScStore.processExpression(elementId);
			var itf=interfaceManager.getActual();
			var vValue=await itf.getValuesOfTable(sElementId);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'get all table values of {string} paginated by {string} and waiting {string} seconds',
		stepFunction: async function(elementId,elementNextPage,waitSeconds){
			debugger;
			console.log ("get all table values of "+elementId+" paginated by "+ elementNextPage+" wait "+waitSeconds );
			var sElementId=await ScStore.processExpression(elementId);
			var sElementNextPage=await ScStore.processExpression(elementNextPage);
			var sWaitSeconds=await ScStore.processExpression(waitSeconds);			
			var itf=interfaceManager.getActual();
			var vValue=await itf.getValuesOfTable(sElementId,sElementNextPage,sWaitSeconds);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
		
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'is visible element {string}',
		stepFunction: async function(elementId){
			debugger;
			var sElementId=await ScStore.processExpression(elementId);
			var itf=interfaceManager.getActual();
			var vValue=await itf.isVisible(sElementId);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'is visible element {string} with same caption',
		stepFunction: async function(elementId){
			debugger;
			var sElementId=await ScStore.processExpression(elementId);
			var itf=interfaceManager.getActual();
			var vValue=await itf.isVisible(sElementId,true);
			await ScStore.setReturnValue(vValue);
			return vValue;
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'load from interface the attributes of object {string} wich id is {string}',
		stepFunction: async function(typeObject,objectId){
			debugger;
			var sTypeObject=await ScStore.processExpression(typeObject);
			var sIdObject=await ScStore.processExpression(objectId);
			var itf=interfaceManager.getActual();
			await itf.loadObjectAttributes(sTypeObject,sIdObject);
		}
	}
];
module.exports=stepDefinitions;