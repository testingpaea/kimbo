var InterfaceElement=class InterfaceElement{
	constructor(id,parentInterface){
		var self=this;
		this.id=id;
		self.parentInterface=parentInterface;
		self.selector="";
		self.focusable=false;
		self.focused=false;
		self.focusOrder=0;
		self.focusArrayIndex=0;
	}
	setSelector(objSelectionInfo){
		if (typeof objSelectionInfo!=="undefined"){
			this.selector=objSelectionInfo;
		}
	}
	loadFromJson(jsonObject){
		var self=this;
		self.setSelector(jsonObject.selector);
		if (typeof jsonObject.focusable!=="undefined"){
			self.focusable=jsonObject.focusable;
		}
		if (typeof jsonObject.focused!=="undefined"){
			self.focused=jsonObject.focused;
		}
		if (typeof jsonObject.focusOrder!=="undefined"){
			self.focusOrder=jsonObject.focusOrder;
		}
	} // do nothing
	async isVisible(bWithCaption){
		return true;
	}
}
module.exports=InterfaceElement;