const {interfaceManager,AppInterface,InterfaceElement} = require('./interfaceManager');
const interfaceFunc = function(showInterfacedefFiles=false){
  const glob = require('glob');
  const path = require('path');

  var interfaceDefinitions = {};
  var totalFiles = 0;

  const fileFunction = function (file) {
    const interfaceDefFile = require(path.resolve(file));
    const fileName = path.parse(file).name.slice(0, -8);
    if(showInterfacedefFiles&&fileName!=='reuse-cucumber-scenarios')
    {
      totalFiles++;
      console.log('   Reading .interfacedef.js file: ',path.parse(file).base)
    };
    interfaceDefinitions[fileName] = interfaceDefFile;
	interfaceManager.addFromJson(interfaceDefFile);
 
  }

  showInterfacedefFiles?console.log('/----- START of reuse-cucumber-scenarios message (INTERFACES)----/'):null;
  glob.sync(__dirname + '/../../' +'{,/!(node_modules)/**/}*.interfacedef.js').forEach(fileFunction);
  
  if(showInterfacedefFiles){
    console.log('   ----------------------------------');
    console.log('   Total .interfacedef.js files: '+ totalFiles);
    console.log('/----- END of reuse-cucumber-scenarios message (INTERFACES)------/')
  };

  return { interfaceDefinitions };
}

interfaceFunc();
module.exports = interfaceManager;