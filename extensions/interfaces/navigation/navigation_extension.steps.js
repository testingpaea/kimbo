var interfaceNavigator = require('./navigation_extension.lib');
const ScStore = require('../../../scenarioStore');

const stepDefinitions = [
	{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add interface navigation {string} with',
		stepFunction: async function(navId,jsonValues){
			debugger;
			await interfaceNavigator.step_loadNavigation(navId,jsonValues);
		}
	},{
		stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'validate interface navigation {string}',
		stepFunction: async function(navId){
			debugger;
			await interfaceNavigator.step_validate(navId);
		}
	},{
		stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'navigate to interface {string}',
		stepFunction: async function(itfName){
			debugger;
			await interfaceNavigator.step_navigateTo(itfName);
		}
	},{
		stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'navigate to interface {string} where variables {string}',
		stepFunction: async function(itfName,theVariables){
			debugger;
			await interfaceNavigator.step_navigateTo(itfName,undefined,theVariables);
		}
	},{
		stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'navigate to datasource {string} of {string}',
		stepFunction: async function(dtsName,sObject){
			debugger;
			await interfaceNavigator.step_navigateToDataSource(dtsName,sObject);
		}
	}
];
module.exports=stepDefinitions;