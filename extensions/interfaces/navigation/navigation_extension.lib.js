const ScStore = require('../../../scenarioStore');
debugger;
ScStore.enableExtension("storable");
ScStore.enableExtension("interfaces");

const {interfaceManager,AppInterface,InterfaceElement} = require('../interfaceManager');

var StorableObjectClass=ScStore.getStorable().StorableObjectClass;
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var NavigationNodes=class NavigationNodes extends StorableObjectClass {
	constructor(type,tags){
		super(type,tags);
		var self=this;
		self.id="";
		self.nodes=new Map();
	}
	newNode(sName){
		var self=this;
		var oNode={
			"id":sName,
			"to":new Map(),
			"from":new Map()
		}
		self.nodes.set(sName,oNode);
		return oNode;
	}
	getNode(sName){
		var self=this;
		if (self.nodes.has(sName)) return self.nodes.get(sName);
		return self.newNode(sName);
	}
	addTransitionTo(sNameSrc,sNameDst,transInfo){
		var self=this;
		debugger;
		var ndSrc=self.getNode(sNameSrc);
		var ndDst=self.getNode(sNameDst);
		if (!ndSrc.to.has(sNameDst)){
			ndSrc.to.set(sNameDst,transInfo);
		} 
		if (!ndDst.from.has(sNameSrc)){
			ndDst.from.set(sNameSrc,transInfo);
		}
	}
	async validate(){
		var self=this;
		var itfManager=ScStore.getInterfaces();
		var bErrors=false;
		for (const itf1Name of self.nodes.keys()) {
			var itf1=itfManager.getBestMatch("id",itf1Name);
			if (typeof itf1==="undefined"){
				console.log("The interface ["+itf1Name+"] does not exists");
				throw new Error("The interface ["+itf1Name+"] does not exists");
			}
			var node1=self.getNode(itf1Name);
			for (const itf2Name of node1.to.keys()) {
				var itf2=itfManager.getBestMatch("id",itf2Name );
				if (typeof itf2==="undefined"){
					console.log("Navigation:"+self.id+": The interface ["+itf2Name+"] does not exists");
					bErrors=true;
				}
				var transitionData=node1.to.get(itf2Name);
				var theElement=transitionData.element;
				if ((transitionData.isHuman)||(transitionData.needsParams)){
					//do nothing
				} else if (!(await itf1.hasElement(theElement))){
					console.log("Navigation:"+self.id+": The interface ["+itf1Name+"] does not have element:"+theElement);
					bErrors=true;
				}
				
			}
		}
		if (bErrors){
			throw new Error("Errors validating navigation:"+self.id);
		}
	}
	bestWayTo(srcName,dstName,objVariables){
		var self=this;
		var tgtNode=self.nodes.get(dstName);
		var srcNode=self.nodes.get(srcName);
		debugger;
		var visitedNodes=new Map();
		var checkingNodes=[];
		checkingNodes.push({node:tgtNode,path:[tgtNode]});
		while (checkingNodes.length>0){
			var pNode=checkingNodes.shift();
			if (pNode.node.id==srcNode.id){
				return pNode.path;
			}
			for (const nextNodeName of pNode.node.from.keys()) {
				if (!visitedNodes.has(nextNodeName)){
					var transitionData=pNode.node.from.get(nextNodeName);
					if (!transitionData.isHuman){
						var bProcess=true;
						if (transitionData.needsParams){
							if ((typeof objVariables!=="undefined")&& (typeof transitionData.params!=="undefined")){
								for (const idParam of transitionData.params){
									if (typeof objVariables["$"+idParam] ==="undefined"){
										bProcess=false;
									}
								}
							} else {
								bProcess=false;
								console.log ("There is not params defined in transition from"+ pNode.node.id +" to:"+nextNodeName +" or not params pased to navigation")
							}
						}
						if (bProcess){
							var auxNode=self.nodes.get(nextNodeName);
							var auxPath=[];
							for (const pathElem of pNode.path){
								auxPath.push(pathElem);
							}
							auxPath.push(auxNode);
							checkingNodes.push({node:auxNode,path:auxPath});
							visitedNodes.set(nextNodeName,auxNode);
						}
					}
				}
			}
		}
		return undefined;
	}
	async navigateFromTo(sActualItf,sItfName,objVariables){
		var self=this;
		var thePath=self.bestWayTo(sActualItf,sItfName,objVariables);
		var prevNode;
		var itfManager=ScStore.getInterfaces();		
		while (thePath.length>0){ 
			var dstNode=thePath.pop();
			if (typeof prevNode!=="undefined"){
				var transition=prevNode.to.get(dstNode.id);
				var itf1=await itfManager.getBestMatch("id",prevNode.id);
				var itf2=await itfManager.getBestMatch("id",dstNode.id);
				if (typeof transition.subScenario!=="undefined"){
					await ScStore.callScenario(transition.subScenario,objVariables);
				} else {
					await itf1.clickOn(transition.element);
				}
				await itf2.waitForLoad();
			} 
			prevNode=dstNode;			
		}
		
	}		
}  

var InterfaceNavigationManager =class InterfaceNavigationManager extends StorableObjectListClass{
	constructor(){
		super();
		var self=this;
		self.type="navigation";
		self.actualNavigation="";
		self.addIndexField("id");
		ScStore.mapNoRootTags.set("@InterfaceNavigation","InterfaceNavigation");
	}
	newInstance(tags,oJsonInterfaceNavigation){
		var self=this;
		debugger;
		console.log("newInstance of Interface Navigation");
		var navNodes=new NavigationNodes(self.type,tags);
		navNodes.id=oJsonInterfaceNavigation.id;
		try {
			for (const srcName of Object.keys(oJsonInterfaceNavigation)){
				var srcItf=oJsonInterfaceNavigation[srcName];
				if (typeof srcItf!=="string") {
					debugger; 
					console.log(srcName);
					if (typeof srcItf.to==="undefined"){
						srcItf.to={};
					}
					if (typeof srcItf.from==="undefined"){
						srcItf.from={};
					}
					var toItf=srcItf.to;
					var fromItf=srcItf.from;
					for (const dstName of Object.keys(toItf)){
						navNodes.addTransitionTo(srcName,dstName,toItf[dstName]);
					}
					for (const dstName of Object.keys(fromItf)){
						navNodes.addTransitionTo(dstName,srcName,fromItf[dstName]);
					}
				}
	 		}
		} catch (err) {
			debugger;
			console.log ("Error with navigation");
			throw err;
		}
		return navNodes;		
	}
	
	async step_loadNavigation(navId,jsonValues){
		var self=this;
		debugger;
		var objTags=ScStore.getActualTags();
		var oInputJson=await ScStore.processExpression(jsonValues);
		var sNavId=await ScStore.processExpression(navId);
		oInputJson.id=sNavId;
		self.addFromJson(objTags,oInputJson);
		self.actualNavigation=sNavId;
	}
	async step_validate(navId){
		var self=this;
		debugger;
		var objTags=ScStore.getActualTags();
		var sNavId=await ScStore.processExpression(navId);
		var oNav=self.getBestMatch("id",sNavId);
		await oNav.validate();
	}
	async step_navigateTo(itfName,actualItfName,theVariables){
		var self=this;
		debugger;
		var objTags=ScStore.getActualTags();
		var sItfName=await ScStore.processExpression(itfName);
		var oNav=self.getBestMatch("id",self.actualNavigation);
		var sActualItf=ScStore.getInterfaces().actualInterface;
		if (typeof actualItfName!=="undefined"){
			sActualItf=await ScStore.getInterfaces().getBestMatch(await ScStore.processExpression(actualItfName));
		}
		var vars;
		if (typeof theVariables!=="undefined"){
			vars=await ScStore.processExpression(theVariables);
		}
		await oNav.navigateFromTo(sActualItf.id,sItfName,vars);		
	}
	async step_navigateToDataSource(dtsName,sObject){
		debugger;
		var self=this;
		var refObject=await ScStore.processExpression(sObject);
		var sTypeObject=refObject.type;
		var sDataSource=await ScStore.processExpression(dtsName);
		var objList=ScStore.getStorable().manager.getList(sTypeObject);
		var oDataSource = objList.datasources[sDataSource];
		var oNavigateTo=oDataSource.navigateTo;
		if (typeof oNavigateTo==="undefined") return;
		var homeItf=oNavigateTo.home;
		if (typeof homeItf!=="undefined"){
			homeItf=await ScStore.processExpression(homeItf);
			await self.step_navigateTo(homeItf);
		}		
		var itfName=await ScStore.processExpression(oNavigateTo.interface);
		var srcParams=oNavigateTo.params;
		var theVariables={};
		for (const srcParam in srcParams){
			var theParam=srcParams[srcParam];
			debugger;
			theVariables["$"+srcParam]=await refObject.getValueByDefinition(theParam);
		}
		debugger;
		//var sActualItf=ScStore.getInterfaces().actualInterface;
		await self.step_navigateTo(itfName,undefined,theVariables);
	}
}	


	
var interfaceNavigationManager=new InterfaceNavigationManager();



module.exports = interfaceNavigationManager;