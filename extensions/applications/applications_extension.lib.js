const ScStore = require('../../scenarioStore');

var Application=class Application{
	constructor(name,environment,tags){
		var self=this;
		self.name=name;
		self.environment=environment;
		if (typeof environment==="undefined"){
			self.environment="";
		}
		self.tags=tags;
/*		if (typeof tags==="string"){
//			console.log("Tags are a one string:"+tags+" "+JSON.stringify(JSON.parse(tags)));
			debugger;
			self.tags=ScStore.processExpression(tags);
//			console.log("Now Tags are Json "+self.tags+":"+JSON.stringify(self.tags));
		}
*/		if (typeof self.tags==="undefined"){
			self.tags=[];
		} else if (!Array.isArray(self.tags)){
			self.tags=[self.tags];
		}
		self.updateTags();
		if (self.environment!==""){
			self.setAttributes(environment);
		}
		console.log(JSON.stringify(self.tags));
	}
	updateTags(){
		var self=this;
		self.tagNames=new Map();
		var iIndex=0;
		//console.log("tags:"+JSON.stringify(self.tags));
		for (const tag of self.tags){
			self.tagNames.set(tag.name,tag);
		};
	}
	setTag(tag){
		var self=this;
		if (self.tagNames.has(tag.name)){
			var prevTag=self.tagName.get(tag.name);
			prevTag.value=tag.value;
		} else {
			self.tagNames.set(tag.name,tag);
			self.tags.push(tag);
		}
	}
	setAttributes(envName,tags){
		var self=this;
		if ((typeof envName!=="undefined")&&(envName!=="")){
			self.environment=envName;
			self.setTag({"name":"@ENV","value":envName});
		} 
		if (typeof tags!=="undefined"){
			for (const tag of tags){
				self.setTag(tag);
			};
		} 
	}
}

var ApplicationManager=class ApplicationManager{
	constructor(){
		var self=this;
		self.stackApps=[];
		self.push("cucumber");
		ScStore.mapNoRootTags.set("@Interface","Interface");
	}
	push(name,tags,environment){
		var self=this;
		var auxEnv=environment;
		if (typeof environment==="undefined"){
			if (self.stackApps.length>0){
				auxEnv=self.get().environment;
			}
		}
		var newApp=new Application(name,auxEnv,tags);
		self.stackApps.push(newApp);
		return newApp;
	}
	pop(){
		var self=this;
		return self.stackApps.pop();
	}
	get(){
		var self=this;
		return self.stackApps[self.stackApps.length-1];
	}
	setApplicationAttributes(envName,tags){
		var actApp=this.get();
		actApp.setAttributes(envName,tags);
	}
	addTagToActualApp(tagName,tagValue){
		var actApp=ScStore.getApplications().get();
		actApp.setTag({name:tagName,value:tagValue});
	}
	getSelectorItems(){
		var arrResult=[];
		var actApp=ScStore.getApplications().get();
		if ((typeof actApp.name!=="undefined")&&(actApp.name!=="")){
			arrResult.push({name:"@APP",value:actApp.name});
		}
		if ((typeof actApp.environment!=="undefined")&&(actApp.environment!=="")){
			arrResult.push({name:"@ENV",value:actApp.environment});
		}
		for (const tag of actApp.tags){
			if ((tag.name!="@APP")&&(tag.name!="@ENV")){
				arrResult.push(tag);
			}
		};
		return arrResult;
	}
}


var applicationManager=new ApplicationManager();
ScStore.getSelectorItems=applicationManager.getSelectorItems;
module.exports=applicationManager;