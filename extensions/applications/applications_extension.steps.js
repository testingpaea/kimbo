const ScStore = require('../../scenarioStore');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
  { stepMethod: 'Given', stepPattern: 'out of app {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function (appName){
    	ScStore.getApplications().pop(await ScStore.processExpression(appName));
    }
  }, { stepMethod: 'Given', stepPattern: 'out of actual app', stepTimeout: maxScenarioTimeout,
    stepFunction: function (appName){
    	ScStore.getApplications().pop();
    }
  }, { stepMethod: 'Given', stepPattern: 'in app {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(appName) { 
		ScStore.getApplications().push(await ScStore.processExpression(appName)); 
	} 
  }, { stepMethod: 'Given', stepPattern: 'in app {string} and tags', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(appName,jsonTags) { 
		ScStore.getApplications().push(await ScStore.processExpression(appName),await ScStore.processExpression(jsonTags)); 
	}	 
  }, { stepMethod: 'Given', stepPattern: 'add tag {string} with value {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(tagName,tagValue) { 
		ScStore.getApplications().addTagToActualApp(await ScStore.processExpression(tagName),await ScStore.processExpression(tagValue)); 
	}	 
  }, { stepMethod: 'Given', stepPattern: 'in app {string} and environment {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(appName,envName) { 
		ScStore.getApplications().push(await ScStore.processExpression(appName),undefined,await ScStore.processExpression(envName)); 
	} 
  }, { stepMethod: 'Given', stepPattern: 'in app {string} and environment {string} and tags', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(appName,envName,jsonTags) { 
		ScStore.getApplications().push(await ScStore.processExpression(appName),await ScStore.processExpression(jsonTags),await ScStore.processExpression(envName)); 
	} 
  }, { stepMethod: 'Given', stepPattern: 'in environment {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(envName) { ScStore.getApplications().setApplicationAttributes(await ScStore.processExpression(envName)); 
	}
  }, { stepMethod: 'Given', stepPattern: 'set {string} to application attribute {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(attName,attValue){ 
			ScStore.getApplications().setApplicationAttributes(undefined,[{name:await ScStore.processExpression(attName),value:await ScStore.processExpression(attValue)}]);
		}
  }, { stepMethod: 'Given', stepPattern: 'set application attributes', stepTimeout: maxScenarioTimeout,
    stepFunction: async function(jsonTags) { 
		ScStore.getApplications().setApplicationAttributes(undefined,await ScStore.processExpression(jsonTags)); 
	}
  }
];

module.exports = stepDefinitions;
