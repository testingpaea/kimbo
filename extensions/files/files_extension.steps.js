const ScStore = require('kimbo/scenarioStore');
debugger;
const theManager = require('./files_extension.lib');

const stepDefinitions = [{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add file {string} with',
		stepFunction: async function(elemId,elemJson){
				return await theManager.step_addNewObject(elemJson,elemId);		
		}
	}
];
module.exports=stepDefinitions;