const ScStore = require('kimbo/scenarioStore');
ScStore.enableExtension("storable");
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var FileList=class FileList extends StorableObjectListClass{
	constructor(){
		super("files");
		var self=this;
	}
}
var fileListManager=new FileList();
storableObjectManager["loadFileListManager"]=function(){
	return fileListManager;
};

module.exports=fileListManager;