const ScStore = require('../../scenarioStore');
const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add watchdog {string} with interval {string} seconds using js file {string}',
		stepFunction: async function(watchdogName,sInterval,jsFileName){
			var callResult=await ScStore.getWatchdog().step_add(watchdogName,sInterval,jsFileName);
			return callResult;
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'create watchdog {string} with interval {string} seconds using js file {string}',
		stepFunction: async function(watchdogName,sInterval,jsFileName){
			var callResult=await ScStore.getWatchdog().step_create(watchdogName,sInterval,jsFileName);
			return callResult;
		}
		
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'start wathdog {string}',
		stepFunction: async function(watchdogName){
			var callResult=await ScStore.getWatchDog().step_start(watchdogName);
			return callResult
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'stop wathdog {string}',
		stepFunction: async function(watchdogName){
			var callResult=await ScStore.getWatchDog().step_stop(watchdogName);
			return callResult
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'restart wathdog {string}',
		stepFunction: async function(watchdogName){
			var callResult=await ScStore.getWatchDog().step_restart(watchdogName);
			return callResult
		}
	}
];
module.exports=stepDefinitions;