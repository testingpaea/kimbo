const ScStore = require('../../scenarioStore');
const { readFile, realpath } = require('fs/promises')

ScStore.enableExtension("storable");
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var WatchDogList=class WatchDogList extends StorableObjectListClass{
	constructor(){
		super("watchdog");
		var self=this;
		self.isFactory=true;
		self.addIndexField("id");
		var sampleObj={
			name:"",
			executions:0,
			stop_next:false,
			running:false,
			theFunction:"",
			filename:"",
			functionSrc:"",
			fncRunWatchdog:""
		}
		var sample=self.newInstance(undefined,sampleObj);
		//self.remove("","sample");
		self.sample=sample;
//		self.addIndexField("name");
	}
/*	getByName(name){
		console.log("getByName:"+name);
		var self=this;
		self.getBestMatch("name",name);
	}*/
	async createWatchdog(name,interval,filename){
		debugger;
		var self=this;
		await self.step_createNewObject(name);
		var wd=self.getById(name);
		wd.name=name;
		wd.interval=interval;
		if (typeof wd.interval=="string"){
			wd.interval=parseInt(wd.interval);
		}
		wd.filename=filename;
		
		if ((typeof wd.theFunction==="undefined")||(wd.theFunction=="")){
			console.log ("compiling function of watchdog "+name);
			debugger;
			var srcRealPath=require('path').resolve('./')+"/"+filename;
			var vDoWhile=false;
			while (vDoWhile){
				var sVirtualPath=filename;
				try{
					debugger;
					var auxPath= require.resolve(sVirtualPath);
					console.log(sVirtualPath + " -> " + auxPath);					
				} catch (e) {
					console.log(e.toString());
				}
			}
			console.log(srcRealPath);
			var srcModule=await readFile(srcRealPath, 'utf8');
			console.log(srcModule);
			wd.functionSrc=srcModule;
			var fncWDModule=require(srcRealPath);
			wd.theFunction=fncWDModule;
		}
		wd.executions=0;
		wd.stopNext=false;
		wd.fncRunWatchdog=async function fncRunWatchdog(){
			debugger;
			wd.executions++;
			wd.running=true;
			await wd.theFunction(wd);
			if (!wd.stopNext) {
				setTimeout(wd.fncRunWatchdog,wd.interval*1000);
			} else {
				console.log("Watchdog "+ wd.name + "stopped");
				wd.running=false;
				wd.stop_next=false;
			}
		}
		return wd;		
	}
	async stop(name){
		var self=this;
		var wd=self.getById(name);
		wd.stop_next=true;		
	}
	async start(name){
		await this.restart(name);
	}
	
	async restart(name){
		var self=this;
		var wd=self.getById(name);
		if (!wd.running){
			setTimeout(wd.fncRunWatchdog,wd.interval*1000);
		}
	}
	async step_create(watchdogName,sInterval,jsFileName){
		var self=this;
		var swdName=await ScStore.processExpression(watchdogName);
		var interval=await ScStore.processExpression(sInterval);
		var fileName=await ScStore.processExpression(jsFileName);
		return await self.createWatchdog(swdName,interval,fileName);
	}
	async step_add(watchdogName,sInterval,jsFileName){
		var self=this;
		await self.step_create(watchdogName,sInterval,jsFileName);
		await self.step_start(watchdogName);
	}
	async step_stop(watchdogName){
		var self=this;
		var swdName=await ScStore.processExpression(watchdogName);
		var wd=self.getById(swdName);
		wd.stop();
	}
	async step_restart(watchdogName){
		var self=this;
		var swdName=await ScStore.processExpression(watchdogName);
		var wd=self.getById(swdName);
		wd.restart();
	}
	async step_start(watchdogName){
		var self=this;
		self.step_restart(watchdogName);
	}	
	
}
var watchdogListManager=new WatchDogList();
storableObjectManager["loadWatchdogListManager"]=function(){
	return watchdogListManager;
};

module.exports=watchdogListManager;