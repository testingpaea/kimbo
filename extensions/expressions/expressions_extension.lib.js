const ScStore = require('../../scenarioStore');

var ExpressionProcessor=class ExpressionProcessor {
	constructor(varToken,varCloseTokens,jsOpenToken,jsCloseToken){
		var self=this;
		self.setParams(varToken,varCloseTokens,jsOpenToken,jsCloseToken);
	}
	setParams(varToken,varCloseTokens,jsOpenToken,jsCloseToken){
		var self=this;
		self.varToken=(typeof varToken==="undefined"?"$":varToken);
		self.varCloseTokens=(typeof varCloseTokens==="undefined"?[".",","," ",":","{","}","[","]","(",")","'",'"',"=","<",">","+","-","!","&","/","%"]:varCloseTokens);
		self.jsOpenToken=(typeof jsOpenToken==="undefined"?"(#(":jsOpenToken);
		self.jsCloseToken=(typeof jsCloseToken==="undefined"?")#)":jsCloseToken);
	}
	processExpressionParseJson(sJSON){
		var self=this;
		var dataReviver=function dataReviver(key, value){
			//console.log("JSON key:"+key+" value:"+value);
			return self.processExpressionSync(value);
		}			
		return JSON.parse(sJSON,dataReviver);
	}
	replaceAllWithBrackets(sText,find,replace){
		return sText.replace(find, replace);
	}
	replaceAllWithoutBrackets(sText,find,replace,bModulator){
		return sText.replace(new RegExp(find, bModulator), replace);
	}
	replaceAll(str, find, replace, bInsensitive) {
		  var self=this;
		  if ((typeof str==="undefined")||(str==null)) return "";
		  var bModulator='g';
		  if ((typeof bInsensitive!=="undefined")&&(bInsensitive)){
			  bModulator="i"+bModulator;
		  }
		  var replaceFnc;
		  if ((find.indexOf("[")>=0)||(find.indexOf("]")>=0)){
			  /*
			   * [ ] are special cases in find.... 
			   */
			  replaceFnc=self.replaceAllWithBrackets;
		  } else {
			  replaceFnc=self.replaceAllWithoutBrackets;
		  }
		  if ((!Array.isArray(str))&&(!(typeof str==='string'))) str=str+"";
		  if (typeof str==='string'){
			  return replaceFnc(str,find,replace,bModulator);
		  }
		  if (str.length==0) return "";
		  var sResult=[];
		  var sAux="";
		  var sSubstr;
		  var fLength=find.length;
		  var strLength=(str.length-1);
		  var bNext=true;
		  var iRow=-1;
		  while (iRow<strLength){
			  if (bNext){
				  iRow++;
				  sAux=sAux+str[iRow];
			  }
			  if ((sAux=="")||(sAux.length<fLength)){
				  bNext=true;
			  } else {
				  sAux=replaceFnc(sAux,find,replace,bModulator);
				  if (sAux.length>fLength){// there is more letter... cut
					  sSubstr=sAux.substring(0,sAux.length-fLength);
					  sResult.push(sSubstr);
					  sAux=sAux.substring(sAux.length-fLength,sAux.length);
					  bNext=true;
				  }
			  }
		  }
		  if (sAux!=""){
			  if (sAux.length>fLength){
				  sAux=replaceFnc(sAux,find,replace,bModulator);
			  }
			  sResult.push(sAux);
		  }
		  if (sResult.length==1){
			  return sResult[0];
		  } else if (sResult.length==0){
			  return "";
		  } else return sResult;
	};
	
	extractToken(inputExpression,firstPos,openToken,arrCloseToken,bLast,bRemoveTokens){
		var openPos;
		if (typeof inputExpression!=="string") {
			return {firstPart:"",inText:inputExpression,lastPart:""};
		}
		var rtnValue=inputExpression;
		
		if ((typeof bLast!=="undefined")&&bLast){
			openPos=rtnValue.lastIndexOf(openToken,firstPos);
		} else {
			openPos=rtnValue.indexOf(openToken,firstPos);
		}
		var bSkipTokens=((typeof bRemoveTokens!=="undefined")&&bRemoveTokens);
		var closePos=0;
		var firstPart="";
		var lastPart="";
		
		firstPart=rtnValue.substring(0,openPos);
		lastPart=rtnValue.substring(openPos+(bSkipTokens?openToken.length:0),rtnValue.length);
		var indexEnd=rtnValue.length;
		var selToken="";
		for (const separator of arrCloseToken){
			var indx=lastPart.indexOf(separator);
			if ((indx>=0)&&(indx<indexEnd)){
				selToken=separator;
				indexEnd=indx;
			}
		};
		var inText=lastPart.substring(0,indexEnd);
		lastPart=lastPart.substring(indexEnd+(bSkipTokens?selToken.length:0),lastPart.length);
		return {firstPart:firstPart.trim(),inText:inText,lastPart:lastPart.trim()};
	}
	processExpressionVariables(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") {
			var objResult={};
			objResult.rtnValue=inputExpression; 
			objResult.arrVarsNames=[];
			objResult.arrVarsValues=[];
			return objResult;
		}			
		var rtnValue=inputExpression;
		var arrVarsValues=[];
		var arrVarsNames=[];
		var openPos=rtnValue.indexOf(self.varToken);
		while (openPos>=0) { // javascript function... its includes a  name of a variable or "$("
//				console.log("includes $");
			var parts=self.extractToken(rtnValue,openPos,self.varToken,self.varCloseTokens);
			var varName=parts.inText;
			var auxRestVarName=varName.substring(varName.trim().length,varName.length);
			var auxVarName=varName.trim();
			if (auxVarName!==""){
				var varValue="";
				var bExistsVar=true;
				if (ScStore.existsVar(auxVarName)){
					varValue=ScStore.getVar(auxVarName).value;
				} else if (ScStore.existsVar(auxVarName,true)){
					varValue=ScStore.getVar(auxVarName,true).value;
				} else if (auxVarName.charAt(1)=="@"){ //its a tag of app
					debugger;
					var theTag=auxVarName.substring(1,auxVarName.length);
					var theApp=ScStore.getApplications().get();
					if (theApp.tagNames.has(theTag)){
						varValue=theApp.tagNames.get(theTag).value;
					} else {
						bExistsVar=false;
					}
				} else {
					bExistsVar=false;
				}
				var sVarResult="";
				if (bExistsVar){
					//if (typeof varValue!=="string"){
						arrVarsNames.push(varName);
						arrVarsValues.push(varValue);
						sVarResult="_arrRefs_["+(arrVarsNames.length-1)+"]";
					//} else {
					//	sVarResult=varValue;
					//}
				} else {
					sVarResult=varName;
					openPos++;
				}
				rtnValue=parts.firstPart
							+sVarResult
							+auxRestVarName+parts.lastPart;
			}
			openPos=rtnValue.indexOf(self.varToken,openPos);
		}
		var objResult={};
		objResult.rtnValue=rtnValue;
		objResult.arrVarsNames=arrVarsNames;
		objResult.arrVarsValues=arrVarsValues;
		return objResult;
	}
	processExpressionJavascriptsSync(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") return inputExpression; 
//			console.log("is string");
		var indInternalVar=0;
		var rtnValue=inputExpression;
		var openToken=self.jsOpenToken;
		var closeToken=self.jsCloseToken;
		while ( (typeof rtnValue==="string") &&
			    (rtnValue.lastIndexOf(openToken)>=0)
			    ){ // its a replacement in a strings.....
			//(#(javascript)#)
			var parts=self.extractToken(rtnValue,rtnValue.length,openToken,[closeToken],true,true);
			var jsPart=parts.inText;
			var objResult=self.processExpressionVariables(jsPart);
			var jsResult=self.processExpressionFunctionSync(objResult.rtnValue,objResult.arrVarsValues);
			debugger;
			if ((parts.firstPart.lastIndexOf(openToken)>=0)
			   || (parts.lastPart.lastIndexOf(openToken)>=0)
			   || ( (typeof jsResult==="string") &&
					(jsResult.lastIndexOf(openToken)>=0)
					)
					){
				var vName="internalVar"+indInternalVar;
				indInternalVar++;
				ScStore.addVar(vName,jsResult,true);
				jsResult="$"+vName;
			}
			if ((parts.firstPart==="")&&(parts.lastPart==="")){
				rtnValue=jsResult;
			} else {
				rtnValue=parts.firstPart+jsResult+parts.lastPart;
			}
		} 
		return rtnValue;
	}
	async processExpressionJavascripts(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") return inputExpression; 
//			console.log("is string");
		var indInternalVar=0;
		var rtnValue=inputExpression;
		var openToken=self.jsOpenToken;
		var closeToken=self.jsCloseToken;
		while ( (typeof rtnValue==="string") &&
				(rtnValue.lastIndexOf(openToken)>=0)
				){ // its a replacement in a strings.....
			//(#(javascript)#)
			var parts=self.extractToken(rtnValue,rtnValue.length,openToken,[closeToken],true,true);
			var jsPart=parts.inText;
			var objResult=self.processExpressionVariables(jsPart);
			var jsResult=await self.processExpressionFunction(objResult.rtnValue,objResult.arrVarsValues);
			debugger;
			if ((parts.firstPart.lastIndexOf(openToken)>=0)
			   || (parts.lastPart.lastIndexOf(openToken)>=0)
			   || ( (typeof jsResult==="string") &&
					(jsResult.lastIndexOf(openToken)>=0)
					)
			   ){
				var vName="internalVar"+indInternalVar;
				indInternalVar++;
				ScStore.addVar(vName,jsResult,true);
				jsResult="$"+vName;
			}
			if ((parts.firstPart==="")&&(parts.lastPart==="")){
				rtnValue=jsResult;
			} else {
				rtnValue=parts.firstPart+jsResult+parts.lastPart;
			}
		} 
		return rtnValue;
	}
	addFunctionShell(sBody,isFunction){
		if ((typeof isFunction=="undefined")||(!isFunction)){
			var sFncFormula=`	var result=
									`+sBody+`
									 ;
								return result;`;
			return sFncFormula;
		}
		return sBody;
	}
	replaceRefs(sInputString,arrValues){
		var self=this;
		var rtnValue=sInputString;
		for (var i=0;i<arrValues.length;i++){
			rtnValue=self.replaceAll(rtnValue,"_arrRefs_["+i+"]",arrValues[i]);
		}
		return rtnValue;
	}
	processExpressionFunctionSync(sBody,arrVarsValues,isFunction){
		var self=this;
		var sFncFormula=self.addFunctionShell(sBody,isFunction);
		var fncFormula = "";
		try {
			fncFormula = new Function("return function (_arrRefs_) { "+sFncFormula+" }")();
			//var fncFormula= "function (_arrRefs_) { "+sFncFormula+" }".parseFunction();
			//var fncFormula=new Function("_arrRefs_",sFncFormula);
		} catch (excpt){
			
		}
		if (fncFormula!==""){
			try {
				var vResult = fncFormula(arrVarsValues);
				return vResult;
			} catch (excpt){
				
			} 
		}
		return self.replaceRefs(sBody,arrVarsValues);
	}
	async processExpressionFunction(sBody,arrVarsValues,isFunction){
		var self=this;
		var sFncFormula=self.addFunctionShell(sBody,isFunction);
		var fncFormula = "";
		try {
			fncFormula = new Function("return async function (_arrRefs_) { "+sFncFormula+" }")();
		//var fncFormula= "async function (_arrRefs_) { "+sFncFormula+" }".parseFunction();
		//var fncFormula=new AsyncFunction("_arrRefs_",sFncFormula);
		} catch (excpt){
			//console.log("The exception is catched");
		}

		if (fncFormula!==""){
			try {
				var oResult = await fncFormula(arrVarsValues);
				return oResult;
			} catch (excpt){
				//console.log("the posible formula doesnt runs:"+fncFormula.toString());
				//console.log(excpt.toString());
			}
		}
		return self.replaceRefs(sBody,arrVarsValues);
	}
	processFirstPass(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") return inputExpression; 
		var objResult={source:inputExpression,result:"",endProcess:false};
		var expression=inputExpression;
		if ((typeof expression ==="undefined")||(typeof expression!=="string")){
			objResult.result=expression;
			objResult.endProcess=true;
		} else {
			expression=expression.trim();
			if (expression=="") {
				objResult.result="";
				objResult.endProcess=true;
			} else {
		//		console.log("procesing expression:"+expression);
				var rtnValue=expression;
				if ((rtnValue[0]=="{")||(rtnValue[0]=="[")){
		//				console.log("is json");
					rtnValue=self.processExpressionParseJson(rtnValue);
					objResult.endProcess=true;
				}
				objResult.result=rtnValue;
			}
		}
		return objResult;
	}
	processExpressionSync(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") return inputExpression; 
		var objResult=self.processFirstPass(inputExpression);
		if (objResult.endProcess) return objResult.result;
		var rtnValue=objResult.result;
		ScStore.getClosures().newClosure(rtnValue);
		rtnValue=self.processExpressionJavascriptsSync(rtnValue);
		objResult=self.processExpressionVariables(rtnValue);
		if (objResult.arrVarsValues.length>0){
			rtnValue=self.processExpressionFunctionSync(objResult.rtnValue,objResult.arrVarsValues);
		}
		ScStore.getClosures().popClosure();
//		console.log("effective value:"+rtnValue + "  --> " + JSON.stringify(rtnValue));
		return rtnValue;
	}
	async processExpression(inputExpression){
		var self=this;
		if (typeof inputExpression!=="string") return inputExpression; 
		var objResult=self.processFirstPass(inputExpression);
		if (objResult.endProcess) return objResult.result;
		var rtnValue=objResult.result;
		ScStore.getClosures().newClosure(rtnValue);
		rtnValue=await self.processExpressionJavascripts(rtnValue);
		objResult=self.processExpressionVariables(rtnValue);
		if (objResult.arrVarsValues.length>0){
			rtnValue=await self.processExpressionFunction(objResult.rtnValue,objResult.arrVarsValues);
		}
		ScStore.getClosures().popClosure();
//		console.log("effective value:"+rtnValue + "  --> " + JSON.stringify(rtnValue));
		return rtnValue;
	}
}
var expressionProcessor=new ExpressionProcessor();
module.exports = expressionProcessor;
