const ScStore = require('kimbo/scenarioStore');
const storableElems=require('./storable_extension.lib');
var storableManager=storableElems.manager;
const maxScenarioTimeout=10000000;
const stepDefinitions = [				   
      { stepMethod: 'Given', stepPattern: 'add storable object {string} with',
        stepFunction: async function(objType,inputJson){
			//console.log("Adding storable object "+objType+" "+ inputJson);
			await storableManager.step_addNewTypedObject(objType,inputJson);
			return;
		}
     },{ stepMethod: 'Given', stepPattern: 'add object factory {string} with this sample',
        stepFunction: async function(objType,inputJson){
			//console.log("Adding storable object factory "+objType+" "+ inputJson);
			await storableManager.step_addNewObjectFactory(objType,inputJson);
			return;
		}
     },{ stepMethod: 'Given', stepPattern: 'add index using field {string} to object {string}',
        stepFunction: async function(fieldName,objType){
			//console.log("Adding index to storable object "+objType);
			await storableManager.step_addIndexToObject(objType,fieldName);
			return;
		}
     },{ stepMethod: 'Given', stepPattern: 'add function to {string} object',
        stepFunction: async function(objType,inputFunction){
			//console.log("Adding function to sample object factory "+objType+" "+ inputFunction);
			await storableManager.step_addFunctionToObjects(objType,inputFunction);
			return;
		}
     },{ stepMethod: 'Given', stepPattern: 'new object {string} with id {string}',
        stepFunction: async function(objType,theId){
			//console.log("create a new object from factory "+objType+" and store with id "+ theId);
			await storableManager.step_createNewObject(objType,theId);
			return;
		}
     },{ stepMethod: 'Given', stepPattern: 'new object {string} from {string}',
        stepFunction: async function(objType,jsonValues){
			//console.log("create a new object or objects from factory "+objType+" and fill with data");
			debugger;
			await storableManager.step_addNewTypedObject(objType,jsonValues);
			return;
		}
	},{ stepMethod: 'Given', stepPattern: 'add object {string} with id {string} and values from json', 
		stepFunction: async function(sObjectType,theId,jsonValues){ 
			debugger;
			await storableManager.step_addNewTypedObject(sObjectType,jsonValues,theId);
		}
	},{ stepMethod: 'Given', stepPattern: 'remove object {string} with id {string}', 
		stepFunction: async function(sObjectType,theId){ 
			debugger;
			await storableManager.step_removeTypedObject(sObjectType,theId);
		}
	  },{ stepMethod: 'Given', stepPattern: 'clear list of objects {string}',
        stepFunction: async function(sObjectType){
			//console.log("Empty the list in memory for objects "+sObjectType);
			debugger;
			await storableManager.step_clearList(sObjectType);
	  	}	  	
	},{ stepMethod: 'Given', stepPattern: 'add object {string} with id {string} as extend of id {string} and values from json', 
		stepFunction: async function(sObjectType,theNewId,theSourceId,jsonValues){
			debugger;
			await storableManager.step_addNewTypedExtendedObject(sObjectType,theSourceId,jsonValues,theNewId);
		}
	},{ stepMethod: 'Given', stepPattern: 'add object {string} as extend of id {string} and values from json', 
		stepFunction: async function(sObjectType,theSourceId,jsonValues){
			debugger;
			await storableManager.step_addNewTypedExtendedObject(sObjectType,theSourceId,jsonValues);
		}
	},{ stepMethod: 'Given', stepPattern: 'extract {string} attributes of {string}', 
		stepFunction: async function(objType,idxValue){
			debugger;
			return await storableManager.step_extractAttributes(objType,"id",idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'extract {string} attributes which {string} is {string}', 
		stepFunction: async function(objType,idxName,idxValue){
			return await storableManager.step_extractAttributes(objType,idxName,idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the result of object {string} function {string} which id is {string}', 
		stepFunction: async function(tgtVariableName,sObjectType,fncName,idxValue){
			return await storableManager.step_assignMethodResultToVariable(sObjectType,tgtVariableName,fncName,"id",idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign {string} to the field {string} of {string} which id is {string}', 
		stepFunction: async function(theValue,fieldName,sObjectType,idxValue){
			return await storableManager.step_assignToField(sObjectType,theValue,fieldName,"id",idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the result of object {string} function {string} which {string} is {string}', 
		stepFunction: async function(tgtVariableName,objType,fncName,idxName,idxValue){
			return await storableManager.step_assignMethodResultToVariable(objType,tgtVariableName,fncName,idxName,idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the value of object {string} attribute {string} which id is {string}',
		stepFunction: async function(tgtVariableName,sObjectType,attName,idxValue){
			return await storableManager.step_assignAttributeToVariable(sObjectType,tgtVariableName,attName,"id",idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the value of object {string} attribute {string} which {string} is {string}',
		stepFunction: async function(tgtVariableName,sObjectType,attName,idxField,idxValue){
			return await storableManager.step_assignAttributeToVariable(sObjectType,tgtVariableName,attName,idxField,idxValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the clone object {string} which id is {string}', 
		stepFunction: async function(tgtVariableName,objType,idxValue){
			var vResult=await storableManager.step_assignCloneToVariable(objType,tgtVariableName,"id",idxValue);
			return vResult;
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the clone object {string} which {string} is {string}', 
		stepFunction: async function(tgtVariableName,objType,idxName,idxValue){
			var vResult=await storableManager.step_assignCloneToVariable(objType,tgtVariableName,idxName,idxValue);
			return vResult;
		}
	},{ stepMethod: 'Given', stepPattern: 'if not exists variable {string} initialize it with the clone of object {string} which id is {string}', 
		stepFunction: async function(tgtVariableName,objType,idxValue){
			debugger;
			var vResult=await storableManager.step_initializeWithClone(objType,tgtVariableName,"id",idxValue);
			return vResult;
		}
									
	},{ stepMethod: 'Given', stepPattern: 'use table {string} to create clones of object {string} wich index {string} is {string} filling attributes {string}', 
		stepFunction: async function(objTable,objType,idxName,idxValue,lstAttributes){
			debugger;
			var vResult=await storableManager.step_createObjectsFromTable(objTable,objType,lstAttributes,false,undefined,undefined,idxName,idxValue);
			return vResult;
		}								   
	},{ stepMethod: 'Given', stepPattern: 'use table {string} to create clones of object {string} wich index {string} is {string} filling attributes {string} and set {string} to attrib {string}', 
		stepFunction: async function(objTable,objType,idxName,idxValue,lstAttributes,extraValue,extraAttr){
			debugger;
			var vResult=await storableManager.step_createObjectsFromTable(objTable,objType,lstAttributes,false,extraValue,extraAttr,idxName,idxValue);
			return vResult;
		}						
	},{ stepMethod: 'Given', stepPattern: 'use table {string} to create objects {string} filling attributes {string}', 
		stepFunction: async function(objTable,objType,lstAttributes){
			debugger;
			var vResult=await storableManager.step_createObjectsFromTable(objTable,objType,lstAttributes,true);
			return vResult;
		}						
	},{ stepMethod: 'Given', stepPattern: 'use table {string} to create objects {string} filling attributes {string} and set {string} to attrib {string}', 
		stepFunction: async function(objTable,objType,lstAttributes,extraValue,extraAttr){
			debugger;
			var vResult=await storableManager.step_createObjectsFromTable(objTable,objType,lstAttributes,true,extraValue,extraAttr);
			return vResult;
		}						
	},{ stepMethod: 'Given', stepPattern: 'use table {string} to create objects {string} autoID filling attributes {string} and set {string} to attrib {string}', 
		stepFunction: async function(objTable,objType,lstAttributes,extraValue,extraAttr){
			debugger;
			var vResult=await storableManager.step_createObjectsFromTable(objTable,objType,lstAttributes,true,extraValue,extraAttr,undefined,undefined,true);
			return vResult;
		}						
	},{ stepMethod: 'Given', stepPattern: 'create new {string} with id {string}', 
		stepFunction: async function(objType,idDst){
			debugger;
			var vResult=await storableManager.step_createNewObject(objType,idDst);
			return vResult;
		}
	},{ stepMethod: 'Given', stepPattern: 'create clon of object {string} wich id is {string} and set id {string} to the new object', 
		stepFunction: async function(objType,idSrc,idDst){
			debugger;
			var vResult=await storableManager.step_createClon(objType,idSrc,idDst);
			return vResult;
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the number of stored objects {string}', 
		stepFunction: async function(tgtVariableName,objType){
			return await storableManager.step_assignCountToVariable(tgtVariableName,objType);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the list of IDs of stored objects {string}', 
		stepFunction: async function(tgtVariableName,objType){
			return await storableManager.step_assignAllIDsToArrayVariable(tgtVariableName,objType);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the list of IDs of stored objects {string} where attribute {string} is {string}', 
		stepFunction: async function(tgtVariableName,objType,whereAttr,whereVal){
			return await storableManager.step_assignAllIDsToArrayVariable(tgtVariableName,objType,whereAttr,whereVal);
		}
		 
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the max attribute {string} value of the objects {string}', 
		stepFunction: async function(tgtVariableName,attName,objType){
			return await storableManager.step_assignMaxToVariable(tgtVariableName,attName,objType);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the datasource {string} of {string}', 
		stepFunction: async function(tgtVariableName,datasource, theObject){
			return await storableManager.step_assignDatasourceToVariable(tgtVariableName,datasource,theObject);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the list of datasources of objects {string}', 
		stepFunction: async function(tgtVariableName,objType){
			return await storableManager.step_assignDatasourcesToVariable(tgtVariableName,objType);
		}
	},{ stepMethod: 'Given', stepPattern: 'prepare a call to {string} datasource of {string}', 
		stepFunction: async function(datasource, theObject){
			return await storableManager.step_extractDatasource(datasource,theObject);
		}
		
	},{ stepMethod: 'Given', stepPattern: 'for each {string} in the list {string} do', stepTimeout: maxScenarioTimeout, isLoopStart:true,
    	stepFunction: async function(auxVarName,objType){
			return await storableManager.step_startLoopForEach(auxVarName,objType);
		} 
	},{ stepMethod: 'Given', stepPattern: 'apply {string} to all elements of {string} using {string}',
    	stepFunction: async function(sPostActions,objType,refObject){
			return await storableManager.step_applyActions(sPostActions,objType,refObject);
		} 
			  

	}
	
	
];
module.exports=stepDefinitions;