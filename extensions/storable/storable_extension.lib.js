const ScStore = require('kimbo/scenarioStore');
const lodash = require('lodash');
const {v1: uuidv1,v4: uuidv4,} = require('uuid');


ScStore.setNotRootUserTag("@DataObject","DataObject");

var StorableObject=class StorableObject{
	constructor(objType,tags){
		var self=this;
		self.type=objType;
		self.tags=tags;
		if (typeof tags==="undefined"){
			self.tags=[];
		} else if (!Array.isArray(tags)){
			self.tags=[tags];
		}
		self.updateTags();
	}
	updateTags(){
		var self=this;
		self.tagNames=new Map();
		var iIndex=0; 
		//console.log("tags:"+JSON.stringify(self.tags));
		for (var i=0;i<self.tags.length;i++){
			var tag=self.tags[i];
			self.tagNames.set(tag.name,tag);
		};
	}
	getAttribute(attName){
		var self=this;
		if (typeof self[attName]==="undefined"){
			//throw new Error("The Attribute "+attName+" does not exists in User "+user.id);
			return "";
		} else {
			return self[attName];
		}			
	}
	loadFromJson(jsonObject){
		var self=this;
		for (const attribute of Object.keys(jsonObject)){
			self[attribute]=jsonObject[attribute];
		};
		self["_jsonSourceObject"]=jsonObject;
	}
	extractAttributes(){
		var self=this;
		ScStore.getClosures().extractAttributes(self);
	}
	getAttributeNames(){
		var self=this;
		var arrResult=[];
		for (const attrib of Object.keys(self)){
			if (typeof self[attrib]!=="function"){
				arrResult.push(attrib);
			}
		};
		return arrResult;
	}
	linkOtherObjects(myNewFieldName,myFieldNameValueToSearch,childObjectType,childIndexNameToUse){
		var self=this;
		var oParent=self;
		var parentIndexValue=oParent[myFieldNameValueToSearch];
		var oList=ScStore.getStorable().manager.getList(childObjectType);
		debugger;
		var arrLinked=oList.getAll(childIndexNameToUse,parentIndexValue);
		if (arrLinked.length==1){
			oParent[myNewFieldName]=arrLinked[0];
		} else {
			oParent[myNewFieldName]=arrLinked;
		}
	}
	async setValueByDefinition(oDefinition,value){
		var self=this;
		if (oDefinition.type=="ObjectField"){
			var arrPath=oDefinition.path;
			if (!Array.isArray(arrPath)){
				arrPath=[arrPath];
			}
			var iPathPos=0;
			var dstObject=self;						
			for (iPathPos=0;iPathPos<arrPath.length-1;iPathPos++){
				var pathPos=arrPath[iPathPos];
				var sPathPos=await ScStore.processExpression(pathPos);
				if (typeof dstObject[sPathPos]=="undefined"){
					dstObject[sPathPos]={};
				}
				dstObject=dstObject[sPathPos];
			}						
			var sPathPos=await ScStore.processExpression(arrPath[arrPath.length-1]);
			dstObject[sPathPos]=value;
		}

	}
	
	async getValueByDefinition(oDefinition){
		var self=this;
		var prmValue=oDefinition;
		var value="";
		if (typeof prmValue==="string") return prmValue; 
		if (prmValue.type=="ObjectField"){
			var arrPath=prmValue.path;
			if (!Array.isArray(arrPath)){
				arrPath=[arrPath];
			}
			value=self;
			for (const pathPos of arrPath){
				var sPathPos=await ScStore.processExpression(pathPos);
				value=value[sPathPos];
			}
			value=await ScStore.processExpression(value);
		} else if (typeof prmValue.value!=="undefined"){
			value=await ScStore.processExpression(prmValue.value);
		}
		return value;
	}

}
var ObjectList=class ObjectList{
	constructor(objType){
		var self=this;
		self.type=objType;
		self.isFactory=false;
		self.arrObjects=[];
		self.arrIndexes=[];
		self.arrIndexFields=[];
		self.addIndexField("id");
		self.extendedWaiting=new Map();
		self.arrFunctions=[];
	}
	updateIndex(index){
		var self=this;
		debugger;
		var mapObjs=self.arrIndexes[index];
		if (typeof mapObjs==="undefined"){
			mapObjs=new Map();
			self.arrIndexes[index]=mapObjs;
		} else {
			mapObjs.clear();
		}
		for (const tmpObj of self.arrObjects){
			var idxValue=tmpObj.getAttribute(index);
			var arrObjs=mapObjs.get(idxValue);
			if (typeof arrObjs==="undefined"){
				arrObjs=[];
				mapObjs.set(idxValue,arrObjs);
			}
			arrObjs.push(tmpObj);
		}
	}
	addIndexField(idxFieldName){
		var self=this;
		var bExists=false;
		for (const idxName of self.arrIndexFields){
			if (idxName==idxFieldName){
				bExists=true;
			}
		};
		if (!bExists) {
			self.arrIndexFields.push(idxFieldName);
			self.updateIndex(idxFieldName);
		}
	}
/*	getSubSet(idxFieldName){
		debugger;
		var self=this;
		var idx=idxFieldName;
		var tgtValue=idxValue;
		if (self.arrIndexFields.length==0) return "";
		if (typeof idxValue==="undefined"){
			idx=self.arrIndexFields[0];
			tgtValue=idxFieldName;
		}
		if (typeof self.arrIndexes[idx]==="undefined") return "";
		var mapObjects=self.arrIndexes[idx];
		if (!mapObjects.has(tgtValue)) return "";
		return mapObjects.get(tgtValue);
	}
*/	


	getAll(idxFieldName,idxValue){
		debugger;
		var self=this;
		var idx=idxFieldName;
		var tgtValue=idxValue;
		if (self.arrIndexFields.length==0) return "";
		if (typeof idxValue==="undefined"){
			idx=self.arrIndexFields[0];
			tgtValue=idxFieldName;
		}
		if (typeof self.arrIndexes[idx]==="undefined") return "";
		var mapObjects=self.arrIndexes[idx];
		if (!mapObjects.has(tgtValue)) return "";
		return mapObjects.get(tgtValue);
	}
	get(idxFieldName,idxValue){
		var self=this;
		var allObjs=self.getAll(idxFieldName,idxValue);
		if (allObjs=="") return "";
		if (allObjs.length==0) return "";
		return allObjs[0];
	}
	getById(elementId){
		//console.log("getById:"+elementId);
		var self=this;
		return self.getBestMatch("id",elementId);
	}
	getBestMatch(idxFieldName,idxValue){
		var self=this;
		var arrItems=self.getAll(idxFieldName,idxValue);
		if (typeof arrItems==="undefined"){
			return undefined;
		}
		return ScStore.getBestMatch(arrItems);
	}
	async extractAttributes(keyAttrib,keyValue){
		var self=this;
		var sKeyValue=await ScStore.processExpression(keyValue);
		var sKeyAttrib=await ScStore.processExpression(keyAttrib);
		var oItem=self.getBestMatch(sKeyAttrib,sKeyValue);
		oItem.extractAttributes();
	}
	
	newInstance(tags,jsonObject){
		var self=this;
		var tmpObj=new StorableObject(self.type,tags);
		if (self.isFactory){
			if ((typeof self.sample!=="undefined")&&(self.sample!="")){
				for (const attrName in self.sample){
					var vAttr=self.sample[attrName];
					if (typeof tmpObj[attrName]==="undefined"){
						if (typeof vAttr==="function"){
						} else if (vAttr instanceof Object){
							tmpObj[attrName]=JSON.parse(JSON.stringify(vAttr));
						} else {
							tmpObj[attrName]=vAttr;
						}
					}
				}
			}
		}
		tmpObj.loadFromJson(jsonObject);
		tmpObj.type=self.type;
		self.arrFunctions.forEach(function(objFunction){
			tmpObj[objFunction.name]=objFunction.function;
		});

		return tmpObj;
	}

	newListObject(tags,jsonObject){
		var self=this;
		debugger;
		var tmpObj=self.newInstance(tags,jsonObject);
		self.addListObject(tmpObj);
		return tmpObj;
	}
	addListObject(theObject){
		var self=this;
		debugger;
		var tmpObj=theObject;
		self.arrObjects.push(tmpObj);
		for (const index of self.arrIndexFields){
			if (tmpObj.getAttribute(index)!==""){
				var idxValue=tmpObj.getAttribute(index);
				if (typeof self.arrIndexes[index]==="undefined"){
					self.arrIndexes[index]=new Map();
				}
				var mapObjs=self.arrIndexes[index];
				if (!mapObjs.has(idxValue)){
					mapObjs.set(idxValue,[]);
				}
				var arrObjs=mapObjs.get(idxValue);
				arrObjs.push(tmpObj);
			}
		};
		return tmpObj;
	}
	
	remove(tags,theId){
		var self=this;
		debugger;
		for (var i=(self.arrObjects.length-1);i>=0;i--){
			if (self.arrObjects[i].id==theId){
				self.arrObjects.splice(i,1);
			}
		}
		for (var i=0;i<self.arrIndexFields.length;i++){
			var indexName=self.arrIndexFields[i];
			var mapObjs=self.arrIndexes[indexName];
			for (const mapKey of mapObjs.keys()) {
				debugger;
			  	var arrAux=mapObjs.get(mapKey);
			  	for (var j=(arrAux.length-1);j>=0;j--){
					if (arrAux[j].id==theId){
						arrAux.splice(j, 1); // 2nd parameter means remove one item only
					}	  
				}
				if (arrAux.length==0) {
					mapObjs.delete(mapKey);
				}
			}
		};
	}
	async clear(){
		var self=this;
		self.arrObjects=[];
		for (var i=0;i<self.arrIndexFields.length;i++){
			var indexName=self.arrIndexFields[i];
			self.arrIndexes[indexName]=new Map();
		};
	}

	
	addFromJson(tags,jsonObject){
		var self=this;
		var newObject;
		if (Array.isArray(jsonObject)){
			newObject=[];
			//console.log("Adding from jason.... is array");
			for (const jsonElement of jsonObject){
				var auxObj=self.newListObject(tags,jsonElement);
				newObject.push(auxObj);
			};
		} else {
			//console.log("Adding from jason.... is object:"+JSON.stringify(jsonObject));
			newObject=self.newListObject(tags,jsonObject);
		}
		if (typeof jsonObject.id!=="undefined"){
			var theId=jsonObject.id;
			if (self.extendedWaiting.has(theId)){
				debugger;
				var arrWaiting=self.extendedWaiting.get(theId);
				self.extendedWaiting.delete(theId); 
				var oSourceObject=self.getById(theId);
				for (const waiting of arrWaiting){
					self.extendFromJson(oSourceObject,waiting.tags,waiting.json);
				};
			}
		}
		return newObject;
	}
	extendFromJson(oSource,tags,jsonObject){
		var self=this;
		var clonedJsonSource=lodash.cloneDeep(oSource["_jsonSourceObject"]);
		for (const attribute of Object.keys(jsonObject)){
			if (attribute!=="_jsonSourceObject"){
				clonedJsonSource[attribute]=jsonObject[attribute];
			}
		};
		clonedJsonSource["_jsonSourceObject"]=undefined;
		return self.addFromJson(tags,clonedJsonSource);
	}
	async step_addFunctionFromString(defFunction){
		//console.log("Adding function to objects and sample");
		var self=this;
		var sFunction=await ScStore.processExpression(defFunction);
		var fnc = new Function("return ("+sFunction+")")();
		var fncName=fnc.name;
		self.arrFunctions.push({name:fncName,function:fnc});
		if (self.isFactory){
			if ((typeof self.sample!=="undefined")&&(self.sample!="")){
				self.sample[fncName]=fnc;
			}
		}
		self.arrObjects.forEach(function(actObjec){
			actObjec[fncName]=fnc;
		});
	}	
	
	async step_addNewObject(jsonValues,theId){
		var self=this;
		//console.log("Adding storable object "+self.type+" "+ jsonValues.substring(100)+"...");
		var objTags=ScStore.getActualTags();
		var oInputJson=await ScStore.processExpression(jsonValues);
		if (typeof theId!=="undefined"){
			oInputJson.id=await ScStore.processExpression(theId);
		}
		return self.addFromJson(objTags,oInputJson);
	}
	async step_removeObject(theId){
		var self=this;
		//console.log("Remove storable object "+ theId);
		var objTags=ScStore.getActualTags();
		var sId=await ScStore.processExpression(theId);
		self.remove(objTags,sId);
	}
	async step_addNewTypedExtendedObject(theSourceId,jsonValues,theId){
		var self=this;
		//console.log("Adding storable object "+self.type+" extended of "+ theSourceId+"  "+ jsonValues.substring(100)+"...");
		var objTags=ScStore.getActualTags();
		var oSrcId=await ScStore.processExpression(theSourceId);
		var oInputJson=await ScStore.processExpression(jsonValues);
		if (typeof theId!=="undefined"){
			oInputJson.id=await ScStore.processExpression(theId);
		}
		var srcObj=self.getById(oSrcId);
		if ((typeof srcObj!=="undefined")&&(srcObj!=="")){
			self.extendFromJson(srcObj,objTags,oInputJson);
		} else {
			var arrWaiting=[];
			if (!self.extendedWaiting.has(oSrcId)){
				self.extendedWaiting.set(oSrcId,arrWaiting);
			}
			arrWaiting=self.extendedWaiting.get(oSrcId);
			arrWaiting.push({tags:objTags,json:oInputJson});
		}
	}
	
	
	async step_extractAttributes(keyAttrib,keyValue){
		var self=this;
		return await ScStore.getClosures().fncVariableOperation(keyAttrib,keyValue,
			async function(sKeyAttrib,sKeyValue){
				return self.extractAttributes(sKeyAttrib,sKeyValue);
			});

	}
	async step_assignAttributeToVariable(tgtVariableName,attName,idxField,idxValue){
		var self=this;
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,attName,idxField,idxValue,
			async function(pVariableName,pAttName,sIdxField,sIdxValue){
				try {
					debugger;
					var srcObj=self.getBestMatch(sIdxField,sIdxValue);
					var value=srcObj[pAttName];
					return ScStore.getClosures().assignOrCreateVar(pVariableName,value);
				} catch (theError){
					debugger;
					console.log ("Error");
					console.log(theError);
					return "";
				}
			});

	}
	async step_assignCountToVariable(tgtVariableName){
		var self=this;
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,
			async function(pVariableName){
				try {
					debugger;
					var value=self.arrObjects.length;
					return ScStore.getClosures().assignOrCreateVar(pVariableName,value);
				} catch (theError){
					debugger;
					console.log ("Error");
					console.log(theError);
					return "";
				}
			});

	}
	async getMaxValue(sAttName){
		var self=this;
		var attName=sAttName;
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,
			async function(){
				try {
					debugger;
					var vMax="";
					self.arrObjects.forEach(function getMax(obj){
						var vVal=obj[attName];
						if ((typeof vVal!=="undefined") && (vVal!=="")){
							if (vMax!=""){
								if (vMax<vVal){
									vMax=vVal;
								}
							} else {
								vMax=vVal;
							}
						}
					});
					return vMax;
				} catch (theError){
					debugger;
					console.log ("Error");
					console.log(theError);
					return "";
				}
			});
	}

	async step_assignAllIDsToArrayVariable(tgtVariableName,whereAttr,whereVal){
		var self=this;
		var arrValue=[];
		var mapObjs=self.arrIndexes["id"];
		var noSpecialWhereValue=ScStore.removeSpecialChars(whereVal);
		for (const mapKey of mapObjs.keys()) {
			var bAddToResult=false;
			if (typeof whereAttr!=="undefined"){
			  	var arrAux=mapObjs.get(mapKey);
			  	for (var j=(arrAux.length-1);j>=0;j--){
					  var auxObj=arrAux[j];
					  var noSpecialObjectVal=ScStore.removeSpecialChars(auxObj[whereAttr]);
					  if (noSpecialObjectVal==noSpecialWhereValue){
						  bAddToResult=true;
					  }
				}
			} else {
				bAddToResult=true;
			}
			if (bAddToResult) arrValue.push(mapKey+"");
		}
		
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,
			async function(pVariableName){
				try {
					debugger;
					return ScStore.getClosures().assignOrCreateVar(pVariableName,arrValue);
				} catch (theError){
					debugger;
					console.log ("Error");
					console.log(theError);
					return "";
				}
			});

	}	
	
	async step_assignMethodResultToVariable(tgtVariableName,fncName,idxField,idxValue){
		var self=this;
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,fncName,idxField,idxValue,
			async function(pVariableName,pAttName,sIdxField,sIdxValue){
				var srcObj=self.getBestMatch(sIdxField,sIdxValue);
				var theFnc=srcObj[pAttName];
				var value=await theFnc();
				return ScStore.getClosures().assignOrCreateVar(pVariableName,value);
			});

	}
	async step_assignToField(value,fieldName,idxFiel,idxValue){
		var self=this;
		return await ScStore.getClosures().fncVariableOperation(value,fieldName,idxFiel,idxValue,
			async function(value,fieldName,sIdxField,sIdxValue){
				var srcObj=self.getBestMatch(sIdxField,sIdxValue);
				srcObj[fieldName]=value;
			});

	}
	
	async step_assignCloneToVariable(tgtVariableName,idxField,idxValue){
		var self=this;
		debugger;
		return await ScStore.getClosures().fncVariableOperation(tgtVariableName,idxField,idxValue,
			async function(pVariableName,sIdxField,sIdxValue){
				var srcObj=self.getBestMatch(sIdxField,sIdxValue);
				var tgtObj=self.cloneObject(srcObj);
				ScStore.getClosures().assignOrCreateVar(pVariableName,tgtObj);
			});
	}
	async step_createObjectsFromTable(theTable,arrAttributes,bFactory,sExtraValue,sExtraAttr,sIdxField,sIdxValue,autoId){
		var self=this;
		debugger;
		//the Table is object {headers:[], rows:[]}
		var tags=ScStore.getActualTags();
		var oRows=theTable.value.rows;
		if (typeof oRows==="undefined"){
			oRows=theTable.value;
		}
		var srcObj;
		if (!bFactory) {
			srcObj=self.getBestMatch(sIdxField,sIdxValue);
		} else {
			if (self.isFactory){
				srcObj=self.sample;
			} else {
				throw new Error('Storable Object List '+self.type +' is not a Factory!');
			}
		}
		var arrAttrs=arrAttributes;
		
		var theExtraValue=sExtraValue;
		var theExtraAttr=sExtraAttr;

		async function processRow(data,iRow){
			var tgtObj=self.cloneObject(srcObj);
			if (typeof autoId!=="undefined"){
				if (autoId) {
					tgtObj.id=uuidv4();
				}
			}
			// have the clone.. now fill the attributes
			for (var i=0;i<arrAttrs.length;i++){
				if (i<data.length){
					var dValue=data[i];
					if ((typeof dValue !=="undefined" )&&(arrAttrs[i]!=="-SKIP-")){
						tgtObj[arrAttrs[i]]=dValue;
					}
				}
			}
			if (typeof theExtraValue!=="undefined"){
				tgtObj[theExtraAttr]=theExtraValue;
			}
			tgtObj.tblRow=""+iRow;
			self.newListObject(tags,tgtObj);
		}
		for (var i=0;i<oRows.length;i++){
			await processRow(oRows[i],i);
		}
	}
	cloneObject(srcObj){
		var self=this;
		debugger;
		var fncCustomClone=function(value){
			if (typeof value.type!=="undefined"){
				var objType=value.type;
				var objId=value.id;
				if (!((value.type==srcObj.type)&&(value.id==srcObj.id))){
					debugger;
					var internalObj=objectStorageManager.get(objType,"id",objId);
					var internalClon=lodash.cloneDeepWith(internalObj,fncCustomClone);
					return internalClon;
				} 
			} 
		}
		var tgtObj=lodash.cloneDeepWith(srcObj,fncCustomClone);
		return tgtObj;
	}
	async createNewObject(sID,bStore=true){
		var self=this;
		debugger;
		var tags=ScStore.getActualTags();
		var srcObj;
		if (self.isFactory){
			srcObj=self.sample;
		} else {
			throw new Error('Storable Object List '+self.type +' is not a Factory!');
		}
		var tgtObj=self.cloneObject(srcObj);
		tgtObj.id=sID;
		if (bStore){
			return self.newListObject(tags,tgtObj);
		}
		return tgtObj;
	}
	
	
	async step_createClon(sIdSrc,sIdDst){
		var self=this;
		debugger;
		var tags=ScStore.getActualTags();
		var srcObj=self.getBestMatch("id",sIdSrc);
		var tgtObj=self.cloneObject(srcObj);
		tgtObj.id=sIdDst;
		return self.newListObject(tags,tgtObj);
	}	
	async step_createNewObject(sIdDst){
		var self=this;
		return await self.createNewObject(sIdDst);
	}
	async step_addIndex(indexName){
		var self=this;
		var sIndex=await ScStore.processExpression(indexName);
		if (typeof self.arrIndexes[sIndex]!=="undefined") {
			throw "index " + sIndex + " ("+indexName+") yet exists";  
		} 
		self.addIndexField(sIndex);
	}
	
	async step_applyActions(arrActions,refObject){
		var self=this;
		for (const postAction of arrActions){
			for (const auxObj of self.arrObjects){
				if (postAction.type=="setAttribute"){
					var oSource=postAction.source;
					var oDest=postAction.destination;
					var value=await refObject.getValueByDefinition(oSource);
					await auxObj.setValueByDefinition(oDest,value);
				}
			}
		}
	}
}

var ObjectStorageManager=class ObjectStorageManager{
	constructor(){
		var self=this;
		self.objectLists=new Map();
	}
	
	getListManager(objType){
		var self=this;
		var sTypeName=ScStore.capitalize(objType);
		if (typeof self["load"+sTypeName+"ListManager"]!=="undefined"){
			return self["load"+sTypeName+"ListManager"]();
		}
		return new ObjectList(objType);
	}
	getList(objType){
		var self=this;
		var objList;
		if (!self.objectLists.has(objType)){
			objList=self.getListManager(objType);
			self.objectLists.set(objType,objList);
		} else {
			objList=self.objectLists.get(objType);
		}
		return objList;
	}
	add(objType,tags,jsonObject){
		var self=this;
		var objList=self.getList(objType);
		objList.addFromJson(tags,jsonObject);
	}
	
	

	getAll(objType,idxFieldName,idxValue){
		var self=this;
		var objList=self.getList(objType);
		return objList.getAll(idxFieldName,idxValue);
	}
	get(objType,idxFieldName,idxValue){
		var self=this;
		var objList=self.getList(objType);
		return objList.get(idxFieldName,idxValue);
	}
	getBestMatch(objType,idxFieldName,idxValue){
		var self=this;
		var objList=self.getList(objType);
		return objList.getBestMatch(idxFieldName,idxValue);
	}
	async extractAttributes(objType,keyAttrib,keyValue){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		objList.extractAttributes(keyAttrib,keyValue);
	}
	async step_addNewTypedObject(sObjectType,jsonValues,theId){
  		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		var oAdded=await objList.step_addNewObject(jsonValues,theId);
		if (typeof oAdded.datasources!=="undefined"){
			objList.datasources=oAdded.datasources;
			delete oAdded.datasources;
		}		
		return oAdded;
	}
	
	async step_addNewObjectFactory(sObjectType,jsonValues){
  		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		objList.isFactory=true;
		var sample=await objList.step_addNewObject(jsonValues,"sample");
		objList.remove("","sample");
		if (typeof sample.datasources!=="undefined"){
			objList.datasources=sample.datasources;
			delete sample.datasources;
		}	
		objList.sample=sample;
		
	}

	async step_addFunctionToObjects(sObjectType,sFunction){
  		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		await objList.step_addFunctionFromString(sFunction);
	}	

	async step_removeTypedObject(sObjectType,theId){
		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		return await objList.step_removeObject(theId);
	}
	async step_clearList(sObjectType){
		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		return await objList.clear();
	}
	
	async step_addNewTypedExtendedObject(sObjectType,theSourceId,jsonValues,theId){
		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=self.getList(sObjType);
		return await objList.step_addNewTypedExtendedObject(theSourceId,jsonValues,theId);
	}
	
	async step_extractAttributes(objType,keyAttrib,keyValue){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_extractAttributes(keyAttrib,keyValue);

	}
	async step_assignAttributeToVariable(objType,tgtVariableName,attName,idxField,idxValue){
		var self=this;
		debugger;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_assignAttributeToVariable(tgtVariableName,attName,idxField,idxValue);

	}
	async step_assignToField(objType,varName,fieldName,idxFiel,idxValue){
		var self=this;
		debugger;
		var sObjType=await ScStore.processExpression(objType);
		var sValue=await ScStore.processExpression(varName);
		var objList=self.getList(sObjType);
		return await objList.step_assignToField(sValue,fieldName,idxFiel,idxValue);

	}
	
	
	async step_assignMethodResultToVariable(objType,tgtVariableName,fncName,idxField,idxValue){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_assignMethodResultToVariable(tgtVariableName,fncName,idxField,idxValue);
	}
	async step_assignCloneToVariable(objType,tgtVariableName,idxField,idxValue){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_assignCloneToVariable(tgtVariableName,idxField,idxValue);
	}
	async step_createObjectsFromTable(objTable,objType,lstAttributes,bFactory,extraValue,extraAttr,idxName,idxValue,autoId){
		var self=this;
		debugger;
		//console.log("use table {string} to create clones of object {string} wich id is {string} filling attributes {string}");
		var theTableVarName=await ScStore.processExpression(objTable); 
		
		var theTable=ScStore.getVar(theTableVarName);
		var sObjType=await ScStore.processExpression(objType);
		var sIdxName=await ScStore.processExpression(idxName);
		var sIdxValue=await ScStore.processExpression(idxValue);
		var sExtraValue=await ScStore.processExpression(extraValue);
		var sExtraAttr=await ScStore.processExpression(extraAttr);
		
		var arrAttributes=await ScStore.processExpression(lstAttributes);
		arrAttributes=arrAttributes.split(",")
		var objList=self.getList(sObjType);
		var tblResult=await objList.step_createObjectsFromTable(theTable,arrAttributes,bFactory,sExtraValue,sExtraAttr,sIdxName,sIdxValue,autoId);
		return tblResult;
	}
	async step_createClon(objType,idSrc,idDst){
		var self=this;
		debugger;
		var sObjType=await ScStore.processExpression(objType);
		var sIdSrc=await ScStore.processExpression(idSrc);
		var sIdDst=await ScStore.processExpression(idDst);
		var objList=self.getList(sObjType);		
		var theResult=await objList.step_createClon(sIdSrc,sIdDst);
		return theResult;
		
	}
	async step_createNewObject(objType,idDst){
		var self=this;
		debugger;
		var sObjType=await ScStore.processExpression(objType);
		var sIdDst=await ScStore.processExpression(idDst);
		var objList=self.getList(sObjType);		
		var theResult=await objList.step_createNewObject(sIdDst);
		return theResult;
	}
	
	async step_startLoopForEach(auxVarName,objType){
		var self=this;
		debugger;
		var sObjType=await ScStore.processExpression(objType);
		var sAuxVarName=await ScStore.processExpression(auxVarName);
		var objList=self.getList(sObjType);
		await ScStore.BranchingSteps.executeLoopForEach(sAuxVarName,objList.arrObjects);
		
	}
	
	
	async step_assignCountToVariable(tgtVariableName,objType){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_assignCountToVariable(tgtVariableName);	
	}	
	async step_assignMaxToVariable(tgtVariableName,attName,objType){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var sAttName=await ScStore.processExpression(attName);
		var objList=self.getList(sObjType);
		var vMaxValue = await objList.getMaxValue(sAttName);
		ScStore.getClosures().assignOrCreateVar(tgtVariableName,vMaxValue);	
	}
	
	async internal_getDatasourceParamsFromObject(oSourceObject,oDataSource){
		var self=this;
		//oResult is a clon
		var oResult=JSON.parse(JSON.stringify(oDataSource));
 		var oParams=oResult.params;
 		delete oResult.params;
		for (const pName in oParams){
			var pValue=oParams[pName];
			var auxValue=await oSourceObject.getValueByDefinition(pValue);
			oParams[pName]=auxValue;
		}	
		oResult.datasourceCallParams=oParams;
		var condsResult=true;
		if ((typeof oResult.skip!=="undefined") && ((oResult.skip==true) ||(oResult.skip=="true"))){  
			condsResult=false;
		} else if (typeof oResult.conditional!=="undefined"){
			var arrConditions=oResult.conditional;
			delete oResult.conditional;
			if (!Array.isArray(arrConditions)){
				arrConditions=[arrConditions];
			}
			for (const oCondition of arrConditions){
				if (oCondition.type==="attributeValue")
					var auxValue=await oSourceObject.getValueByDefinition(oCondition.source);
					var chkValue=await ScStore.processExpression(oCondition.value);
					var op=await ScStore.processExpression(oCondition.operation);
					if (op==="eq"){
						debugger;
						condsResult=(condsResult && (auxValue===chkValue));						
					}
			}
		}
		oResult.conditionalExecution=condsResult;
		return oResult;
	}
	async step_assignDatasourceToVariable(tgtVariableName,dsName,theObject){
		var self=this;
		debugger;
		var oObject=await ScStore.processExpression(theObject);
		var sVarName=await ScStore.processExpression(tgtVariableName);
		var sDatasourceName=await ScStore.processExpression(dsName);
		var objList=self.getList(oObject.type);
		var oDataSource = objList.datasources[sDatasourceName];
		var oResult=await self.internal_getDatasourceParamsFromObject(oObject,oDataSource);
		var closureManager=ScStore.getClosures();
		return await closureManager.fncVariableOperation(sVarName,oResult,closureManager.assignOrCreateVar);
	}
	async step_assignDatasourcesToVariable(tgtVariableName,objType){
		var self=this;
		debugger;
		var sObjectType=await ScStore.processExpression(objType);
		var sVarName=await ScStore.processExpression(tgtVariableName);
		var objList=self.getList(sObjectType);
		var oDsList=[];
		for (const dsName in objList.datasources){
			oDsList.push(dsName);			
		}
		var closureManager=ScStore.getClosures();
		return await closureManager.fncVariableOperation(sVarName,oDsList,closureManager.assignOrCreateVar);
	}

	async step_extractDatasource(dsName,theObject){
		var self=this;
		debugger;
		var oObject=await ScStore.processExpression(theObject);
		var sDatasourceName=await ScStore.processExpression(dsName);
		var objList=self.getList(oObject.type);		
		var oDataSource = objList.datasources[sDatasourceName];
		var oResult=await self.internal_getDatasourceParamsFromObject(oObject,oDataSource);
		var closureManager=ScStore.getClosures();
		await closureManager.fncVariableOperation(oResult.datasourceCallParams,closureManager.extractAttributes);
		return await closureManager.fncVariableOperation(oResult,closureManager.extractAttributes);
	}
	
	async step_navigateToDatasource(dsName,theObject){
		var self=this;
		debugger;
		var oObject=await ScStore.processExpression(theObject);
		var sDatasourceName=await ScStore.processExpression(dsName);
		var objList=self.getList(oObject.type);		
		var oDataSource = objList.datasources[sDatasourceName];
		var oResult=await self.internal_getDatasourceParamsFromObject(oObject,oDataSource);
		var closureManager=ScStore.getClosures();
		await closureManager.fncVariableOperation(oResult.datasourceCallParams,closureManager.extractAttributes);
		return await closureManager.fncVariableOperation(oResult,closureManager.extractAttributes);
	}
	
		
	async step_assignAllIDsToArrayVariable(tgtVariableName,objType,whereAttr,whereVal){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		var sWhereAttr=await ScStore.processExpression(whereAttr);
		var sWhereVal=await ScStore.processExpression(whereVal);
		return await objList.step_assignAllIDsToArrayVariable(tgtVariableName,sWhereAttr,sWhereVal);	
	}
	

	
	async step_initializeWithClone(objType,tgtVariableName,idxField,idxValue){
		var self=this;
		debugger;
		var tgtVarName=await ScStore.processExpression(tgtVariableName);
		if (!ScStore.existsVar(tgtVarName)){
			if (tgtVarName.charAt(0)!=='$'){
				tgtVarName="$"+tgtVarName;
			}
			var jsonDefVars={};
			jsonDefVars[tgtVarName]="";
			var closureManager=ScStore.getClosures();
			await closureManager.fncVariableOperation(JSON.stringify(jsonDefVars),closureManager.setDefaultVariables);
			await self.step_assignCloneToVariable(objType,tgtVariableName,idxField,idxValue);
			debugger;
		}
	}
	async step_applyActions(sPostActions,objType,refObject){
		var self=this;
		var oPostActions=await ScStore.processExpression(sPostActions);
		if ((oPostActions!==sPostActions)&&(typeof oPostActions==="string")){
			console.log("there is not postactions");
			return ;
		}
		var sObjType=await ScStore.processExpression(objType);
		var oRefObject=await ScStore.processExpression(refObject);		
		if (!Array.isArray(oPostActions)){
			oPostActions=[oPostActions];
		}
		var objList=self.getList(sObjType);
		return await objList.step_applyActions(oPostActions,oRefObject);
	}	
	async step_addIndexToObject(objType,fieldName){
		var self=this;
		var sObjType=await ScStore.processExpression(objType);
		var objList=self.getList(sObjType);
		return await objList.step_addIndex(fieldName);
	}

}

var objectStorageManager=new ObjectStorageManager();
module.exports={manager:objectStorageManager,StorableObjectManagerClass:ObjectStorageManager,StorableObjectListClass:ObjectList,StorableObjectClass:StorableObject};
