const manager = require('./xlsx_extension.lib');
const ScStore = require('../../scenarioStore');

//const fncGet=require('./run.steps.js').getExecuteFeatures;
//Defining more steps (has to be in a .stepdef.js file)
const stepDefinitions = [{
    stepMethod: 'Given',
    stepPattern: 'open xlsx file {string}',
    stepTimeout: 40000,
    stepFunction: async function(sFileId){
	  console.log("Open xlsx file:"+sFileId);
	  await manager.step_openFile(sFileId);
      return;
    }
  }
];


module.exports = stepDefinitions;
