const ScStore = require('../../scenarioStore');
const XLSX = require("xlsx");
const {resolve} = require('path');


// stderr is sent to stderr of parent process
// you can set options.stdio if you want it to go elsewhere
var XlsxDriver=class XlsxDriver{
	constructor(){
		var self =this;
		self.lastError=0;
		self.lastResult="";
	}
	async step_openFile(fileId){
		var self=this;
		var sFileId=await ScStore.processExpression(fileId);
		var oFile=ScStore.getFiles().getById(sFileId);
		var fileName=oFile.filename;
		var relativePath=oFile.relativePath;
		var basePath=oFile.basePath;
		var auxPath="";
		if (typeof basePath!=="undefined"){
			if (basePath.substring(0,1)=='.'){
				auxPath=resolve(".")+"/"+basePath;
				console.log("basePath:"+auxPath);
			}
		}
		if (relativePath!=="undefined"){
			auxPath+=relativePath;
		}
		auxPath+=fileName;
		console.log("complete Path:"+auxPath);
		const workbook = XLSX.readFile(auxPath);
		const sheet_name_list = workbook.SheetNames;
		console.log(sheet_name_list.toString());		
		debugger;
		let first_sheet_name = workbook.SheetNames[0];
		let worksheet = workbook.Sheets[first_sheet_name];
		let cell = worksheet['D4']
		if (typeof cell=="undefined"){
			XLSX.utils.sheet_add_aoa(worksheet, [['NEW VALUE from NODE']], {origin: 'D4'});
		} else {
			cell.v='NEW VALUE from NODE';
		}
		XLSX.writeFile(workbook, '/tmp/test2.xls');
		
	}
}
var xlsxDriver=new XlsxDriver();

module.exports = xlsxDriver;
