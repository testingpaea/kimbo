const ScStore = require('../../scenarioStore');
const OtherWorld = require('../../OtherWorld');

var EventManager=class EventManager{
	constructor(){
		var self=this;
		self.callbacks={};
		self.enabled=true;
		self.scenarioLevel=0;
	}
	addEvent(eventName,callback){
		var self=this;
		self.callbacks[eventName]=callback;
	}
	async call(callName,callParams){
		var self=this;
		if (typeof self.callbacks[callName]!=="undefined"){
			return await self.callbacks[callName](callParams);
		} 
		return "";
	}
	async finishedAll(){
		debugger;
		var self=this;
		var bIsCompiling=OtherWorld.getCompiling();
		//console.log("Event Finisihed All (Level "+self.scenarioLevel+") was compiling:"+bIsCompiling);
		var rootList=[];
		if (bIsCompiling){
		   rootList=ScStore.getRootScenarios();
		} else {
		   ScStore.traceExecutionTree();
		}
		var results=ScStore.getExecutionExtractInfo(3);
		return await self.call("finishedAll",{compiling:bIsCompiling,scenarios:rootList,results:results});
	}
	async finished(scenarioTag){
		var self=this;
		debugger;
		self.scenarioLevel--;
		//console.log("Event Finisihed "+self.scenarioLevel+"):"+scenarioTag);
		await self.call("finished",[scenarioTag]);
		/*if (self.scenarioLevel==0){
			await self.finishedAll();
		}*/
	}
	async started(scenarioTag){
		var self=this;
		//console.log("Event Started "+self.scenarioLevel+"):"+scenarioTag);
		self.scenarioLevel++;
		return await self.call("started",[scenarioTag]);
	}
}


var eventManager=new EventManager();
module.exports=eventManager;