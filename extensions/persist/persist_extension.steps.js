const ScStore = require('../../scenarioStore');

const stepDefinitions = [
	  { stepMethod: 'Given', stepPattern: 'add persistent storable object {string} with id {string} and values from json',
        stepFunction: async function(objType,objId,inputJson){
			//console.log("Adding persistent storable object "+objType+" "+objId +" "+ inputJson);
			debugger;
			await ScStore.getPersist().step_addNewTypedObject(objType,inputJson,objId);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'add persistent storable object factory {string} with this sample',
	  										 
        stepFunction: async function(objType,inputJson){
			//console.log("Adding persistent storable object factory "+objType+" "+ inputJson);
			debugger;
			await ScStore.getPersist().step_addNewObjectFactory(objType,inputJson);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'persist object {string} with id {string}',
        stepFunction: async function(objType,objId){
			//console.log("Persisting storable object "+objType +" with id "+objId);
			debugger;
			await ScStore.getPersist().step_persist(objType,objId);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'ensure storage and persist {string}',
        stepFunction: async function(theObject){
			//console.log("Persisting storable object instance "+theObject);
			debugger;
			await ScStore.getPersist().step_persistObject(theObject,true);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'persist {string}',
        stepFunction: async function(theObject){
			//console.log("Persisting storable object instance "+theObject);
			debugger;
			await ScStore.getPersist().step_persistObject(theObject,false);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'persist object {string} with id {string} using provider {string}',
        stepFunction: async function(objType,objId,providerId){
			//console.log("Persisting storable object "+objType +" with id "+objId + " using provider "+providerId);
			debugger;
			await ScStore.getPersist().step_persist(objType,objId,providerId);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'persist all objects {string}',
        stepFunction: async function(objType){
			//console.log("Persisting all storable object "+objType);
			debugger;
			await ScStore.getPersist().step_persistAll(objType,"");
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'persist all objects {string} using provider {string}',
        stepFunction: async function(objType,providerId){
			//console.log("Persisting all storable object "+objType +" using provider:"+ providerId);
			debugger;
			await ScStore.getPersist().step_persistAll(objType,providerId);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'remove storage for objects {string}',
        stepFunction: async function(objType){
			//console.log("Remove the storage (table, file, etc) for objects "+objType);
			debugger;
			await ScStore.getPersist().step_removeStorage(objType);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'clear storage of objects {string}',
        stepFunction: async function(objType){
			//console.log("Clear the storage (table, file, etc) for objects "+objType);
			debugger;
			await ScStore.getPersist().step_clearStorage(objType);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'remove storage for objects {string} using provider {string}',
        stepFunction: async function(objType,providerId){
			//console.log("Remove the storage (table, file, etc) for objects "+objType +" using provider:"+ providerId);
			debugger;
			await ScStore.getPersist().step_removeStorage(objType,providerId);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'create storage for objects {string}',
        stepFunction: async function(objType){
			//console.log("Create the storage (table, file, etc) for objects "+objType);
			debugger;
			await ScStore.getPersist().step_createStorage(objType);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'create storage for objects {string} using provider {string}',
        stepFunction: async function(objType,providerId){
			//console.log("Create the storage (table, file, etc) for objects "+objType +" using provider:"+ providerId);
			debugger;
			await ScStore.getPersist().step_createStorage(objType,providerId);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'load objects {string}',
        stepFunction: async function(objType){
			//console.log("load objects "+objType +" from default provider");
			debugger;
			await ScStore.getPersist().step_loadObjects(objType);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'load objects {string} from query {string} with params {string}',
        stepFunction: async function(objType,queryId,sParams){
			//console.log("load objects "+objType +" from query id "+queryId);
			debugger;
			await ScStore.getPersist().step_loadObjectsByQuery(objType,queryId,sParams);
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'save in {string} the result of query {string} over {string} with params {string}',
        stepFunction: async function(tgtVariableName,queryId,objType,sParams){
			////console.log("save in "+tgtVariableName+" the result of query id "+queryId+" over "+objType);
			debugger;
			var oResult=await ScStore.getPersist().step_executeQuery(objType,queryId,sParams);
			var sVarName=await ScStore.processExpression(tgtVariableName);
			ScStore.getClosures().assignOrCreateUpperVar(sVarName,oResult);	
	  	}
	  },{ stepMethod: 'Given', stepPattern: 'save in {string} the result of query {string} over {string} with params',
        stepFunction: async function(tgtVariableName,queryId,objType,sParams){
			debugger;
			////console.log("save in "+tgtVariableName+" the result of query id "+queryId+" over "+objType);
			var oResult=await ScStore.getPersist().step_executeQuery(objType,queryId,sParams);
			debugger;
			var sVarName=await ScStore.processExpression(tgtVariableName);
			ScStore.getClosures().assignOrCreateUpperVar(sVarName,oResult);
		}
	  },{ stepMethod: 'Given', stepPattern: 'load objects {string} from query {string} with params',
        stepFunction: async function(objType,queryId,sParams){
			//console.log("load objects "+objType +" from query id "+queryId);
			debugger;
			await ScStore.getPersist().step_loadObjectsByQuery(objType,queryId,sParams);
	  	}
	  	
	  },{ stepMethod: 'Given', stepPattern: 'load objects {string} from provider {string}',
        stepFunction: async function(objType,providerId){
			//console.log("load objects "+objType +" from provider " + providerId);
			debugger;
			await ScStore.getPersist().step_loadObjects(objType,providerId);
	  	}	  	
	  },{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the max attribute {string} value of persistent objects {string}', 
		stepFunction: async function(tgtVariableName,attName,objType){
			return await ScStore.getPersist().step_assignPersistentMaxToVariable(tgtVariableName,attName,objType);

		}
	  },{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the storename for persistent objects {string}', 
		stepFunction: async function(tgtVariableName,objType){
			return await ScStore.getPersist().step_assignStorenameToVariable(tgtVariableName,objType);

		}
	  	
	  }

	  
];
module.exports=stepDefinitions;