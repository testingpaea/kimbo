const ScStore = require('../../scenarioStore');
ScStore.enableExtension("storable");
ScStore.enableExtension("providers");
var providerManager=ScStore.getProviders().manager;		

 
var persistenceManager=class persistenceManager {
	constructor(){
	}
	async step_addNewTypedObject(sObjectType,jsonValues,theId){
  		var self=this;
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=ScStore.getStorable().manager.getList(sObjType);

		var objTags=ScStore.getActualTags();
		var oInputJson=await ScStore.processExpression(jsonValues);
		if (typeof theId!=="undefined"){
			oInputJson.id=await ScStore.processExpression(theId);
		}
		if (typeof oInputJson.persistence!=="undefined"){
			objList.persistence=oInputJson.persistence;
			delete oInputJson.persistence;
		}		
		if (typeof oInputJson.datasources!=="undefined"){
			objList.datasources=oInputJson.datasources;
			delete oInputJson.datasources;
		}	
		
		objList.addFromJson(objTags,oInputJson);
	}
	
	async step_addNewObjectFactory(sObjectType,jsonValues){
  		var self=this;
  		debugger;
  		await ScStore.getStorable().manager.step_addNewObjectFactory(sObjectType,jsonValues)
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=ScStore.getStorable().manager.getList(sObjType);
		var sample=objList.sample;
		if (typeof sample.persistence!=="undefined"){
			objList.persistence=sample.persistence;
			delete sample.persistence;
		}
		if (typeof sample.datasources!=="undefined"){
			objList.datasources=sample.datasources;
			delete sample.datasources;
		}	
	}
	
	 
	
	async step_getProvider(sObjectType,sProvider){
		var sObjType=await ScStore.processExpression(sObjectType);
		var objList=ScStore.getStorable().manager.getList(sObjType);
		var sProviderId;
		var oPersistence=objList.persistence;
		if (typeof oPersistence==="undefined"){
			throw new Error('The Storable Object is not persistent');
		}
		if ((sProvider==="")||(typeof sProvider==="undefined")){
			sProviderId=await ScStore.processExpression(oPersistence.provider);
		} else {
			sProviderId=await ScStore.processExpression(sProvider);
		}
		var theProvider=providerManager.providersList.getBestMatch(sProviderId);
		return {provider:theProvider,list:objList,persistence:oPersistence};		
	}	
	async step_persist(objType,objId,sProviderId){
  		var self=this;
		var sObjId=await ScStore.processExpression(objId);
  		var oProviderInfo=await self.step_getProvider(objType,sProviderId);
		await oProviderInfo.provider.persist(oProviderInfo.list,oProviderInfo.persistence,sObjId);
	}	
	async step_persistObject(theObject,bEnsureStorage){
  		var self=this;
		var oObject=await ScStore.processExpression(theObject);
  		var oProviderInfo=await self.step_getProvider(oObject.type);
  		var oProvider=oProviderInfo.provider;
  		if ((typeof bEnsureStorage!=="undefined")&&(bEnsureStorage)){
			await oProvider.ensureStorage(oProviderInfo.list,oProviderInfo.persistence,oObject);	  
		}
		await oProvider.persist(oProviderInfo.list,oProviderInfo.persistence,oObject);
	}	
	
	async step_persistAll(sObjectType,sProviderId){
  		var self=this;
  		var oProviderInfo=await self.step_getProvider(sObjectType,sProviderId);
		await oProviderInfo.provider.persistAll(oProviderInfo.list,oProviderInfo.persistence);
	}
	async step_loadObjects(sObjectType,sProviderId){
  		var self=this;
  		var oProviderInfo=await self.step_getProvider(sObjectType,sProviderId);
		await oProviderInfo.provider.loadObjects(oProviderInfo.list,oProviderInfo.persistence);
	}
	async step_loadObjectsByQuery(objType,queryId,oParams){
		var self=this;
  		var oProviderInfo=await self.step_getProvider(objType);
		await oProviderInfo.provider.loadObjectsByQuery(oProviderInfo.list,queryId,oParams);
		
	}
	async step_executeQuery(objType,queryId,oParams){
		var self=this;
  		var oProviderInfo=await self.step_getProvider(objType);
		var oResult=await oProviderInfo.provider.executeQuery(queryId,oParams);
		return oResult;
	}
	

	async step_removeStorage(sObjectType,sProvider){
  		var self=this;
  		debugger;
  		var oProviderInfo=await self.step_getProvider(sObjectType,sProvider);
		await oProviderInfo.provider.removeStorage(oProviderInfo.persistence.storename);
	}
	async step_clearStorage(sObjectType,sProvider){
  		var self=this;
  		debugger;
  		var oProviderInfo=await self.step_getProvider(sObjectType,sProvider);
		await oProviderInfo.provider.clearStorage(oProviderInfo.persistence.storename);
	}
	
	async step_createStorage(sObjectType,sProvider){
  		var self=this;
  		var oProviderInfo=await self.step_getProvider(sObjectType,sProvider);
		await oProviderInfo.provider.createStorage(oProviderInfo.list,oProviderInfo.persistence);
	}	
	async step_assignPersistentMaxToVariable(tgtVariableName,attName,objType){
		debugger;
		var self=this;
		var sObjectType=await ScStore.processExpression(objType);
		var sAttName=await ScStore.processExpression(attName);
  		var oProviderInfo=await self.step_getProvider(sObjectType);
		var vMaxValue = await oProviderInfo.provider.getMaxValue(sAttName,oProviderInfo.list,oProviderInfo.persistence);
		ScStore.getClosures().assignOrCreateVar(tgtVariableName,vMaxValue);	
	}
	async step_assignStorenameToVariable(tgtVariableName,objType){
		debugger;
		var self=this;
		var varName=await ScStore.processExpression(tgtVariableName);
		var sObjectType=await ScStore.processExpression(objType);
		var objList=ScStore.getStorable().manager.getList(sObjectType);
		var oPersistence=objList.persistence;
		var storeName=oPersistence.storename;
		return ScStore.getClosures().assignOrCreateUpperVar(varName,storeName);	
	}

	
}
var persistenceManager=new persistenceManager();
module.exports=persistenceManager;