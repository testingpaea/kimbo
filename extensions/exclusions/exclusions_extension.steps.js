const ScStore = require('kimbo/scenarioStore');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{ stepMethod: 'Given', stepTimeout: (1000*60*20),
		stepPattern: 'start critical section', 
		stepFunction: async function(){
			await ScStore.getExclusions().startLocalExclusionSection();
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'end critical section', 
		stepFunction: async function(){
			await ScStore.getExclusions().endLocalExclusionSection();
		}
	}
];

module.exports = stepDefinitions;
