const ScStore = require('kimbo/scenarioStore');

var ExclusionsManager=class ExclusionsManager{
	constructor(){
		var self=this;
		self.totalScreens=0;
		self.actualScreen=0;
		self.enabled=false;
	}
	setTotalScreens(totalScreens){
		this.totalScreens=totalScreens;
	}
	setActualScreen(actualScreen){
		this.actualScreen=actualScreen;
	}
	async startLocalExclusionSection(){
		var result="";
		if (ScStore.isExtensionLoaded("events")){
			if (ScStore.isExtensionLoaded("multiscreen")){
				await ScStore.getMultiscreen().openProgressBox("Waiting for exclusion starts");
			}
			result=await ScStore.getEvents().call("exclusion_start",{});
			
			if (ScStore.isExtensionLoaded("multiscreen")){
				await ScStore.getMultiscreen().closeProgressBox();
			}
		}
		return result;
	}
	async endLocalExclusionSection(){
		var result="";
		if (ScStore.isExtensionLoaded("events")){
			result=await ScStore.getEvents().call("exclusion_end",{});
		}
		return result;
	}
}


var exclusionsManager=new ExclusionsManager();
module.exports=exclusionsManager;