const ScStore = require('../../scenarioStore');
ScStore.enableExtension("events");
var CredentialsManager=class CredentialsManager{
	constructor(){
		var self=this;
		self.credentials=new Map();
	}
	async getCredentials(credentialId){
		var self=this;
		if (self.credentials.has(credentialId)){
			return self.credentials.get(credentialId);
		}
		return await self.pool.getConnection();
	}
}	

var credentialsManager=new CredentialsManager();
ScStore.getEvents().addEvent("getCredentials",async function(objCredentialNeeded){
	return await credentialsManager.getCredentials(objCredentialNeeded.id);
});
module.exports=credentialsManager;