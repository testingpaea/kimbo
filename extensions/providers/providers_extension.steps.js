const ScStore = require('../../scenarioStore');

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of call to method {string} of provider type {string} with id {string} with parameters',
		stepFunction: async function(variableName,sMethodName,providerType,providerId,sParameters){
			var callResult=await ScStore.getProviders().call(providerType,providerId,sMethodName,parameters);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'call to method {string} of provider type {string} with id {string} and parameters',
		stepFunction: async function(sMethodName,providerType,providerId,sParameters){
			var callResult=await ScStore.getProviders().call(providerType,providerId,sMethodName,parameters);
		}
		
	}
];
module.exports=stepDefinitions;