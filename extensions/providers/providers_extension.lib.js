const ScStore = require('../../scenarioStore');
ScStore.enableExtension("storable");
var StorableObjectClass=ScStore.getStorable().StorableObjectClass;
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var StorableObjectManagerClass=ScStore.getStorable().StorableObjectManagerClass;
  
var Provider=class Provider extends StorableObjectClass{
	constructor(tags,jsonObject){
		super(jsonObject.providerType,tags);
		var self=this;
		self.id=jsonObject.providerId;
		self.providerType=jsonObject.providerType;
		self.indirect=true;
		self.credentials="";
	}
	async call(methodId,parameters){
		var self=this;
		var oResult=await ScStore.getEvents().call("CALL TO PROVIDER METHOD",
					{
						providerType:self.providerType,
						providerId:self.id,
						providerMethod:methodId,
						parameters:parameters
					});	
	}
	async getConnection(){
		var self=this;
		debugger;
		if (((typeof self.credentials==="undefined")||(self.credentials==""))
			 &&(!self.indirect)){
			self.credentials=await ScStore.getEvents().call("getCredentials",{type:self.type,id:self.id});
			self.credentials=self.credentials.value;
		}
		//this method MUST BE EXTENDED IN PROVIDERS WICH USE CONNECTIONS...
		return "";
	}
	
}	
var DefaultProviderList=class DefaultProviderList extends StorableObjectListClass{
	constructor(objType){
		super(objType);
		var self=this;
		self.addIndexField("id");
	}
	newListObject(tags,jsonObject){
		var self=this;
		var tmpObj=new Provider(self.type,tags,jsonObject);
		tmpObj.loadFromJson(jsonObject);
		self.addListObject(tmpObj);
		return tmpObj;
	}
}

var DefaultProvidersManager=new DefaultProviderList();


var ProviderManager=class ProviderManager extends StorableObjectManagerClass{
	constructor(){
		super();
		var self=this;
		ScStore.mapNoRootTags.set("@Provider","Provider");
		self.providersList=DefaultProvidersManager;
	}
	async call(provType,provId,methodId,parameters){
		var self=this;
		var provider=self.getBestMatch(provType,provId);
		return await provider.call(methodId,parameters);
	}
}


var defaultProviderListManager=new DefaultProviderList("default");
var providerManager=new ProviderManager();
providerManager["loadDefaultListManager"]=function(){
	return defaultProviderListManager;
};

module.exports={manager:providerManager,ProviderManagerClass:ProviderManager,ProviderListClass:DefaultProviderList,ProviderClass:Provider};