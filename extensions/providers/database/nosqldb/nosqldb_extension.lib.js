const {v1: uuidv1,v4: uuidv4,} = require('uuid');

const ScStore = require('../../../../scenarioStore');
ScStore.enableExtension("providers");
ScStore.enableExtension("providers/database");

var DatabaseProvider=ScStore.getDatabase().DatabaseProviderClass;
var DatabaseProviderList=ScStore.getDatabase().DatabaseProviderListClass;
var databaseManager=ScStore.getDatabase();
var providerManager=ScStore.getProviders().manager;


var NOSQLDatabaseProviderList=class NOSQLDatabaseProviderList extends DatabaseProviderList{
	constructor(objType){
		super(objType);
		var self=this;
		self.providerClass="";
	}
	newListObject(tags,jsonObject){
		var self=this;
		var tmpObj=new DatabaseProvider(self.type,tags,jsonObject);
		self.addListObject(tmpObj);
		providerManager.providersList.addListObject(tmpObj);
		//tmpObj.loadFromJson(jsonObject);
		return tmpObj;
	}
}
var nosqldbProvidersManager=new NOSQLDatabaseProviderList();
  
var NOSQLDatabaseProvider=class NOSQLDatabaseProvider extends DatabaseProvider{
	constructor(tags,jsonParams){
		super(tags,jsonParams);
		var self=this;
		self.credentials=jsonParams.connectionInfo;
		self.dbType=jsonParams.dbType;
		self.pool="";
		self.indirect=true;
	}
	async getConnection(){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async callShell(oQuery){
		var self=this;
		var oResult="";
		debugger;
		ScStore.getClosures().newClosure("Executing:"+ oQuery.functionName);
		var self=this;
		var oResult="";
		if (!self.indirect){
			try {
				var dbConnection=await self.getConnection();
				await dbConnection.async_startTransaction();
				var theFnc=dbConnection[oQuery.functionName];
				var funStr = theFnc.toString();
				var arrParams=funStr.slice(funStr.indexOf('(')+1, funStr.indexOf(')')).match(/([^\s,]+)/g);
				var arrValues=[];
				var oParams=oQuery.params;
				for (let paramName of arrParams){
					var vParam=oParams[paramName];
					if ((typeof vParam!=="undefined")&&(vParam!=="")){
						debugger;
						vParam=await ScStore.processExpression(vParam);
					}
					arrValues.push(vParam);
				}
				oResult=await dbConnection[oQuery.functionName](...arrValues);
				//console.log("Result:"+JSON.stringify(oResult);
			} catch(e){
				console.log("Error in Query:"+JSON.stringify(oQuery)+" Exception:" +e.toString());
				throw e;
			} finally {
				if ((typeof dbConnection!=="undefined")&&(dbConnection!="")){
					await dbConnection.async_commit();
					await dbConnection.async_close();
				}
			}
		} else {
			oResult=await self.call(oQuery.functionName,oQuery.params);	
		}
		ScStore.getClosures().popClosure();
		return oResult;
	}	
	async existsStorage(storename){
		var self=this;
		debugger;
		var oResult=await self.callShell({
			functionName:"existsCollection"
			,params:{collectionName:storename}
		});
		return oResult;
	}	
	async removeStorage(storename){
		debugger;
		var self=this;
		var oResult=await self.callShell({
			functionName:"removeCollection"
			,params:{collectionName:storename}
		});
		return oResult;
	}
	async clearStorage(storename){
		debugger;
		var self=this;
		var oResult=await self.callShell({
			functionName:"clearCollection"
			,params:{collectionName:storename}
		});
		return oResult;
	}
	async createStorage(objList,oPersistence,srcObject){
		debugger;
		var self=this;
		var oResult=await self.callShell({
			functionName:"createCollection",
			params:{objList:objList,oPersistence:oPersistence}
		});
		return oResult;
	}
	async plainQuery(sPlainQuery){
		debugger;
		var self=this;
		var oResult=await self.callShell({
			functionName:"plainQuery",
			params:{query:sPlainQuery}
		});
		return oResult;
	}
	
	async executeQuery(queryId,bindParams,options){
		var self=this;
		var oResult="";
		debugger;
		ScStore.getClosures().newClosure(queryId);
		var oParams=await ScStore.processExpression(bindParams);
		if ((typeof oParams!=="undefined")&&
			(typeof oParams!=="string")&&
			(typeof oParams!=="")){
			debugger;
			ScStore.getClosures().extractAttributes(oParams);
		}
		var processedId=await ScStore.processExpression(queryId);
		var oQuery=await ScStore.getQuery().manager.getBestMatch(processedId);
		oResult=await self.internal_executeQuery(oQuery);
		ScStore.getClosures().popClosure();
		return oResult;
	}
	async loadObjectsByQuery(objFactory,queryId,theParams){
		debugger;
		var self=this;
		var oResult="";
		ScStore.getClosures().newClosure(queryId);
		var tags=ScStore.getActualTags();		
		var oParams=await ScStore.processExpression(theParams);
		if ((typeof oParams!=="undefined")&&
			(typeof oParams!=="string")&&
			(typeof oParams!=="")){
			debugger;
			ScStore.getClosures().extractAttributes(oParams);
		}
		var processedId=await ScStore.processExpression(queryId);
		var oQuery=await ScStore.getQuery().manager.getBestMatch(processedId);
		oResult=await self.internal_executeQuery(oQuery);
		var objTags=ScStore.getActualTags();
		for (const oRow of oResult){
			objFactory.addFromJson(objTags,oRow);
		}
		ScStore.getClosures().popClosure();
		return oResult;
	}

	
	async internal_executeQuery(oQuery,bindParams,options){
		var self=this;
		var oResult="";
		var fncName="execute";
		if ((typeof oQuery.internal_functionName!=="undefined")&&(oQuery.internal_functionName!=="")){
			fncName=oQuery.internal_functionName;
		}
		oResult=await self.callShell({
			functionName:fncName,
			params:oQuery
		});
		return oResult;
	}
	
	
}
databaseManager["loadNOSQLDatabaseListManager"]=function(){
	return nosqldbProvidersManager;
};

nosqldbProvidersManager.NOSQLDatabaseProviderClass=NOSQLDatabaseProvider;
nosqldbProvidersManager.NOSQLDatabaseProviderListClass=NOSQLDatabaseProviderList;

module.exports=nosqldbProvidersManager;