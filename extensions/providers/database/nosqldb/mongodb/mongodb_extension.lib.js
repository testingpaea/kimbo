const ScStore = require('../../../../../scenarioStore');
const { ObjectId } = require('mongodb');
ScStore.enableExtension("events");
ScStore.enableExtension("providers/database/nosqldb");
var NOSQLDatabaseProvider=ScStore.getNosqldb().NOSQLDatabaseProviderClass;
var NOSQLDatabaseProviderList=ScStore.getNosqldb().NOSQLDatabaseProviderListClass;

var nosqldbManager=ScStore.getNosqldb();
var providerManager=ScStore.getProviders().manager;
var databaseManager=ScStore.getDatabase();



var MongoClient = require('mongodb').MongoClient;
var fs         = require('fs');

var MongodbProviderList=class MongodbProviderList extends NOSQLDatabaseProviderList{
	constructor(){
		debugger;
		super("mongodb");
		var self=this;
	}
	newListObject(tags,jsonObject){
		debugger;
		var self=this;
		jsonObject.providerType=self.type;
		jsonObject.providerId=jsonObject.id;
		var tmpObj=new MongodbDatabaseProvider(tags,jsonObject);
		//tmpObj.loadFromJson(jsonObject);
		self.addListObject(tmpObj);
		nosqldbManager.addListObject(tmpObj);
		databaseManager.addListObject(tmpObj);

		providerManager.providersList.addListObject(tmpObj);
		return tmpObj;
	}

}
var mongodbProvidersManager=new MongodbProviderList();


  
var MongodbDatabaseProvider=class MongodbDatabaseProvider extends NOSQLDatabaseProvider{
	constructor(tags,jsonObject){ //connectionId,connectionInfo,indirect){
		super(tags,jsonObject);
		var self=this;
		self.indirect=jsonObject.indirect;
		self.withPool=false;
	}
	
	async getConnection(){
		// getting credentials...
		await super.getConnection();
		debugger;
		// getting the connection with the credentials...
		var self=this;
		var connection;
		if ((typeof self.pool!=="undefined" )&&(self.pool!=="")){
			connection=self.pool;
		} else {
			var nLimitPool=1;
			if (typeof self.credentials.connectionLimit!=="undefined"){
				nLimitPool=self.credentials.connectionLimit; 
			}
			var mngUrl=self.credentials.url;
			var mngDBName=self.credentials.dbName;
			//client.db.auth(self.credentials.user,self.credentials.password);
			var fncReplaceAll=function(sSource,sFind,sReplacer){
				const stringToSearch = sFind;  
				const replacer = new RegExp(stringToSearch, 'g');
				return (sSource.replace(replacer, sReplacer)) ;
			}	
			mngUrl=fncReplaceAll(mngUrl,"CRED_USER",self.credentials.user);
			mngUrl=fncReplaceAll(mngUrl,"CRED_PASS",self.credentials.password);
			
			var client;
			if (nLimitPool==1){
				client = await MongoClient.connect(mngUrl);
			} else  {
				client = await MongoClient.connect(mngUrl, { poolSize: nLimitPool });
			}
			connection= client.db(mngDBName);
			self.pool=connection;				
		}
		connection.async_query=async function async_query( sql, args ) {
			return await connection.query( sql, args);
		}
		connection.async_startTransaction=async function async_startTransaction(){
			debugger;
			connection.transSession=await connection.client.startSession();
			await connection.transSession.startTransaction( { readConcern: { level: "snapshot" }, writeConcern: { w: "majority" } } );
		}
		connection.async_commit=async function async_commit() {
			debugger;
			await connection.transSession.commitTransaction();
			await connection.transSession.endSession();
			connection.transSession="";
		}			    	
		connection.async_close=async function async_close() {
			debugger;
			if ((typeof connection.transSession!=="undefined")
					&&(connection.transSession!=="")){
				await connection.async_commit();
			}
//			await connection.client.close();
//			connection="";
//			self.pool="";
		}
		connection.execute=async function execute(collection,action,actionParam1,actionParam2,actionParam3,postAction){
			debugger;
			var oResult=await connection.collection(collection)[action](actionParam1,actionParam2,actionParam3);
			if ((typeof postAction!=="undefined")&&(postAction!=="")){
				oResult=await oResult[postAction]();
			}
			//console.log("operation result:"+JSON.stringify(oResult));
			return oResult;
		}
		connection.executeComplex=async function executeComplex(collection,action,actionParam1,actionParam2,actionParam3,postAction){
			debugger;
			var sFunction=`
					async function executeComplex(connection){				
						var oReturn=await connection.collection("`+collection+`").`+action;
						if ((actionParam1!="")&&(typeof actionParam1!=="undefined")){
							sFunction+="("+actionParam1;
							if ((actionParam2!="")&&(typeof actionParam2!=="undefined")){
								sFunction+=","+actionParam2;
								if ((actionParam3!="")&&(typeof actionParam3!=="undefined")){
									sFunction+=","+actionParam3;
								}
							}
							sFunction+=")";
						}
				sFunction+=`
						return oReturn;
					}`;
			var asyncExecuteComplex = new Function("return " + sFunction.trim())();			
				
			var oResult=await asyncExecuteComplex(connection);
			if ((typeof postAction!=="undefined")&&(postAction!=="")){
				oResult=await oResult[postAction]();
			}
			//console.log("operation result:"+JSON.stringify(oResult));
			return oResult;
		}
		connection.plainQuery=async function plainQuery(query){
			debugger;
			var sFunction=`
					async function executePlain(theConnection){				
						var connection=theConnection;
						var oReturn=await `+query.trim()+`;
						return oReturn;
					}`;
			var asyncPlainQuery = new Function("return " + sFunction.trim())();			
				
			var oResult=await asyncPlainQuery(connection);
			//console.log("operation result:"+JSON.stringify(oResult));
			return oResult;
		}
		connection.existsCollection=async function existsCollection(collectionName){
			debugger;
			var bCollectionExists = await connection.ListCollectionNames().ToList().Contains(collectionName);
			return bCollectionExists ;
		}
		connection.removeCollection=async function removeCollection(collectionName){
			debugger;
			var bCollectionExists = await connection.collection(collectionName).drop();
			return bCollectionExists ;
		}
		connection.clearCollection=async function clearCollection(collectionName){
			debugger;
			var oResult=await connection.collection(collectionName).deleteMany({});
			return oResult;
		}
		connection.createCollection=async function createCollection(collectionName){
			debugger;
			var oResult=await connection.createCollection(collectionName);
			return oResult;
		}
		return connection;
	}
	async insertObject(srcObj,oPersistence){
		debugger;
		var self=this;
		var queryInfo=await self.getQueryElements(srcObj,oPersistence);
		var tmpObj={};
		var fields=queryInfo.columns;
		var values=queryInfo.values;
		for (let i in fields){
			tmpObj[fields[i]]=values[i];
		}		
		var oQuery={};
		if (typeof tmpObj["_id"]!=="undefined"){
			// remove first			
			var antId=new ObjectId(tmpObj["_id"].toString());
			delete tmpObj._id;
	 		oQuery={"collection":oPersistence.storename,
					"action":"replaceOne",
					"postAction":"",
					"actionParam1":{_id: antId},
					"actionParam2":tmpObj,
					"actionParam3":{ upsert: true }
				   }
			await self.internal_executeQuery(oQuery);
			tmpObj["_id"]=antId;
		} else {
			if (typeof oPersistence.attrsUniqueId !=="undefined"){
				// if uniqueId has more than one field.... remove insert is necesary
				var oDeleteParams={};
				if (typeof oPersistence.attrsUniqueId ==="string"){
					var sKey=await ScStore.processExpression(oPersistence.attrsUniqueId);
					var oValue=tmpObj[sKey];
					oDeleteParams[sKey]=oValue;
				} else {
					for (const idx of oPersistence.attrsUniqueId){
						var sKey=await ScStore.processExpression(idx);
						var oValue=tmpObj[sKey];
						oDeleteParams[sKey]=oValue;
					}
				}
		 		oQuery={"collection":oPersistence.storename,
						"action":"deleteMany",
						"postAction":"",
						"actionParam1":oDeleteParams,
						"actionParam2":undefined,
						"actionParam3":undefined
					   }
				await self.internal_executeQuery(oQuery);
			}
	 		oQuery={"collection":oPersistence.storename,
					"action":"insertOne",
					"postAction":"",
					"actionParam1":tmpObj,
					"actionParam2":undefined,
					"actionParam3":undefined
				   }
			await self.internal_executeQuery(oQuery);
		}
	}
	async loadObjects(objFactory,oPersistence){
		var self=this;
		var tags=ScStore.getActualTags();		
		var srcObj=objFactory.sample;
		if ((typeof oPersistence==="undefined")||(oPersistence=="")){
			oPersistence=objFactory.persistence;
		}
		var queryInfo=await self.getQueryElements(srcObj,oPersistence);
	 	var oQuery={"collection":oPersistence.storename,
					"action":"find",
					"postAction":"",
					"actionParam1":{},  // to find all elements
					"actionParam2":undefined,
					"actionParam3":undefined
				   }
		var oResult=await self.internal_executeQuery(oQuery);
		for (const oRow of oResult){
			var oInstance=await objFactory.createNewObject(uuidv4(),false);
			for (const sColumnName of queryInfo.columns) {
				var vValue=oRow[sColumnName];
				var attrName=sColumnName;
				if (typeof vValue!=="undefined"){
					for (const oAlias of arrAlias){
						if (oAlias.alias==sColumnName){
							attrName=oAlias.attrib;
						}
					}
				}
				oInstance[attrName]=vValue;
			}
			objFactory.newListObject(tags,oInstance);
		}
	}

	async getMaxValue(attName,objFactory,oPersistence){
		debugger;
		var self=this; 
		var tags=ScStore.getActualTags();		
		var srcObj=objFactory.sample;
		if ((typeof oPersistence==="undefined")||(oPersistence=="")){
			oPersistence=objFactory.persistence;
		}
		debugger;
		

  
 		var oQuery={
			 	"internal_functionName":"executeComplex",
			 	"collection":oPersistence.storename,
				"action":"aggregate([{'$sort': {'"+attName+"': -1}}, {'$limit': 1}]).toArray()",
				"postAction":"",
				"actionParam1":undefined,  // to find all elements
				"actionParam2":undefined,
				"actionParam3":undefined
			   }			
		var oMaxObject=await self.internal_executeQuery(oQuery);
		var oResult="";
		if (oMaxObject.length>0){
			oMaxObject=oMaxObject[0];
			oResult=oMaxObject[attName];
		}
		return oResult;
	}
	async fullQuery(collectionName,oQuery){
		var self=this;
		var tags=ScStore.getActualTags();		
	}
}
nosqldbManager["loadMongodbListManager"]=function(){
	return mongodbProvidersManager;
};


module.exports=mongodbProvidersManager;