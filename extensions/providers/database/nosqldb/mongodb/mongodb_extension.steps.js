const ScStore = require('../../../../../scenarioStore');
const mongodbManager = require('./mongodb_extension.lib');

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add mongodb database direct provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			mongodbManager.addFromJson(objTags,{id:databaseId,indirect:false});
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add mongodb database indirect provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			mongodbManager.addFromJson(objTags,{id:databaseId,indirect:true});
		}
	}
];

module.exports = stepDefinitions;
