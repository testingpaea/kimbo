const ScStore = require('../../../../scenarioStore');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'do plain search query over provider {string} with',
		stepFunction: async function(databaseId,sPlainQuery){
			debugger;
			var provider=await ScStore.getProviders().manager.getBestMatch("Database",databaseId);
			var result=await provider.plainQuery(sPlainQuery);
			return result;
		}
	}
];

module.exports = stepDefinitions;
