const ScStore = require('../../../../../scenarioStore');
ScStore.enableExtension("events");
ScStore.enableExtension("providers/database");
ScStore.enableExtension("providers/database/sqldb");
ScStore.enableExtension("providers/database/sqldb/sql");
var SQLDatabaseProvider=ScStore.getSqldb().SQLDatabaseProviderClass;
var SQLDatabaseProviderList=ScStore.getSqldb().SQLDatabaseProviderListClass;
var sqldbManager=ScStore.getSqldb();
var databaseManager=ScStore.getDatabase();
var providerManager=ScStore.getProviders().manager;

var mysql      = require('mysql');
var fs         = require('fs');

var MySqlProviderList=class MySqlProviderList extends SQLDatabaseProviderList{
	constructor(){
		debugger;
		super("mysql");
		var self=this;
	}
	newListObject(tags,jsonObject){
		debugger;
		var self=this;
		jsonObject.providerType=self.type;
		jsonObject.providerId=jsonObject.id;
		var tmpObj=new MySqlConnectionPool(tags,jsonObject);
		//tmpObj.loadFromJson(jsonObject);
		self.addListObject(tmpObj);
		sqldbManager.addListObject(tmpObj);
		databaseManager.addListObject(tmpObj);
		providerManager.providersList.addListObject(tmpObj);
		return tmpObj;
	}
}
var mysqlProvidersManager=new MySqlProviderList();


  
var MySqlConnectionPool=class MySqlConnectionPool extends SQLDatabaseProvider{
	constructor(tags,jsonObject){ //connectionId,connectionInfo,indirect){
		super(tags,jsonObject);
		var self=this;
		self.indirect=jsonObject.indirect;
		self.withPool=true;
	}
	getColumnNameDelimiter(){
		return "`";
	}
	
	getExistTableSQL(tableName){
		var sSQL='SELECT COUNT(TABLE_NAME) as `EXISTS` FROM information_schema.TABLES where table_type="BASE TABLE" AND TABLE_NAME="'+tableName+'"';
		return sSQL;
	}

	
	internal_getConnection(){
		var self=this;
        return new Promise( ( resolve, reject ) => {
            self.pool.getConnection( (err, connection) => {
                if ( err ) return reject( err );
                //return connection;
                resolve(connection);
            } )
        } );
	}
	
	async getConnection(){
		// getting credentials...
		await super.getConnection();
		// getting the connection with the credentials...
		var self=this;
		var nConnections=0;
		var connection;
		if (self.withPool) {
			debugger;
			if (self.pool===""){
				self.credentials.connectionLimit=10; 
				console.log("Credentials:"+JSON.stringify(self.credentials));
				self.pool=mysql.createPool(self.credentials);
			}
			connection=await self.internal_getConnection();
		} else {
			if (self.pool===""){
				connection=mysql.createConnection(self.credentials);
				self.pool=connection;				
			} else {
				connection=self.pool;
			} 
			
		}
		connection.async_query=function ( sql, args ) {
		        return new Promise( ( resolve, reject ) => {
		            connection.query( sql, args, ( err, rows ) => {
		                if ( err )
		                    return reject( err );
		                resolve( rows );
		            } );
		        } );
		    }
		connection.async_startTransaction=function(){
		        return new Promise( ( resolve, reject ) => {
		            connection.beginTransaction( err => {
		                if ( err ) return reject( err );
		                resolve();
		            } );
		        } );
		}
		connection.async_commit=function() {
		        return new Promise( ( resolve, reject ) => {
		            connection.commit( err => {
		                if ( err ) return reject( err );
		                resolve();
		            } );
		        } );
		    }			    
		connection.async_close=function() {
		        return new Promise( ( resolve, reject ) => {
					if (self.withPool){
			            connection.release();
			            resolve();
					} else {
			            connection.end( err => {
			                nConnections--;
			                self.pool="";
			                if ( err ) return reject( err );
			                resolve();
			            } );
					}
		        } );
		    }
		connection.execute=async function(sSQL){
			const tblResult = await connection.async_query( sSQL);
			return tblResult
		}
		return connection;
	}
	
}
sqldbManager["loadMySqlListManager"]=function(){
	return mysqlProvidersManager;
};


module.exports=mysqlProvidersManager;