const ScStore = require('../../../../../scenarioStore');
const mysqlManager = require('./mysql_extension.lib');

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add mysql database direct provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			mysqlManager.addFromJson(objTags,{id:databaseId,indirect:false});
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add mysql database indirect provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			mysqlManager.addFromJson(objTags,{id:databaseId,indirect:true});
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in mysql database {string}, the SQL {string} with params',
		stepFunction: async function(variableName,databaseId,sqlId,sParams){
			debugger;
			var provider=mysqlManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId,sParams);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in mysql database {string}, the SQL {string} with',
		stepFunction: async function(databaseId,sqlId,sParams){
			debugger;
			var provider=mysqlManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId,sParams);
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in mysql database {string}, the SQL {string}',
		stepFunction: async function(variableName,databaseId,sqlId){
			debugger;
			var provider=mysqlManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in mysql database {string}, the SQL {string}',
		stepFunction: async function(databaseId,sqlId){
			var provider=mysqlManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId);
		}
	}
];

module.exports = stepDefinitions;
