const ScStore = require('../../../../../scenarioStore');

ScStore.enableExtension("storable");
ScStore.enableExtension("providers/query");
var QueryListClass=ScStore.getQuery().QueryListClass;
var queryManager=ScStore.getQuery().manager;

var SqlList=class SqlList extends QueryListClass {
	constructor(){
		super("sql");
		var self=this;
		self.addIndexField("id");
		self.addIndexField("name");
		ScStore.mapNoRootTags.set("@SQL","SQL");
	}
	getById(userId){
		console.log("getById:"+userId);
		var self=this;
		self.getBestMatch("id",userId);
	}
	getByName(name){
		console.log("getByName:"+name);
		var self=this;
		self.getBestMatch("name",name);
	}
}
var sqlListManager=new SqlList();

queryManager["loadSqlListManager"]=function(){
	return sqlListManager;
};

module.exports={manager:sqlListManager,StorableObjectListClass:SqlList};
