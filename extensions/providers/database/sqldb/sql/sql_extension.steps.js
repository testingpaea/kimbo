const ScStore = require('../../../../../scenarioStore');

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add sql id {string} with',
		stepFunction: async function(sqlId,jsonSQL){
			var objTags=ScStore.getActualTags();
			var oJsonObject=JSON.parse(jsonSQL);
			oJsonObject.id=sqlId;
			var sqlManager=ScStore.getSql().manager;
			sqlManager.addFromJson(objTags,oJsonObject);
		}
	}
];

module.exports = stepDefinitions;
