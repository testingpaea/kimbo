const ScStore = require('../../../../../scenarioStore');
const oracledbManager = require('./oracledb_extension.lib');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add oracle database direct provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			oracledbManager.addFromJson(objTags,{id:databaseId,indirect:false});
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'add oracle database indirect provider with id {string}',
		stepFunction: async function(databaseId){
			var objTags=ScStore.getActualTags();
			oracledbManager.addFromJson(objTags,{id:databaseId,indirect:true});
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in oracle database {string}, the SQL {string} with params',
		stepFunction: async function(variableName,databaseId,sqlId,sParams){
			debugger;
			var provider=oracledbManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId,sParams);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in oracle database {string}, the SQL {string} with',
		stepFunction: async function(databaseId,sqlId,sParams){
			debugger;
			var provider=oracledbManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId,sParams);
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in oracle database {string}, the SQL {string}',
		stepFunction: async function(variableName,databaseId,sqlId){
			debugger;
			var provider=oracledbManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in oracle database {string}, the SQL {string}',
		stepFunction: async function(databaseId,sqlId){
			var provider=oracledbManager.getBestMatch(databaseId);
			var result=await provider.executeSQL(sqlId);
		}
	}
];

module.exports = stepDefinitions;
