const ScStore = require('../../../../../scenarioStore');

ScStore.enableExtension("events");
ScStore.enableExtension("providers/database");
ScStore.enableExtension("providers/database/sqldb");
ScStore.enableExtension("providers/database/sqldb/sql");

var SQLDatabaseProvider=ScStore.getSqldb().SQLDatabaseProviderClass;
var SQLDatabaseProviderList=ScStore.getSqldb().SQLDatabaseProviderListClass;
var sqldbManager=ScStore.getSqldb();
var databaseManager=ScStore.getDatabase();
var providerManager=ScStore.getProviders().manager;


var oracledb = require('oracledb');

var OracleProviderList=class OracleProviderList extends SQLDatabaseProviderList{
	constructor(){
		super("oracledb");
		var self=this;
	}
	newInstance(tags,jsonObject){
		var self=this;
		jsonObject.providerType=self.type;
		jsonObject.providerId=jsonObject.id;
		var tmpObj=new OracleConnectionPool(tags,jsonObject);
		self.addListObject(tmpObj);
		sqldbManager.addListObject(tmpObj);
		databaseManager.addListObject(tmpObj);
		providerManager.providersList.addListObject(tmpObj);

		//tmpObj.loadFromJson(jsonObject);
		return tmpObj;
	}
}
var oracleProvidersManager=new OracleProviderList();
  
var OracleConnectionPool=class OracleConnectionPool extends SQLDatabaseProvider{
	constructor(tags,jsonObject){ //connectionId,connectionInfo,indirect){
		super(tags,jsonObject);
		var self=this;
		self.indirect=jsonObject.indirect;
	}
	getColumnNameDelimiter(){
		return '"';
	}

	async getConnection(){
		// getting credentials...
		await super.getConnection();
		// getting the oracle connection with the credentials...
		var self=this;
		if (self.pool===""){
			console.log("Credentials:"+JSON.stringify(self.credentials));
			var connPool=await oracledb.createPool(self.credentials);
			self.pool=connPool;
		}
		return await self.pool.getConnection();
	}
}
databaseManager["loadOracleListManager"]=function(){
	return oracleProvidersManager;
};


module.exports=oracleProvidersManager;