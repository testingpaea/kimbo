const {v1: uuidv1,v4: uuidv4,} = require('uuid');
const ScStore = require('../../../../scenarioStore');
ScStore.enableExtension("providers");
ScStore.enableExtension("providers/database");
ScStore.enableExtension("providers/database/query");

var DatabaseProvider=ScStore.getDatabase().DatabaseProviderClass;
var DatabaseProviderList=ScStore.getDatabase().DatabaseProviderListClass;
var databaseManager=ScStore.getDatabase();
var providerManager=ScStore.getProviders().manager;

var SQLDatabaseProviderList=class SQLDatabaseProviderList extends DatabaseProviderList{
	constructor(objType){
		super(objType);
		var self=this;
		self.providerClass="";
	}
	newListObject(tags,jsonObject){
		var self=this;
		var tmpObj=new DatabaseProvider(self.type,tags,jsonObject);
		self.addListObject(tmpObj);
		providerManager.providersList.addListObject(tmpObj);
		//tmpObj.loadFromJson(jsonObject);
		return tmpObj;
	}
}
var sqldbProvidersManager=new SQLDatabaseProviderList();
  
var SQLDatabaseProvider=class SQLDatabaseProvider extends DatabaseProvider{
	constructor(tags,jsonParams){
		super(tags,jsonParams);
		var self=this;
		self.credentials=jsonParams.connectionInfo;
		self.dbType=jsonParams.dbType;
		self.pool="";
		self.indirect=true;
	}
	async getConnection(){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async internal_executeSQL(sSQL,bindParams,options){
		var self=this;
		var oResult="";
		if (!self.indirect){
			try {
				var dbConnection=await self.getConnection();
				await dbConnection.async_startTransaction();
				//ScStore.traceClosures();
				console.log("Connection....OK... now execute:"+sSQL);//,bindParams,options
				oResult=await dbConnection.execute(sSQL);
				//console.log("Result:"+JSON.stringify(oResult));
			} catch(e){
				console.log("Error in SQL:"+sSQL+" Exception:" +e.toString());
				throw "Error in SQL:"+sSQL+" Exception:" +e.toString();
			} finally {
				if (dbConnection){
					await dbConnection.async_commit();
					await dbConnection.async_close();
					return oResult;
				}
			}
		} else {
			oResult=await self.call("executeSQL",{sql:sSQL,params:bindParams,options:options});	
		}
		return oResult;
	}
	async executeQuery(sqlId,bindParams,options){
		var self=this;
		return await self.executeSQL(sqlId,bindParams,options);
	}
	
	async executeSQL(sqlId,bindParams,options){
		var self=this;
		var oResult="";
		//debugger;
		ScStore.getClosures().newClosure(sqlId);
		var oParams=await ScStore.processExpression(bindParams);
		if ((typeof oParams!=="undefined")&&
			(typeof oParams!=="string")&&
			(typeof oParams!=="")){
			//debugger;
			ScStore.getClosures().extractAttributes(oParams);
		}
		var processedId=await ScStore.processExpression(sqlId);
		var oSQL=ScStore.getSql().getBestMatch(processedId);
		var sSQL=await ScStore.processExpression(oSQL.sql);
		oResult=await self.internal_executeSQL(sSQL,bindParams,options);
		ScStore.getClosures().popClosure();
		return oResult;
	}
	async insertObject(srcObj,oPersistence){
		var self=this;
		var sqlInfo=await self.getQueryElements(srcObj,oPersistence);
		var sql="INSERT INTO "+oPersistence.storename 
					+ "("+self.getColumnNameDelimiter() 
						+ sqlInfo.columns.join(self.getColumnNameDelimiter()+" \n, "+self.getColumnNameDelimiter()) 
					+ self.getColumnNameDelimiter()
					+")"
					+ " VALUES (";
		var indValue=0;
		for (const vValue of sqlInfo.values){
			sql+="\n";
			if (indValue>0){
				sql+=",";
			};
			sql+= " /* "+ sqlInfo.attTypes[indValue] +" */ ";
			if (sqlInfo.attTypes[indValue]=="longtext"){
				//debugger;
				if (typeof vValue=="object"){
					sql+=" '"+JSON.stringify(vValue)+"'";
				} else {
					sql+=" '"+vValue+"'";
				}
			} else if (sqlInfo.attTypes[indValue]=="bigint(20)"){
				sql+=" " + vValue;
			} else if (sqlInfo.attTypes[indValue]=="decimal(30,10)"){
				sql+=" " + vValue.toFixed(8);
			} else {
				sql+=" '"+vValue+"'";
			}
			indValue++;
		}
		sql+=")";
		await self.internal_executeSQL(sql);
	}
	
	async modifySchema(srcObj,oPersistence){
		var self=this;
		//debugger;
		var sqlInfo=await self.getQueryElements(srcObj,oPersistence);
		var bStorageExists=await self.existsStorage(oPersistence.storename);
		if (!bStorageExists){
			var sObjType=srcObj.type;
			var objList=ScStore.getStorable().manager.getList(sObjType);
			await self.createStorage(objList,oPersistence,srcObj);
		}
		await self.alterStorage(oPersistence,srcObj);	
	}

	async loadObjects(objFactory,oPersistence){
		var self=this;
		var tags=ScStore.getActualTags();		
		var srcObj=objFactory.sample;
		var sqlInfo=await self.getQueryElements(srcObj,oPersistence);
		var sql="select " + self.getColumnNameDelimiter() 
						+ sqlInfo.columns.join(self.getColumnNameDelimiter()+" , "+self.getColumnNameDelimiter()) 
					+ self.getColumnNameDelimiter()
				+" from "+oPersistence.storename; 
		var oResult=await self.internal_executeSQL(sql);
		var arrAlias=oPersistence.attrsAlias;
		for (const oRow of oResult){
			var oInstance=await objFactory.createNewObject(uuidv4(),false);
			for (const sColumnName of sqlInfo.columns) {
				var vValue=oRow[sColumnName];
				var attrName=sColumnName;
				if (typeof vValue!=="undefined"){
					for (const oAlias of arrAlias){
						if (oAlias.alias==sColumnName){
							attrName=oAlias.attrib;
						}
					}
				}
				oInstance[attrName]=vValue;
			}
			objFactory.newListObject(tags,oInstance);
		}
	}
	async existsStorage(storename){
		var self=this;
		var sSql=self.getExistTableSQL(storename);
		//debugger;
		var oResult=await self.internal_executeSQL(sSql);
		return (oResult[0][`EXISTS`]>0);
	}	
	async removeStorage(storename){
		var self=this;
		//debugger;
		var bExistsTable=await self.existsStorage(storename);
		if (bExistsTable){
			var sql="DROP TABLE "+self.getColumnNameDelimiter()+storename+self.getColumnNameDelimiter();
			await self.internal_executeSQL(sql);
		}
	}
	async clearStorage(storename){
		var self=this;
		//debugger;
		var bExistsTable=await self.existsStorage(storename);
		if (bExistsTable){
			var sql="TRUNCATE TABLE "+self.getColumnNameDelimiter()+storename+self.getColumnNameDelimiter();
			await self.internal_executeSQL(sql);
		}
	}
	async createStorage(objList,oPersistence,srcObject){
		var self=this;
		//debugger;
		var sqlInfo;
		var srcObj;
		if (typeof srcObject!=="undefined"){
			srcObj=srcObject;
		} else if (objList.isFactory){
			srcObj=objList.sample;
		} else if (objList.arrObjects.length>0){
			srcObj=objList.arrObjects[0];
		} else {
			throw new Error("The object list " + self.type + " has no elements to use for create storage");
		}
		//debugger;		
		sqlInfo=await self.getQueryElements(srcObj,oPersistence);
		//debugger;
		var sql="CREATE TABLE "+self.getColumnNameDelimiter()+oPersistence.storename+self.getColumnNameDelimiter()  
					+ "(";
		for (var i=0;i<sqlInfo.columns.length;i++){
			sql+=self.getColumnNameDelimiter();
			sql+=sqlInfo.columns[i]+self.getColumnNameDelimiter();
			sql+= " " +sqlInfo.attTypes[i];
			if (i<(sqlInfo.columns.length-1)){
				sql+=",";
			}
		}
		sql+=")";
		await self.internal_executeSQL(sql);
	}
	async alterStorage(oPersistence,oElement){
		var self=this;
		//debugger;
		var sqlInfo;
		var srcObj=oElement;
		//debugger;
		sqlInfo=await self.getQueryElements(srcObj,oPersistence);
		//debugger;
		var sql="";
		var appendSql=function(sLine){
			sql+=sLine+"\n";
		}
		//debugger;
		var delimitedTableName=self.getColumnNameDelimiter()+oPersistence.storename+self.getColumnNameDelimiter();
		var arrActualFields=await self.internal_executeSQL("SHOW FIELDS FROM "+delimitedTableName);
		var fldDefs={};
		if ((typeof arrActualFields!=="string") && (Array.isArray(arrActualFields))){
			for (const fld of arrActualFields){
				fldDefs[fld.Field]=fld.Type;
			}
		}
		//debugger;

		appendSql("ALTER TABLE "+delimitedTableName);  
		var iColAdded=0;
		var arrMigrate=[];
		for (var i=0;i<sqlInfo.columns.length;i++){
			var colName=sqlInfo.columns[i];
			var colType=sqlInfo.attTypes[i];
			var antType=fldDefs[colName];
			if (typeof antType=="undefined"){
				var sqlAux="";
				if (iColAdded>0){
					sqlAux=",";
				}
				iColAdded++;
				sqlAux+="ADD COLUMN IF NOT EXISTS ";
				sqlAux+=self.getColumnNameDelimiter();
				sqlAux+=colName+self.getColumnNameDelimiter();
				//debugger;
				sqlAux+=" " +colType;
				appendSql(sqlAux);
			} else if (antType!==colType){
				if (((colType=="decimal(30,10)")&&(antType=="bigint(20)"))||(colType=="text")){
					arrMigrate.push({colName:colName,antType:antType,newType:colType});
				}
			}
		}
		if (iColAdded>0){
			await self.internal_executeSQL(sql);
		}
		for (const migCol of arrMigrate){
			var delimitedColumnName=self.getColumnNameDelimiter()+migCol.colName+self.getColumnNameDelimiter();
			await self.internal_executeSQL("ALTER TABLE "+delimitedTableName+" MODIFY "+delimitedColumnName+" " + migCol.newType);
		}
	}
}
databaseManager["loadSQLDatabaseListManager"]=function(){
	return sqldbProvidersManager;
};

sqldbProvidersManager.SQLDatabaseProviderClass=SQLDatabaseProvider;
sqldbProvidersManager.SQLDatabaseProviderListClass=SQLDatabaseProviderList;

module.exports=sqldbProvidersManager;