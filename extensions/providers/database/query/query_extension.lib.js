const ScStore = require('../../../../scenarioStore');
ScStore.enableExtension("storable");
var StorableObjectListClass=ScStore.getStorable().StorableObjectListClass;
var storableObjectManager=ScStore.getStorable().manager;

var QueryList=class QueryList extends StorableObjectListClass{
	constructor(){
		super("query");
		var self=this;
		self.addIndexField("id");
		self.addIndexField("name");
		ScStore.mapNoRootTags.set("@QUERY","QUERY");
	}
	getById(userId){
		console.log("getById:"+userId);
		var self=this;
		self.getBestMatch("id",userId);
	}
	getByName(name){
		console.log("getByName:"+name);
		var self=this;
		self.getBestMatch("name",name);
	}
}
var queryListManager=new QueryList();
storableObjectManager["loadQueryListManager"]=function(){
	return queryListManager;
};

queryListManager.QueryListClass=QueryList;

module.exports={manager:queryListManager,QueryListClass:QueryList};
