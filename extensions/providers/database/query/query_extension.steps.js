const ScStore = require('../../../../scenarioStore');

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'add query id {string} with',
		stepFunction: async function(queryId,jsonQuery){
			debugger;
			var objTags=ScStore.getActualTags();
			var sJSONQuery=jsonQuery.replaceAll("\n"," ").replaceAll("\t"," ");
			while (sJSONQuery.indexOf("  ")>=0){
				sJSONQuery=sJSONQuery.replaceAll("  "," ");
			}
			var oJsonObject=JSON.parse(sJSONQuery);
			oJsonObject.id=queryId;
			qManager=ScStore.getQuery().manager;
			qManager.addFromJson(objTags,oJsonObject);
		}
	}
];

module.exports = stepDefinitions;
