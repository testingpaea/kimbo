const ScStore = require('../../../scenarioStore');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in database {string}, the query {string} with params',
		stepFunction: async function(variableName,databaseId,QueryId,sParams){
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			var result=await dbPool.executeQuery(QueryId,sParams);
			ScStore.getClosures().assignOrCreateVar(variableName,result);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in database {string}, the query {string} with',
		stepFunction: async function(databaseId,QueryId,sParams){
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			var result=await dbPool.executeQuery(QueryId,sParams);
		}
	},{   stepMethod: 'Given', stepTimeout: 40000, 
		stepPattern: 'save in variable {string} the result of execute, in database {string}, the query {string}',
		stepFunction: async function(variableName,databaseId,QueryId){
			debugger;
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			var result=await dbPool.executeQuery(QueryId);
			ScStore.getClosures().assignOrCreateVar(variableName,result);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'execute, in database {string}, the query {string}',
		stepFunction: async function(databaseId,QueryId){
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			await dbPool.executeQuery(QueryId);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'store all objects {string} in database {string}',
		stepFunction: async function(databaseId,QueryId){
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			var result=await dbPool.executeQuery(QueryId);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'empty storage {string} of database {string}',
		stepFunction: async function(storename,databaseId){
			var dbPool=ScStore.getDatabase().getBestMatch("id",databaseId);
			var sStorename=await ScStore.processExpression(storename);			
			var result=await dbPool.clearStorage(sStorename);
			//var result=await provider.executeQuery(QueryId);
		}
	},{ stepMethod: 'Given', stepTimeout: 40000,
		stepPattern: 'create storage {string} for objects {string}, in database {string}',
		stepFunction: async function(storename,objName,databaseId){
			var provider=await ScStore.getProvider().getProvider("Database",databaseId);
			var lstObjects=ScStore.getStorable().manager.getList(objName);
			var result=await provider.createStorage(storename,lstObjects.persistence);
			return result;
			//var result=await provider.executeQuery(QueryId);
		}
	}
];

module.exports = stepDefinitions;
