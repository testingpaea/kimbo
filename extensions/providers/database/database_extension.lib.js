const {v1: uuidv1,v4: uuidv4,} = require('uuid');
const ScStore = require('../../../scenarioStore');
ScStore.enableExtension("providers");
ScStore.enableExtension("providers/database/query");
var Provider=ScStore.getProviders().ProviderClass;
var ProviderManagerClass=ScStore.getProviders().ProviderManagerClass;
var ProviderListClass=ScStore.getProviders().ProviderListClass;
var providerManager=ScStore.getProviders().manager;

var DatabaseProviderList=class DatabaseProviderList extends ProviderListClass{
	constructor(objType){
		super(objType);
		var self=this;
		self.providerClass="";
	}
	newListObject(tags,jsonObject){
		var self=this;
		var tmpObj=new DatabaseProvider(self.type,tags,jsonObject);
		self.addListObject(tmpObj);
		providerManager.providersList.addListObject(tmpObj);
		//tmpObj.loadFromJson(jsonObject);
		return tmpObj;
	}
}
var dbProvidersManager=new DatabaseProviderList();
  
var DatabaseProvider=class DatabaseProvider extends Provider{
	constructor(tags,jsonParams){
		super(tags,jsonParams);
		var self=this;
		self.credentials=jsonParams.connectionInfo;
		self.dbType=jsonParams.dbType;
		self.pool="";
		self.indirect=true;
	}
	async getConnection(){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async insertObject(srcObj,oPersistence){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async modifySchema(srcObj,oPersistence){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async loadObjects(objFactory,oPersistence){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async loadObjectsByQuery(objFactory,queryId,oParams){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	
	async getQueryElements(oObject,oPersistence){
		debugger;
		var theObj=oObject;
		var arrAttrs=[];
		var arrColumns=[];
		var arrValues=[];
		var arrTypes=[];
		var arrInternalAttrs=["tags","tagNames","type","_jsonSourceObject"];
		var persistenceParams=oPersistence;
		var autoFields={};
		if (typeof oPersistence.attrsAuto!=="undefined"){
			autoFields=oPersistence.attrsAuto;
		}
		
		var auxArr=Object.keys(theObj);
		for (const autofield in autoFields){
			if (typeof theObj[autofield]==="undefined"){
				auxArr.push(autofield);
			}
		}
		for (var i=0;i<auxArr.length;i++){
			var attribute=auxArr[i];
			var attType="text";
			var isExcluded=false;
			var sAlias=attribute;
			var vValue="";
			for (var j=0;j<arrInternalAttrs.length;j++){
				if (arrInternalAttrs[j]==attribute){
					isExcluded=true;
				}
			}
			for (var j=0;j<persistenceParams.attrsExcluded.length;j++){
				if (persistenceParams.attrsExcluded[j]==attribute){
					isExcluded=true;
				}
			}
			for (var j=0;j<persistenceParams.attrsAlias.length;j++){
				if (persistenceParams.attrsAlias[j][0]==attribute){
					sAlias=persistenceParams.attrsAlias[j][1];
				}
			}
			if (!isExcluded) {
				vValue=theObj[attribute];
				if (typeof vValue!==undefined) {
					if (Array.isArray(vValue)){
						//isExcluded=true;
						vValue=JSON.parse(JSON.stringify(vValue));
						attType="longtext";
					} else if(typeof vValue === 'function') { 
						isExcluded=true; 
					} else if (vValue instanceof Object){
						//isExcluded=true
						vValue=JSON.parse(JSON.stringify(vValue));
						attType="longtext";
					} else if((typeof vValue == 'number') && (!isNaN(vValue))) {
						if ((Number.isSafeInteger(vValue))&& ((""+vValue).indexOf(".")<0)){
							attType="bigint(20)";
						} else {
							attType="decimal(30,10)";
						}
					}
				} else {
					isExcluded=true;
				}
				if (((isExcluded) || (vValue=="") || (typeof vValue==="undefined")) && (typeof autoFields[attribute]!=="undefined")){
					if (autoFields[attribute]=="$RPA_TIMESTAMP"){
						var date=new Date();
						var pad2=function pad2(value){
							var sValue=value+"";
							while (sValue.length<2){
								sValue="0"+sValue;
							}
							return sValue;
						}
						vValue=pad2(date.getFullYear()) 
								+ '/' +  pad2(date.getMonth()+1)
								+ '/' +  pad2(date.getDate())
								+ "  " +  pad2(date.getHours())
								+ ":" + pad2(date.getMinutes())
								+ "." + date.getMilliseconds();
					} else {
						vValue=await ScStore.processExpression(autoFields[attribute]);
					}
					isExcluded=false;
				}
			}
			if (!isExcluded){
				arrTypes.push(attType);
				arrAttrs.push(attribute);
				arrColumns.push(sAlias);
				arrValues.push(vValue);
			}
		};
		return {
			attTypes:arrTypes,
			attrs:arrAttrs,
			columns:arrColumns,
			values:arrValues
		}
	}

	async persist(objList,oPersistence,objIdOrInstance){
		var self=this;
		debugger;
		var srcObj;
		if (objIdOrInstance instanceof Object) {
			srcObj=objIdOrInstance;
		}else {
			srcObj=objList.getById(objIdOrInstance);
		}
		await self.insertObject(srcObj,oPersistence);
	}
	
	async ensureStorage(objList,oPersistence,objIdOrInstance){
		var self=this;
		debugger;
		var srcObj;
		if (objIdOrInstance instanceof Object) {
			srcObj=objIdOrInstance;
		}else {
			srcObj=objList.getById(objIdOrInstance);
		}
		await self.modifySchema(srcObj,oPersistence);
	}


	async persistAll(objList,oPersistence){
		var self=this;
		debugger;
		for (var i=0;i<objList.arrObjects.length;i++){
			var srcObj=objList.arrObjects[i];
			await self.insertObject(srcObj,oPersistence);
		};
	}
	async existsStorage(storename){
		throw new "this method need to be overriden by database expecification";
	}	
	async removeStorage(storename){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async clearStorage(storename){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async createStorage(objList,oPersistence,srcObject){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async executeQuery(objList,oPersistence){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
	async getMaxValue(attName,objFactory,oPersistence){
		await super.getConnection();
		//this method MUST BE EXTENDED IN DIRECT CONNECTIONS...WITH DATABASE SPECIFIC OBJECTS
		return "";
	}
}
providerManager["loadDatabaseListManager"]=function(){
	return dbProvidersManager;
};

dbProvidersManager.DatabaseProviderClass=DatabaseProvider;
dbProvidersManager.DatabaseProviderListClass=DatabaseProviderList;

module.exports=dbProvidersManager;