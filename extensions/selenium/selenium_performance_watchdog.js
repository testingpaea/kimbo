//const slnmDriver = require('./selenium_extension.lib');
const ScStore = require('kimbo/scenarioStore');


async function selenium_performance_watchdog(watchdog){
	var selManager=ScStore.getSelenium();
	var theDriver=await selManager.getDriver();
	var messages=await theDriver.manage().logs().get('performance');
	var nTotal=messages.length;
	messages.forEach(function(msg,index){
		var oMess=JSON.parse(msg.message).message;
		if (oMess.method=="Network.responseReceived"){
			var oResponse=oMess.params.response;
			var theUrl=oResponse.url;
			var theTime=0;
			if (typeof oResponse.timing!=="undefined"){
				theTime=oResponse.timing.sendEnd-oResponse.timing.sendStart;
			}
			var theSize=0;
			if (typeof oResponse.headers!=="undefined"){
				if (typeof oResponse.headers["Content-Length"]!=="undefined"){
					theSize=oResponse.headers["Content-Length"];
				}
			}
			console.log("received "+index+"/"+nTotal+" ["+watchdog.executions+"] ["+theSize+"] ["+ theTime +"s]: "+theUrl);
		}
		
	});
}
module.exports=selenium_performance_watchdog;