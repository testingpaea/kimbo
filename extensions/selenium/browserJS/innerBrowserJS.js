var BrowserJSDriver=class BrowserJSDriver{
	constructor(){
		this.isLoaded=true;
		this.headers=[];
	}
	async setHeaders(arrHeaders){
		this.headers=arrHeaders;
	}
	async waitSecs(nSecs){
		var nWaitMilliSecs=nSecs*1000;
		return new Promise(res => setTimeout(res, nWaitMilliSecs));
	}
	
	async doRequest(sTechnique="fetch",method,sURL,oUrlParams,oHeaderParams,oBody,timeToRetry=45,maxRetries=5){
		//debugger;
		var self=this;
		var sUrl=sURL;
		var sMethod=method;
		var oHeaders={};
		var theBody=oBody;
		var oResult="";
		
		console.log("["+sTechnique+"] ["+sMethod+"] ["+sURL+"] [" +timeToRetry+ "]");
		if ((typeof oUrlParams!=="undefined")&& (oUrlParams!=="")){
			console.log("Url Params:");
			console.log(JSON.stringify(oUrlParams));
			for (const param in oUrlParams) {
				var sParamString=encodeURIComponent(param)+"="+encodeURIComponent(oUrlParams[param]);
				if (sUrl.indexOf("&")>=0){
					sUrl+="&"+sParamString;
				} else if (sUrl.indexOf("?")>=0){
					if (sUrl.indexOf("?")==(sUrl.length-1)) {
						sUrl+=sParamString;
					} else {
						sUrl+="&"+sParamString;
					}
				} else {
					sUrl+="?" + sParamString;
				}
			}			
		}
        self.headers.forEach(function(headerPair){
			for (const attr in headerPair){
				oHeaders[attr]=headerPair[attr];
			}
		});
		if ((typeof oHeaderParams!=="undefined")&& (oHeaderParams!=="")){
			console.log("Request Headers:");
			console.log(JSON.stringify(oHeaderParams));
			for (const theHeader in oHeaderParams) {
				oHeaders[theHeader]=oHeaderParams[theHeader];
			}			
		}
		if ((typeof oBody!=="undefined")&& (oBody!=="")){
			console.log("Request Body:");
			console.log(JSON.stringify(oBody));
			theBody=oBody;
		}
		if (sMethod=="GET"){
			theBody=undefined;
		}
		var objRetryInfo={
			"responded":false,
			"nCalls":0,
			"oResult":"",
			"nRetries":0
		}
		var fncRequest=async function fncRequest(){
			objRetryInfo.nCalls++;
			if (sTechnique==="fetch"){
				oResult=await self.makeRequestFetch(sMethod,sUrl,oHeaders,theBody,objRetryInfo);
			} else if (sTechnique==="xhr"){
				oResult=await self.makeRequestXHR(sMethod,sUrl,oHeaders,theBody,objRetryInfo);
			}
			return oResult;
		}
		if (typeof timeToRetry ==="undefined"){
			return await fncRequest();
		} else {
			//asynchronous call....
			//debugger;
			fncRequest();
			//debugger;
			var nTimeAct=0;
			var nTimeSlice=0.25;
			objRetryInfo.nRetries=0;
			while (!objRetryInfo.responded){
				console.log(objRetryInfo.nCalls + " is Waiting... wait to retry");
				await self.waitSecs(nTimeSlice);
				nTimeAct+=nTimeSlice;
				if ((nTimeAct>=timeToRetry)&&(!objRetryInfo.responded)){
					console.log("Retrying... "+ objRetryInfo.nCalls + "is still Waiting? "+ (!objRetryInfo.responded) +" after " + nTimeAct + " secs");
					console.log("Retry call");
					fncRequest();
					nTimeAct=0;
					objRetryInfo.nRetries++;
					if (objRetryInfo.nRetries>maxRetries){
						objRetryInfo.responded=true;
					}
				}
			}
		}
		//debugger;
		return objRetryInfo.oResult;
	}
	async makeRequestFetch(method,url,oHeaders,oBody,objRetryInfo) {
		//debugger;
		var self=this;
		var actCall=objRetryInfo.nCalls;
		var oResult="";
		console.log("Fetch Call:"+actCall+" ["+method+"] ["+url+"]");
		var sBody=undefined;
		if (typeof oBody!=="undefined"){
			sBody=JSON.stringify(oBody);
		} 
	    let response = await fetch(url, {
		      method: method,
      		  headers: oHeaders,
      		  body: sBody,
    	});
	    //debugger;
    	if (!objRetryInfo.responded){
			oResult = await response.json();
			objRetryInfo.oResult=oResult;
			objRetryInfo.responded=true;
		} else {
			console.log("Call "+actCall+" Late");
		}
		return oResult;
	}
	
	async makeRequestXHR(method,url,oHeaders,oBody,objRetryInfo) {
		var self=this;
		var oResult="";
		var actCall=objRetryInfo.nCalls;
		var sMethod=method;
		var sUrl=url;
		var theHeaders=oHeaders;
		var theBody=oBody;
		var theRetryInfo=objRetryInfo;
		console.log("XHR Call:"+actCall+" ["+method+"] ["+url+"]");
	    return new Promise(
			function (resolve, reject , headers , form) {
		        let xhr = new XMLHttpRequest();
		        xhr.open(sMethod, sUrl);
		        xhr.onload = function () {
		            if (this.status >= 200 && this.status < 300) {
						//debugger;

						if (!theRetryInfo.responded){
							oResult=xhr.response;
							objRetryInfo.oResult=xhr.response;
							objRetryInfo.responded=true;
		                	resolve(xhr.response);
						} else {
							console.log("Call "+actCall+" Late");
		                	resolve(xhr.response);
						}
		            } else {
						console.log("Error in request:"+this.status + " -> "+ xhr.statusText);
						//debugger;
		                /*reject({
		                    status: this.status,
		                    statusText: xhr.statusText
		                });*/
		                resolve("");
		            }
		        };
		        xhr.onerror = function () {
					console.log("Error in request:"+this.status + " -> "+ xhr.statusText);
					//debugger;
	                /*reject({
	                    status: this.status,
	                    statusText: xhr.statusText
	                });*/
	                resolve("");
		            //reject({status: this.status,statusText: xhr.statusText});
		        };
		        if (typeof theHeaders !== "undefined"){
					for (const headerName in theHeaders){
						xhr.setRequestHeader(headerName,theHeaders[headerName]);	
					}
				}
		        if ((sMethod=="POST") && (theBody!==undefined)){
					var sDataToSend=JSON.stringify(theBody);
		        	xhr.send(sDataToSend);
				} else {
		        	xhr.send();
				}
		    }
	    );
	}
}
var browserJSDriver=new BrowserJSDriver();
document.browserJSDriver=browserJSDriver;