const ScStore = require('../../../scenarioStore');
const slnmDriver = require('../selenium_extension.lib');
const { Builder,By,Key,until} = require('selenium-webdriver');
const {Options}= require('selenium-webdriver/chrome');
var fs = require('fs');
const { readFile, realpath } = require('fs/promises')
const { execSync } = require( 'child_process' );
const { Z_VERSION_ERROR } = require('zlib');

var BrowserJSDriver=class BrowserJSDriver{
	constructor(){
		this.slnmDriver=slnmDriver;
	}
	async changeScriptTimeout(nSecs){
		await this.slnmDriver.changeScriptTimeout(nSecs);
		
	}
	async executeJS(jsCode){
		var self=this;
		var result=await self.slnmDriver.executeJS(jsCode);
		return result;
	}
	async executeAsyncJS(jsCode){
		var self=this;
		var result=await self.slnmDriver.executeAsyncJS(jsCode);
		return result;
	}
	async isInnerModuleInjected(){
		var self=this;
		var vResult=await self.executeJS(`
		  			return (function(){ 
		  			var vVal=document.browserJSDriver;
		  			if (typeof vVal==="undefined"){
						  return false;
					} 
					return true;
		  			})();
		  		`);
	    return vResult;		
		
	}
	async injectInnerModule(){
		debugger;
		var self=this;
		var bIsInjected=await self.isInnerModuleInjected();
		//console.log("The module previously loaded:"+bIsInjected);
		if (bIsInjected) return true;
		var srcRealPath=require.resolve("./innerBrowserJS.js");
		//console.log(srcRealPath);
		var srcModule=await readFile(srcRealPath, 'utf8');
		//console.log(srcModule);
		//console.log("injecting");

		await self.executeJS(srcModule);
		//console.log("injected");
		var bIsInjected=await self.isInnerModuleInjected();
		//console.log("The module loaded now:"+bIsInjected);
		return bIsInjected;
	}
	async setHeaders(arrHeaderPairs){
		var self=this;
		debugger;
		if (typeof arrHeaderPairs!=="undefined"){
			await self.injectInnerModule();
			var sHeaders=JSON.stringify(arrHeaderPairs);
			var sJSCode=`
			  			return (function(){ 
			  			var bjsDriver=document.browserJSDriver;
			  			var oHeaders=`+sHeaders+`;
			  			console.log (oHeaders);
			  			console.log (JSON.stringify(oHeaders));
			  			bjsDriver.setHeaders(oHeaders);
						return true;
			  			})();
			  		`;
			debugger;
			var vResult=await self.executeJS(sJSCode);
		    return vResult;		
		}

	}
	async getUrl(url){
		var self=this;
		debugger;
		await self.injectInnerModule();
		var vResult=await self.executeAsyncJS(`
		  			var bjsDriver=document.browserJSDriver;
		  			var oResult =await bjsDriver.makeRequestXHR("get",\``+url+`\`);
					return oResult;
		  			`);
	    return vResult;		
		
	}
	async fetchUrl(method,url){
		var self=this;
		debugger;
		await self.injectInnerModule();
		await self.changeScriptTimeout(3600);
		var vResult=await self.executeAsyncJS(`
					//debugger;
					var callback = arguments[arguments.length - 1];
		  			var bjsDriver=document.browserJSDriver;
		  			var oResult =await bjsDriver.makeFetch("`+method+`",\``+url+`\`,undefined,45);
					callback(oResult);
		  		`);
	    return vResult;		
		
	}
	
	async xhrUrl(method,url){
		var self=this;
		debugger;
		await self.injectInnerModule();
		await self.changeScriptTimeout(3600);
		var vResult=await self.executeAsyncJS(`
					//debugger;
					var callback = arguments[arguments.length - 1];
		  			var bjsDriver=document.browserJSDriver;
		  			var oResult =await bjsDriver.makeRequestXHR("`+method+`",\``+url+`\`,undefined,45);
					callback(oResult);
		  		`);
	    return vResult;
	}
	async doRequest(sTechnique,sMethod,sURL,oUrlParams,oHeaderParams,oBody,timeRetry=45){
		var self=this;
		debugger;
		await self.injectInnerModule();
		await self.changeScriptTimeout(3600);
		var sCadParams=``;
		sCadParams+=`"`+sTechnique+`"`;
		sCadParams+=`,"`+sMethod+`"`;
		sCadParams+=`,\``+sURL+`\``;
		sCadParams+=`,`+JSON.stringify(oUrlParams);
		sCadParams+=`,`+JSON.stringify(oHeaderParams);		
		sCadParams+=`,`+JSON.stringify(oBody);
		sCadParams+=`,`+timeRetry;		
				
		
		var vResult=await self.executeAsyncJS(`
					//debugger;
					var callback = arguments[arguments.length - 1];
		  			var bjsDriver=document.browserJSDriver;
		  			var oResult =await bjsDriver.doRequest(`+sCadParams+`);
					callback(oResult);
		  		`);
	    return vResult;
	}
	
	async step_ApiCall(idCall,idAPI,bPersist=false){
		debugger;
		var self=this;
		var lstApi=ScStore.getStorable().manager.getList(idAPI);
		var sCallId=await ScStore.processExpression(idCall);
		var objApiCall=lstApi.getById(sCallId);
		var sTechnique=await ScStore.processExpression(objApiCall.technique);
		var sMethod=await ScStore.processExpression(objApiCall.fetchAction);
		debugger;
		var sURL=await ScStore.processExpression(objApiCall.url);
		var oUrlParams=await ScStore.processExpression(JSON.stringify(objApiCall.urlParams));
		var oHeaderParams=await ScStore.processExpression(JSON.stringify(objApiCall.headerParams));
		var oBody=await ScStore.processExpression(JSON.stringify(objApiCall.body));
		var vResult=await self.doRequest(sTechnique,sMethod,sURL,oUrlParams,oHeaderParams,oBody);
		var sObjType=objApiCall.objType;
		if ((typeof sObjType!=="undefined")&&(sObjType!=="")){
			await ScStore.getStorable().manager.step_clearList(sObjType);
			if (vResult!==""){ // if result is "" the call not return any data
				await ScStore.getStorable().manager.step_addNewTypedObject(sObjType,vResult);
				if (bPersist){
					await ScStore.getPersist().step_persistAll(sObjType,"");
				}
			}
		}
		return vResult;
	}


}

var browserJSDriver=new BrowserJSDriver();
module.exports = browserJSDriver;
