const {interfaceManager, AppInterface , InterfaceElement} = require('../../interfaces/interfaceManager');
const ScStore = require('../../../scenarioStore');
const bjsDriver = require('./browserJS_extension.lib');


const stepDefinitions = [{ stepMethod: 'Given', stepTimeout: 40000,
	stepPattern: 'alert in browser {string}',
	stepFunction: async (sText)=>{
		var txtAux=await ScStore.processExpression(sText);
 
		if ((typeof sText=="string")&&(sText[0]=="$")){
//			console.log("SHOW it has a $.... "+JSON.stringify(ScStore.getVar(sText)));
			txtAux=self.getVar(sText).value;
//			console.log("SHOW txtValue:"+txtAux);
		}
	  	await bjsDriver.executeJS("alert ('"+txtAux+"');");
		return;
	  }
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'inject browserJS module',
    stepFunction: async function(){
		  debugger;
		  var vResult=await bjsDriver.injectInnerModule();
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'execute javascript in browser',
    stepFunction: async function(){
		  debugger;
		  var vResult=await bjsDriver.executeJS("var script = document.createElement('script');script.src = 'https://code.jquery.com/jquery-3.6.3.min.js'; document.getElementsByTagName('head')[0].appendChild(script);");
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'load {string} from sessionStorage of browser',
    stepFunction: async function(sessionVarName){
		  debugger;
		  var sVarName=await ScStore.processExpression(sessionVarName);
		  var vResult=await bjsDriver.executeJS(`
		  			return (function(){ 
		  			var vVal=sessionStorage.getItem('`+sVarName+`');
		  			console.log("["+vVal+"]");
		  			vVal=vVal.replaceAll('"','');
		  			console.log("["+vVal+"]");
		  			return vVal; 
		  			})();
		  		`);
		  debugger;
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser set headers {string}',
    stepFunction: async function(headers){
		  debugger;
		  var oHeaders=await ScStore.processExpression(headers);
		  var vResult=await bjsDriver.setHeaders(oHeaders);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser get url {string}',
    stepFunction: async function(url){
		  debugger;
		  var sURL=await ScStore.processExpression(url);
		  var vResult=await bjsDriver.getUrl(sURL);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser fetch {string} url {string}',
    stepFunction: async function(method,url){
		  debugger;
		  var sURL=await ScStore.processExpression(url);
		  var sMethod=await ScStore.processExpression(method);

		  var vResult=await bjsDriver.fetchUrl(sMethod,sURL);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser xhr {string} url {string}',
    stepFunction: async function(method,url){
		  debugger;
		  var sURL=await ScStore.processExpression(url);
		  var sMethod=await ScStore.processExpression(method);

		  var vResult=await bjsDriver.xhrUrl(sMethod,sURL);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser {string} request {string} to url {string} with url params {string}, header params {string} and body {string}',
    stepFunction: async function(technique,method,url,urlParams,headerParams,body){
		  debugger;
		  var sTechnique=await ScStore.processExpression(technique);
		  var sMethod=await ScStore.processExpression(method);
		  var sURL=await ScStore.processExpression(url);
		  var oUrlParams=await ScStore.processExpression(urlParams);
		  var oHeaderParams=await ScStore.processExpression(headerParams);
		  var oBody=await ScStore.processExpression(body);
		  var vResult=await bjsDriver.doRequest(sTechnique,sMethod,sURL,oUrlParams,oHeaderParams,oBody);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser API call {string} using {string}',
    stepFunction: async function(callId,apiID){
		  debugger;
		  var sCallId=await ScStore.processExpression(callId);
		  var sAPIid=await ScStore.processExpression(apiID);
		  var vResult=await bjsDriver.step_ApiCall(sCallId,sAPIid);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	},{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'browser API call {string} using {string} and persist',
    stepFunction: async function(callId,apiID){
		  debugger;
		  var sCallId=await ScStore.processExpression(callId);
		  var sAPIid=await ScStore.processExpression(apiID);
		  var vResult=await bjsDriver.step_ApiCall(sCallId,sAPIid,true);
		  await ScStore.setReturnValue(vResult);
		  return vResult;
      }	
	}
];
module.exports = stepDefinitions;
