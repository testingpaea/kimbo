const {interfaceManager, AppInterface , InterfaceElement} = require('../interfaces/interfaceManager');
const ScStore = require('../../scenarioStore');
const slnmDriver = require('./selenium_extension.lib');
//const fncGet=require('./run.steps.js').getExecuteFeatures;
//Defining more steps (has to be in a .stepdef.js file)
const stepDefinitions = [{ stepMethod: 'Given', stepTimeout: 40000,
	stepPattern: 'extensionTestlog {string}',
	stepFunction: async (sText)=>{
		var txtAux=await ScStore.processExpression(sText);
//		console.log("SHOW Try to show:"+sText); 
		if ((typeof sText=="string")&&(sText[0]=="$")){
//			console.log("SHOW it has a $.... "+JSON.stringify(ScStore.getVar(sText)));
			txtAux=self.getVar(sText).value;
//			console.log("SHOW txtValue:"+txtAux);
		}
		console.log("Another Trace '"+txtAux+"'");
		return;
	}
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'close browser',
    stepFunction: async function(){
	  console.log("Closing browser");
	  await slnmDriver.closeBrowser(); 
      return;
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'launch browser to {string} maximized',
    stepFunction: async function(url){
	  console.log("Launching browser to "+url);
	  await slnmDriver.gotoUrl(await ScStore.processExpression(url));   //opens a (pop-up) window for each user
	  await slnmDriver.maximize();
      return;
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'launch browser to {string} maximized with performance watchdog every {string} seconds',
    stepFunction: async function(url,interval){
	  console.log("Launching browser to "+url);
	  var sUrl=await ScStore.processExpression(url);
	  var sInterval=await ScStore.processExpression(interval);	  
	  await slnmDriver.createPerformanceWatchdog("slnm_performance_watchdog",sInterval);
	  slnmDriver.watchdog="slnm_performance_watchdog";
	  await slnmDriver.gotoUrl(sUrl);   //opens a (pop-up) window for each user
	  await slnmDriver.maximize();
      return;
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'launch browser to {string} maximized with watchdog {string}',
    stepFunction: async function(url,watchdog){
	  console.log("Launching browser to "+url);
	  var sUrl=await ScStore.processExpression(url);
	  var sWatchdog=await ScStore.processExpression(watchdog);
	  slnmDriver.watchdog=sWatchdog;
	  await slnmDriver.gotoUrl(sUrl);   //opens a (pop-up) window for each user
	  await slnmDriver.maximize();
      return;
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'add watchdog {string} to browser',
    stepFunction: async function(watchdog){
	  console.log("adding watchdog "+watchdog+ " to browser");
	  var sWatchdog=await ScStore.processExpression(watchdog);
	  slnmDriver.watchdog=sWatchdog;
      return;
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'get url param {string}',
    stepFunction: async function(urlParam){
	  debugger;
	  console.log("get browser url param "+urlParam+" value");
	  var vResult=await slnmDriver.getUrlParam(await ScStore.processExpression(urlParam));   
	  var closures=ScStore.getClosures();
	  return await closures.fncVariableOperation(vResult,closures.setReturnValue);
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'goto url {string}',
    stepFunction: async function(url){
	  debugger;
	  var vUrl=await ScStore.processExpression(url);
	  console.log("goto url "+vUrl+" from "+url);
	  await slnmDriver.gotoUrl(vUrl);   
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'goto url {string} and wait for load of interface {string}',
    stepFunction: async function(url,interfaceName){
	  debugger;
	  var vUrl=await ScStore.processExpression(url);
	  console.log("goto url "+vUrl+" from "+url);
	  await slnmDriver.gotoUrl(vUrl);
	  var itf=await ScStore.getInterfaces().getBestMatch(await ScStore.processExpression(interfaceName));
	  return await itf.waitForLoad();
    }
  },{ stepMethod: 'Given', stepTimeout: 40000,
    stepPattern: 'debug click on {string}',
    stepFunction: async function(elementName){
	  debugger;
	  var elemName=await ScStore.processExpression(elementName);
	  var itf=interfaceManager.getActual();
      await itf.clickOn(elemName);
    }
  }
];

module.exports = stepDefinitions;
