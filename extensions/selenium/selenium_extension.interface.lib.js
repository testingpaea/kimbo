const {interfaceManager, AppInterface , InterfaceElement} = require('../interfaces/interfaceManager');
const SeleniumInterfaceElement = require('./selenium_extension.interface_element.lib');

var SeleniumInterface=class SeleniumInterface extends AppInterface {
	constructor(manager,tags){
		super(manager,tags);
		var self=this;
		self.focusDirect=true;
	}
	newElement(id){
		var self=this;
		return new SeleniumInterfaceElement(id,self);
	}
}

module.exports=SeleniumInterface;