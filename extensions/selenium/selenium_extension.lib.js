const ScStore = require('../../scenarioStore');
const slnmInterface = require('./selenium_extension.interface.lib');
//const slnmInterfaceElement = require('./selenium_extension.interface_element.lib');
const { Builder,By,Key,until} = require('selenium-webdriver');
const {Options}= require('selenium-webdriver/chrome');
const webdriver = require('selenium-webdriver');

var fs = require('fs');
const { execSync } = require( 'child_process' );


var executeOpenWindow = async (windowName,url)=>{  //open new pop-up window
  await slnmDriver.driver.executeAsyncScript((values)=>{
    // http://www.tysoncadenhead.com/blog/executeasyncscript-in-selenium-webdriver-for-node/
    // ------- START: THIS CODE IS EXECUTED IN THE WEBDRIVER WINDOW ----------
    var callback = arguments[arguments.length - 1];  //callback is needed for coming back form the webdriver window
    var windowName = values.windowName;
	var url=values.url;
    window.open(url,windowName
      ,'scrollbars=1,menubar=0,resizable=1,width=850,height=900'); //parameters for new pop-up window  
    callback();
    // ------- END: UP TO HERE IS EXECUTED IN THE WEBDRIVER WINDOW ---------
  },{windowName,url}); //this object is going to be injected as the argument of the function, in this case: "values"
}
var SlnmDriver=class SlnmDriver{
	constructor(){
		this.initialized=false;
		this.binChrome="/usr/bin/chromium-browser";
		if (typeof process.env.chromiumBinary!=="undefined"){
			this.binChrome=process.env.chromiumBinary;
		}
		this.newInterfacesSavePath="/tmp";
		this.watchdog="";
	}
	async initialize(){
		var self=this;
		debugger;
		if (!self.initialized){
			process.argv.forEach(function (val, index, array) {
				console.log("ARGUMENT "+index + ': ' + val);
			});
			console.log("Type of ChromeOptions:"+(typeof Options));
			var chrOptions = new Options();
			chrOptions.addArguments("tls13-variant=disabled");
			chrOptions.addArguments("enforce-tls13-downgrade=disabled");
			chrOptions.addArguments("no-sandbox");
			chrOptions.addArguments("start-maximized");
			chrOptions.addArguments("no-first-run");
			chrOptions.addArguments("disable-features=InfiniteSessionRestore");
			chrOptions.addArguments("disable-session-crashed-bubble");
			chrOptions.addArguments("ignore-certificate-errors");
			
			
			var bInRunningMachine=true;
			if (ScStore.agentProperties.has("seleniumhub")){
				bInRunningMachine=false;
			}
			if (bInRunningMachine) { // run in node machine
				console.log("exist setBinary:"+chrOptions.setBinary);
				if (ScStore.isExtensionLoaded("multiscreen")&&(ScStore.getMultiscreen().enabled)){
					var nScreen=(ScStore.getMultiscreen().actualScreen+2);
					
					chrOptions.addArguments("remote-debugging-port=9233");
					chrOptions.addArguments("remote-debugging-address=0.0.0.0");
					//chrOptions.addArguments("user-data-dir=remote-profile"); 
					 chrOptions.addArguments("user-data-dir=/tmp/chrome_path_byOptions"+nScreen);
					
					console.log("Launch chromium browser script with multiscreen :"+nScreen);
					var binaryPath="/tmp/launchChromium-browser"+nScreen;
					var bExists=false;
					try {
					  if (fs.existsSync(binaryPath)) {
						bExists=true;
					  }
					} catch(err) {
					  console.error(err)
					}
	//				if (!bExists) {
	/*									#!/bin/bash 
							echo launching chrome in display :` + nScreen+ `
							export DISPLAY=:` + nScreen+ `
							export sCommand="/usr/bin/chromium-browser $@"
							echo "Launching chrome binary with parameters: $sCommand" &> /tmp/launchChromium` + nScreen+ `.log
							/usr/bin/chromium-browser "$@" &> /tmp/instanceChromium` + nScreen+ `.log
							expect -c 'spawn ssh `+process.env.ENV_USER+`@localhost "export DISPLAY=:` + nScreen+ `; $sCommand"; expect "assword:"; send "`+process.env.ENV_USER+`\r"; interact'
	*/

							//--user-data-dir=/tmp/chromiumSlave` + nScreen+ `
						fs.writeFileSync(binaryPath,`#!/bin/bash 
							echo launching chrome in display :` + nScreen+ ` &>> /tmp/instanceChromium` + nScreen+ `.log
							export DISPLAY=:` + nScreen+ `
							export sCommand="` + self.binChrome + ` $@ &>> /tmp/instanceChromium` + nScreen+ `.log"
							echo "Launching chrome binary with parameters: $sCommand" &>> /tmp/launchChromium` + nScreen+ `.log
							` + self.binChrome + ` "$@"
							`,'ascii');
								//--no-first-run --disable-features=InfiniteSessionRestore --disable-session-crashed-bubble --no-sandbox --ignore-certificate-errors --user-data-dir=/tmp/chromiumSlave` + nScreen+ `							
						fs.chmodSync(binaryPath, 0o777);
						console.log(execSync("ls -lisa "+binaryPath));
	//				}
					console.log("Executable Path:"+binaryPath);
					console.log(execSync("cat "+binaryPath));
					chrOptions.setChromeBinaryPath(binaryPath);
	//				chrOptions.setChromeBinaryPath(self.binChrome);

				}
				console.log("Chrome Options:"+JSON.stringify(chrOptions));
				debugger;
				if ((typeof self.watchdog!=="undefined")&&(self.watchdog!=="")){
					ScStore.enableExtension("watchdog");
					var logging_prefs = new webdriver.logging.Preferences();
					logging_prefs.setLevel(webdriver.logging.Type.PERFORMANCE, webdriver.logging.Level.ALL);
					chrOptions.setLoggingPrefs(logging_prefs);
				}
				
				var theBuilder=new Builder();
				theBuilder.forBrowser('chrome');
				theBuilder.setChromeOptions(chrOptions);
				theBuilder.withCapabilities({'browserName': 'chrome','name':'Chrome Test','tz':'America/Los_Angeles','build':'Chrome Build','idleTimeout':'60'});
				
				self.driver=await theBuilder.build();
				if ((typeof self.watchdog!=="undefined")&&(self.watchdog!=="")){
					ScStore.getWatchdog().start(self.watchdog);
				}								
				
			} else { // to run in selenium grid it needs nodejs 14 or above!... 
				console.log("In selenium grid");
				console.log("Chrome Options:"+JSON.stringify(chrOptions));
			    const capabilities ={
					"browserName":"chrome"
					/*,
					"goog:chromeOptions":{"args":["--remote-allow-origins=*"]},
					"platformName":"Windows 8.1",
			        name: 'Test 1', // name of the test
			        build: 'NodeJS build' // name of the build
*/			    }

			    
			    // URL: https://{username}:{accessKey}@hub.lambdatest.com/wd/hub
			    var gridUrl = 'http://127.0.0.1:4444/wd/hub';
				var seleniumhubprop=ScStore.agentProperties.get("seleniumhub");
				gridUrl='http://'+seleniumhubprop.value+'/wd/hub';
			 
			    // setup and build selenium driver object
			    this.driver = await new Builder()
			        .usingServer(gridUrl)
			        .withCapabilities(capabilities)
			        .build();
			    console.log("Selenium attached!!!");
			}
			this.initialized=true;
			debugger;
		}
	}
	async closeBrowserWindow(){
		if (this.initialized){
			try {
				var theDriver=await this.getDriver();
				await theDriver.close();
			} catch (theError){
				console.log(theError);
			}
		}
	}
	async closeBrowser(){
		if (this.initialized){
			var theDriver=await this.getDriver();
			try {
				var handles=await theDriver.getAllWindowHandles();
				while (handles.length>0){
					await theDriver.switchTo().window(handles[0]);
					await theDriver.close();
					handles=await theDriver.getAllWindowHandles();
				}
			} catch (theError){
				console.log(theError);
			}
			try {
				await theDriver.quit();
			} catch (theError){
				console.log(theError);
			}

			await ScStore.waitSecs(10);
			this.initialized=false;
		}
	}
	async reloadBrowser(){
		var self=this;
		await self.closeBrowser();
		await self.initialize();
		await ScStore.waitSecs(10);
	}
	async getDriver(){
		await this.initialize();
		return this.driver;
	}
	async openWindow(windowName){    // opens new or closes left over pop-up windows to match the qunatity of users.
	   debugger;
	   await this.initialize();
	   await executeOpenWindow(windowName,"https://paega2.atlassian.net");
	}
	async maximize(){
		console.log("maximize");
		var theDriver=await this.getDriver();
		theDriver.manage().window().maximize();
	}
	async gotoUrl(url){
		console.log("gotoUrl:"+url);
		var driver=await this.getDriver();
		await driver.get(url);
		await this.waitForLoad();
	}
	async getUrlParam(urlParam){
		debugger;
		var driver=await this.getDriver();
		var theUrl=await driver.getCurrentUrl();
		var parts=theUrl.split("?");
		var arrParams=parts[1].split("&");
		for (var i=0;i<arrParams.length;i++){
			var param=arrParams[i];
			var arrDuo=param.split("=");
			if (arrDuo.length==2){
				var varName=arrDuo[0];
				var varValue=arrDuo[1];
				if (varName==urlParam){
					return varValue;
				}
			}
		}
		return "";
	}
	async countWindows(){
		var theDriver=await this.getDriver();
		let windows = await theDriver.getAllWindowHandles();
		return windows.length;
	}
	async isLoaded(elementBy,iFrameArray){
		var self=this;
		var fncWaiter = async function() {
			debugger;
			console.log(" Selenium Check is Loaded fncWaiter");
			await ScStore.waitSecs(1);
			var theDriver=await self.getDriver();
			
			await theDriver.switchTo().defaultContent();
			await ScStore.waitSecs(1);
			if (typeof iFrameArray!=="undefined"){
				for (var i=0;i<iFrameArray.length;i++){
					await theDriver.switchTo().frame(iFrameArray[i]);
					await ScStore.waitSecs(1);
				}
			}
			
			var bResult=false;
			try {
				if (typeof elementBy==="function"){
					console.log(" Element is loaded by a custom function");
					bResult=await elementBy();
					console.log(" Result of custom function:"+bResult);
				} else {
					console.log(" Element By:"+elementBy);
					var el;
					if (typeof elementBy!=="undefined"){
						console.log(JSON.stringify(elementBy));
						el= await theDriver.findElement(elementBy);
					} else {
						el =await theDriver.findElement(By.tagName("html"));
					}
					console.log(" Custom function result found!");
					bResult=await el.isDisplayed();
					console.log(" Element Exists and is visible:"+bResult);
				}
			} catch(error){
				console.log(" error not found:"+error);
				bResult=false;
			}
			console.log(" Selenium check For Load fncWaiter ends:"+bResult);
			return bResult;
		}
		
		// waiting for iFrame Exists.....
		if (typeof iFrameArray!=="undefined") {
			var lstIframes=iFrameArray;
			if (!Array.isArray(iFrameArray)){
				lstIframes=[iFrameArray]
			} 
			console.log("Waiting for iframes..."+lstIframes.length+" " + JSON.stringify(lstIframes));
			for (const iFrame of lstIframes){
				console.log("Iframe:"+iFrame);
				var theDriver=await self.getDriver();
				await theDriver.wait(until.ableToSwitchToFrame(iFrame),100000);
			};
			console.log("End of waiting for iframes");
		}
		console.log("Waiting for element");
		var bLocated=false;

		console.log("Checking if allready exists");
		try{
			bLocated=await fncWaiter();
		} catch(theError){
			console.log("It doesn't exists");
			bLocated=false;
		}
		if (bLocated){
			console.log("It exists...");
		} else {
			console.log("Not Exists...");
		}
		console.log("End checking for element:"+bLocated);
		return bLocated;
	}
	async waitForLoad(elementBy,iFrameArray){
		var self=this;
		var iteration=0;
		var fncWaiter = async function() {
			debugger;
			console.log(iteration+" Selenium Wait For Load fncWaiter");
			var bResult=self.isLoaded(elementBy,iFrameArray);
			console.log(iteration+" Selenium Wait For Load fncWaiter ends:"+bResult);
			iteration++;
			return bResult;
		}
		
		var bLocated=false;

		console.log("Checking if allready exists");
		try{
			bLocated=await fncWaiter();
		} catch(theError){
			console.log("It doesn't exists");
			bLocated=false;
		}
		if (bLocated){
			console.log("It exists...");
		} else {
			console.log("waiting until exists...");
			var theDriver=await self.getDriver();
			bLocated=await theDriver.wait(fncWaiter,100000);
		}
		console.log("End Waiting for element:"+bLocated);
		return bLocated;
	}
	
	
	async switchTo(str_windowId){   //switch to (pop-up) window of user number "windowId"
	  var windowId = parseInt(str_windowId);
	  var theDriver=await this.getDriver();
	  var windows = await theDriver.getAllWindowHandles();
	  //console.log('looking for window: ',windowId);
	  //console.log(windows);
	  var currentWindow = await theDriver.getWindowHandle();
	  if(currentWindow!=windows[windowId-1]) {
		  await theDriver.switchTo().window(windows[windowId-1]);
	  }
	}
	async selectIframeByIndex(ind){
		var driver=await this.getDriver();
		await driver.switchTo().frame(ind);
	}
	newInterface(itf,tags){
		var self=this;
		var baseInterface=new slnmInterface(self,tags);
		return baseInterface;
	}
	
	async changeScriptTimeout(nSecs){
		var self=this;
		var driver=await self.getDriver();
		debugger;
		await driver.manage().setTimeouts( { script: 30000000 } );
	}
		

	async executeJS(jsCode){
		var self=this;
		var driver=await self.getDriver();
		var result=await driver.executeScript(jsCode);
		return result;
	}
	async executeAsyncJS(jsCode){
		var self=this;
		var driver=await self.getDriver();
		var result=await driver.executeAsyncScript(jsCode);
		return result;
	}
	removeSpecialChars(sText){
		var woSpecialChars=sText.replace(/[^a-zA-Z ]/g, "");
		return woSpecialChars;
	}
	
	normalizeText(sText){
		console.log("Text to Normalize:"+sText);
		if ((sText==null)||(typeof sText==="undefined")){
			return "";
		}
		var vAux=sText.replace(/^\s+|\s+$/g, '').trim().substring(0,50);
		var vAux=vAux.replace(/^\s+|\s+$/g, '').trim().substring(0,50);
		return vAux;
	}
	async normalizeCaption(slnmElement){
		var self=this;
		var actText=await slnmElement.getText();
		actText=self.normalizeText(actText);
		if (actText==="") actText=self.normalizeText(await slnmElement.getAttribute("value"));
		if (actText==="") actText=self.normalizeText(await slnmElement.getAttribute("name"));
		if (actText==="") actText=self.normalizeText(await slnmElement.getAttribute("id"));
		if (actText==="") actText=self.normalizeText(await slnmElement.getAttribute("title"));
		if (actText==="") actText=self.normalizeText(await slnmElement.getId());
		return actText;
	}
	async generateXPATH(childElement, inCurrent) {
		var self=this;
		var current=(typeof inCurrent!=="undefined"?inCurrent:"");
		var childId= await childElement.getId();
		var childTag = await childElement.getTagName();
		var childIdAttribute= await childElement.getAttribute("id");
		if (childTag=="body") {
			return "//*/body"+current;
		} else if ((typeof childIdAttribute!=="undefined")&&(childIdAttribute!=null)&&(childIdAttribute!=="")){
			return "//*[@id='"+childIdAttribute+"']"+current;
		}
		var parentElement = await childElement.findElement(By.xpath("..")); 
		var childrenElements = await parentElement.findElements(By.xpath("*"));
		var count = 0;
		for(var i=0;i<childrenElements.length; i++) {
			var childrenElement = childrenElements[i];
			var childrenElementTag = await childrenElement.getTagName();
			var childrenElementId =await childrenElement.getId();
			if (childTag==childrenElementTag) {
				count++;
				if ((childElement==childrenElement)||(childId==childrenElementId)) {
					return self.generateXPATH(parentElement, "/" + childTag + "[" + count + "]"+current);
				}
			}
		}
		return "";
	}
	async listInteractiveElements(fileName){
		var self=this;
		var mapElemIDs=new Map();
		var arrElements=[];
		var driver=await self.getDriver();
		await driver.switchTo().defaultContent();
		var deepestFrameNumber=-1;
		var deepestElement="";
		
		
		var fncProcessBody=async function(elmBody,ifrStack){
			var allChilds=await elmBody.findElements(By.xpath(".//*"));
			var bFocusable=false;
			var innerIFrames=[];
			for (var j=0;(j<allChilds.length);j++){
				var auxChild=allChilds[j];
				var actTagName=await auxChild.getTagName();
				var actTagInnerHtml=await auxChild.getAttribute("innerHTML");
				var actTagFocusable=await auxChild.getAttribute("tabIndex");
				if (actTagName==="iframe"){
					innerIFrames.push(auxChild);
				} else if ((actTagFocusable!==null) && (typeof actTagFocusable!=="undefined")
								&& (actTagFocusable!=="") && (actTagFocusable!="-1")){
					var actXpath=await self.generateXPATH(auxChild);
					var actText=await self.normalizeCaption(auxChild);
					var elemId=actText;
					var sIdCounter="";
					if (mapElemIDs.has(elemId)){
						sIdCounter=mapElemIDs.get(elemId);
						if (sIdCounter===""){
							sIdCounter="2";
						} else {
							var nId=parseInt(sIdCounter);
							nId++;
							sIdCounter=(""+nId);
						}
					}
					mapElemIDs.set(elemId,sIdCounter);
					elemId=(elemId+" " +sIdCounter).trim();

					arrElements.push({
							"id":elemId
							,"selector":{
								"type":"xpath",
								"expression":actXpath,
								"frameNumbers":ifrStack.concat([])
							}
							,"focusable":true
							,"caption":actText
							,"HTMLtag":actTagName
							,"innerHTML":actTagInnerHtml
						});
					if ((deepestFrameNumber==="")||((ifrStack.length-1)>deepestFrameNumber)){
						deepestFrameNumber=ifrStack.length-1;
						deepestElement=actText;
					}
				}
			}
			for (var i=0;i<innerIFrames.length;i++){
				await driver.switchTo().defaultContent();
				for (var j=0;j<ifrStack.length;j++){
					await driver.switchTo().frame(ifrStack[j]);
				}
				await driver.switchTo().frame(innerIFrames[i]); 
				var auxBody=await driver.findElement(By.tagName("body"));
				await fncProcessBody(auxBody,ifrStack.concat([i])); //it is a clon);
				
			}
		}
		
		
		
		var topBody=await driver.findElement(By.tagName("body"));
		var topIFrames=await topBody.findElements(By.tagName("iframe"));
		console.log("Frames:"+topIFrames.length);

		await fncProcessBody(topBody,[]);
		
		var oTextLoaded="";
		if (deepestElement===""){
			arrElements.push({
					"id":"BODY",
					"selector":{
						"type":"xpath",
						"expression":"//*/body",
						"frameNumbers":[]
					},
					"focusable":false,
					"caption":"BODY"
				});
			deepestElement="BODY";
		}
		var oResultInterface={
			"testLoaded":{ 
					 "element":deepestElement,
					 "testWay": "VISIBLE"
				},
			"elements":arrElements,
			"relatedScreens":[],
			"extendsScreen":""
		}
		try {
		  var auxFileName=fileName;
		  if (typeof auxFileName==="undefined"){
				auxFileName="Interface_"+(new Date()).getTime()+'.txt';
		  } else {
			  auxFileName+=".txt";
		  }
		  
		  fs.writeFileSync(self.newInterfacesSavePath+"/"+auxFileName, JSON.stringify(oResultInterface,null,4),{ mode: 0o755 });
		} catch(err) {
		  // An error occurred
		  console.error(err);
		}
	}
	async createPerformanceWatchdog(sWatchdogName,interval){
		ScStore.enableExtension("watchdog");
		debugger;
		var sActLibrary='./selenium_extension.lib';
		var pathToModule = require.resolve(sActLibrary);
		pathToModule = pathToModule.substring(0,pathToModule.length-(sActLibrary.substring(2,sActLibrary.length)+".js").length);
		pathToModule = pathToModule + "selenium_performance_watchdog.js";
		var srcAppPath=require('path').resolve('./')+"/";
		pathToModule =pathToModule.substring(srcAppPath.length,pathToModule.length); 
		debugger;
		await ScStore.getWatchdog().createWatchdog(sWatchdogName,interval,pathToModule);
	}

}

var slnmDriver=new SlnmDriver();
module.exports = slnmDriver;
