const {interfaceManager, AppInterface , InterfaceElement} = require('../interfaces/interfaceManager');
const ScStore = require('../../scenarioStore');
const { Builder,By,Key,until } = require('selenium-webdriver');
const driver = require('selenium-webdriver');



var SeleniumInterfaceElement=class SeleniumInterfaceElement extends InterfaceElement{
	constructor(id,parentInterface){
		super(id,parentInterface);
		var self=this;
		self.caption="";
	}
	loadFromJson(jsonObject){
		super.loadFromJson(jsonObject);
		var self=this;
		self.caption=jsonObject.caption;
		self.HTMLtag=jsonObject.HTMLtag;
		self.inputType=jsonObject.inputType;

	}
	async isVisible(bWithCaption){
		var self=this;
		console.log("Checking visibility for:"+self.id);
		debugger;
		var slnmElement="";
		var arrParams=["EXISTS","VISIBLE"];
		if ((typeof bWithCaption!=="undefined")&&bWithCaption){
			arrParams.push("CAPTION");
		}
		if (self.selector==="") return;
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);

		var bExists=await self.checkElement(elementBy,arrParams);
		return bExists;
	}
	async exists(bWithCaption){
		var self=this;
		console.log("Checking visibility for:"+self.id);
		debugger;
		var slnmElement="";
		var arrParams=["EXISTS"];
		if ((typeof bWithCaption!=="undefined")&&bWithCaption){
			arrParams.push("CAPTION");
		}
		if (self.selector==="") return;
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);

		var bExists=await self.checkElement(elementBy,arrParams);
		return bExists;
	}
	
	async checkElement(elementBy,arrConditions){
		var self=this;
		var bResult=true;
		var selManager=ScStore.getSelenium();
		for (var i=0;(i<arrConditions.length)&&(bResult);i++){
			var cond=arrConditions[i];
			if (cond=="EXISTS"){
				//console.log(elementBy);
				try {
					var theDriver=await selManager.getDriver();
					var el= await theDriver.findElement(elementBy);
					//console.log("Exists!!!");
					self.seleniumElement=el;
					bResult=true;
				} catch(error){
					//console.log("Exists....Error:"+error);
					bResult=false;
				}
			} else if (cond=="VISIBLE"){
				//console.log(elementBy);
				try {
					var theDriver=await selManager.getDriver();
					var el= await theDriver.findElement(elementBy);
					//console.log("Visible!!!");
					self.seleniumElement=el;
					bResult=await el.isDisplayed();
				} catch(error){
					//console.log("Visible....Error:"+error);
					bResult=false;
				}
			} else if (cond=="CAPTION"){
				//console.log(elementBy);
				try {
					var theDriver=await selManager.getDriver();
					var el= await theDriver.findElement(elementBy);
					var elText=await selManager.normalizeCaption(el);
					elText=selManager.removeSpecialChars(elText);
					var itfTextNormalized=selManager.normalizeText(self.caption.trim());
					itfTextNormalized=selManager.removeSpecialChars(itfTextNormalized);
					console.log("elText #"+elText+"# vs #"+itfTextNormalized+"#");
					if (elText==itfTextNormalized){
						//console.log("Same Caption!!!");
					} else {
						bResult=false;
					}
				} catch(error){
					//console.log("Same Caption....Error:"+error);
					bResult=false;
				}
			}
		}
		return bResult;
	}
	async isLoaded(arrConditions){
		var self=this;
		console.log("check for:"+self.id+" conditions:"+JSON.stringify(arrConditions));
		if (self.selector==="") return;
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);
		var fncWaitForElement=async function(){
			console.log("Wait for element "+self.id+":"+arrConditions.length+" "+arrConditions[0]);
			var bResult=await self.checkElement(elementBy,arrConditions);
			return bResult;
		};
		var bResult=await selManager.isLoaded(fncWaitForElement,self.selector.frameNumbers);
		return bResult;
	}
	async waitFor(arrConditions){
		var self=this;
		console.log("waiting for:"+self.id+" conditions:"+JSON.stringify(arrConditions));
		if (self.selector==="") return;
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);
		var fncWaitForElement=async function(){
			console.log("Wait for element "+self.id+":"+arrConditions.length+" "+arrConditions[0]);
			var bResult=await self.checkElement(elementBy,arrConditions);
			return bResult;
		};
		var bResult=await selManager.waitForLoad(fncWaitForElement,self.selector.frameNumbers);
		return bResult;
	}
	async focus(bWithCaption){
		var self=this;
		var arrOptions=["VISIBLE"];
		if (self.caption!==""){
			if ((typeof bWithCaption==="undefined")||bWithCaption){
				arrOptions.push("CAPTION");
			} 
		}
		var vResult=await this.waitFor(arrOptions);
		//vResult.focused=true;
		return vResult;
	}
	async scrollWindowTo(selElement){
		if (typeof selElement==="undefined") return;
		var self=this;
		try {
			var theDriver=await ScStore.getSelenium().getDriver();
			await theDriver.executeScript("arguments[0].scrollIntoView(true);", selElement);
		} catch (err){
			console.log("Can´t scroll to element:"+self.id);
		}
		
	}
	async getElements(){
		var self=this;
		if (self.selector==="") return "";
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);
		var theDriver=await selManager.getDriver();
		var el= await theDriver.findElements(elementBy);
		return el;
	}
	async getElement(){
		var self=this;
		if (self.selector==="") return "";
		var selManager=ScStore.getSelenium();
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var elementBy=new By(self.selector.type,sExpression);
		var theDriver=await selManager.getDriver();
		var el= await theDriver.findElement(elementBy);
		return el;
	}
	async gotoElement(){
		var self=this;
		if (self.selector==="") return "";
		var el=await self.getElement();
		var oneElement=el;
		if (Array.isArray(el)){
			if (el.length>0){
				oneElement=el[0];
			} else {
				oneElement=undefined;
			}
		}
		await self.scrollWindowTo(oneElement);
		console.log("Located:"+el+".... now do the action");
		return el;
	}
	async getTableRows(){
		var self=this;
		var theParent=await self.gotoElement();
		if (theParent==="") return "";
		var sExpression=await ScStore.processExpression(self.selector.expression);
		var sNewExpression=sExpression+"/*/tr";

		var elementBy=new By("xpath",sNewExpression);
		var selManager=ScStore.getSelenium();
			
		var tblRows= "";
		var theDriver=await selManager.getDriver();
		tblRows= await theDriver.findElements(elementBy);
		
		var tblResult={headers:[],rows:[]};
		for (var i=0;i<tblRows.length;i++){
			var isHeader=false;
			var rowVals=[];
			sNewExpression="td";
			elementBy=new By("xpath",sNewExpression);
			var rowCells=await tblRows[i].findElements(elementBy);
			if (rowCells.length==0) {
				sNewExpression="th";
				elementBy=new By("xpath",sNewExpression);
				rowCells=await tblRows[i].findElements(elementBy);
				isHeader=true;
			} 
			for (var j=0;j<rowCells.length;j++){
				rowVals.push(await rowCells[j].getText());
				console.log(rowVals[j]);
			}
			if (isHeader){
				tblResult.headers.push(rowVals);
			} else {
				tblResult.rows.push(rowVals);
			}
			
		}
		console.log("Loaded Table: "+tblResult.rows.length+" rows");
		debugger;
		return tblResult;
	}
	async getValue(){
		var self=this;
		debugger;
		var slnmElement="";
		var bExists=await self.waitFor(["EXISTS"]);
		if (bExists) {
			slnmElement=await self.gotoElement(); 
		}
		if (slnmElement==="") return;
		
		var actText=await slnmElement.getText();
		if (actText==="") actText=await slnmElement.getAttribute("value");
		if (actText==="") actText=await slnmElement.getAttribute("name");
		if (actText==="") actText=await slnmElement.getAttribute("id");
		if (actText==="") actText=await slnmElement.getAttribute("title");
		if (actText==="") actText=await slnmElement.getId();
		return actText;
	}
	async checkClass(className){
		var self=this;
		debugger;
		var slnmElement="";
		var bExists=await self.waitFor(["EXISTS"]);
		if (bExists) {
			slnmElement=await self.gotoElement(); 
		}
		if (slnmElement==="") return;
		var fullClass=await slnmElement.getAttribute("class");
		debugger;

		if (fullClass.indexOf(className)>=0){
			return true;
		} else {
			return false;
		}
	}
	async isEnabled(){
		var self=this;
		debugger;
		var slnmElement="";
		var bExists=await self.waitFor(["EXISTS"]);
		if (bExists) {
			slnmElement=await self.gotoElement(); 
		}
		if (slnmElement==="") return;
		var bIsEnabled=await slnmElement.isEnabled();
		return bIsEnabled;
	}	
	
	async writeText(sText){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		await el.sendKeys(sText);		
	}
	async clear(){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		await el.clear();		
	}
	async pressArrow(idArrow,nInputTimes){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var selManager=ScStore.getSelenium();
		var selDriver=await selManager.getDriver();
		var sKey="";
		if (idArrow.toUpperCase()=="UP"){
			sKey=driver.Key.UP;
		} else if (idArrow.toUpperCase()=="DOWN"){
			sKey=driver.Key.DOWN;
		} else if (idArrow.toUpperCase()=="LEFT"){
			sKey=driver.Key.LEFT;
		} else if (idArrow.toUpperCase()=="RIGHT"){
			sKey=driver.Key.RIGHT;
		}
		var nTimes=1;
		if ((typeof nInputTimes==="undefined")||(nInputTimes==="")){
			nTimes=1;
		} else {
			try {
				nTimes=parseInt(nInputTimes);
			} catch (theError){
				nTimes=1;
			}
		}
		if (nTimes>0) {
			await el.sendKeys(sKey,nTimes);		
		}
	}
	async pressEnter(nTimes){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var selManager=ScStore.getSelenium();
		var selDriver=await selManager.getDriver();
		var sKey=driver.Key.ENTER;
		var nTimes=1;
		if ((typeof nInputTimes==="undefined")||(nInputTimes==="")){
			nTimes=1;
		} else {
			try {
				nTimes=parseInt(nInputTimes);
			} catch (theError){
				nTimes=1;
			}
		}
		if (nTimes>0) {
			await el.sendKeys(sKey,nTimes);	
		}
	}
	async pressSpace(){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var selManager=ScStore.getSelenium();
		var selDriver=await selManager.getDriver();
		var sKey=driver.Key.SPACE;
		await el.sendKeys(sKey);	
	}
	async clickOnItemNumber(nItem){
		debugger;
		var self=this;
		var arrElems=await self.getElements();
		var el=arrElems[nItem];
		try {
			await self.scrollWindowTo(el);
		} catch (err1){
			debugger;
			console.log("Error scrolling to window... maybe to fast to the scrolling.... try again in 5 secs");
			await ScStore.waitSecs(5);
			await self.scrollWindowTo(el);
			console.log("This Time OK in scrollwindowto");
		}
		
		try {
			await el.click();
		} catch (err2){
			debugger;
			console.log("Error clicking in element... maybe to fast to the scrolling.... try again in 5 secs");
			await ScStore.waitSecs(5);
			await el.click();
			console.log("This Time OK in click");
		}
	}
	async click(){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		//console.log("seleniumElement click");
		debugger;
		try {
			await el.click();
		} catch (err1){
			debugger;
			console.log("Error clicking in element... maybe to fast to the scrolling.... try again in 5 secs");
			await ScStore.waitSecs(5);
			await el.click();
			console.log("This Time OK in click");
		}
		/*var seleniumElement="";
		await self.waitFor(["EXISTS"]);
		console.log("Element exists");
		await self.seleniumElement.click();*/
		console.log("Clicked");
	}
	async selectOptionNumber(optionNumber){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var elOptions=await el.findElements(By.tagName('option'));
		for (var i=0;i<elOptions.length;i++){
			var theOption=elOptions[i];
			if (i==optionNumber){
				await theOption.click();
			}
		}
	}
	async selectOptionValue(optionValue){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var elOptions=await el.findElements(By.tagName('option'));
		for (var i=0;i<elOptions.length;i++){
			var theOption=elOptions[i];
			var theValue=await theOption.getAttribute('value');
			if (theValue==optionValue){
				await theOption.click();
			}
		}
	}
	async selectOptionText(optionText){
		var self=this;
		var el=await self.gotoElement(); if (el==="") return;
		var elOptions=await el.findElements(By.tagName('option'));
		for (var i=0;i<elOptions.length;i++){
			var theOption=elOptions[i];
			var theText=await theOption.getText();
			if (theText==optionText){
				return await theOption.click();
			}
		}
	}
}

module.exports=SeleniumInterfaceElement;
