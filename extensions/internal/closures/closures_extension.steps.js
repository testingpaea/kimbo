const ScStore = require('../../../scenarioStore');
const closures = require('./closures_extension.lib');
const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{ stepMethod: 'Given', stepPattern: 'assign returned value to {string}', stepTimeout: maxScenarioTimeout,attrPrueba:33,
		stepFunction: async function(tgtVar){
			debugger;
			return await closures.fncVariableOperation(tgtVar,closures.assignReturnedToVariable);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign {string} to variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(srcValue,tgtVar){
			debugger;
			return await closures.fncVariableOperation(tgtVar,srcValue,closures.assignOrCreateVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign timestamp to variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVar){
			debugger;
			var sTimestamp=Date.now();
			return await closures.fncVariableOperation(tgtVar,sTimestamp,closures.assignOrCreateVar);
		}
	},{ stepMethod: 'Then', stepPattern: 'return value {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(rtnVar){
			return await closures.fncVariableOperation(rtnVar,closures.setReturnValue);
		}
	},{ stepMethod: 'Given', stepPattern: 'exists variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(varName){
			return await closures.fncVariableOperation(varName,closures.existsVariable);
		}
	},{ stepMethod: 'Given', stepPattern: 'scenario default variables', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(varValues){
			return await closures.fncVariableOperation(varValues,closures.setDefaultVariables);
		}
	},{ stepMethod: 'Given', stepPattern: 'scenario default variable {string}. If not exits set the value {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(varName,varValue){
			return await closures.fncVariableOperation(varName,varValue,closures.setDefaultVariable);
		}
	},{ stepMethod: 'Given', stepPattern: 'scenario default variables {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(varValues){
			return await closures.fncVariableOperation(varValues,closures.setDefaultVariables);
		}
	},{ stepMethod: 'Given', stepPattern: 'set {string} to new variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValue,tgtVariableName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputValue,closures.addVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'set {string} to new global variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValue,tgtVariableName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputValue,0,closures.addVarAtLevel);
		}
	},{ stepMethod: 'Given', stepPattern: 'set {string} to new variable {string}, {int} levels up', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValue,tgtVariableName,iLevelsUp){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputValue,(-1)*iLevelsUp,closures.addVarAtLevel);
		}
	},{ stepMethod: 'Given', stepPattern: 'set {string} to variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValue,tgtVariableName){
			return await closures.fncVariableOperation(tgtVariableName,inputValue,closures.setVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'set to new variable {string} multiline text', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVariableName,inputValue){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputValue,closures.addVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'set to variable {string} multiline text', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVariableName,inputValue){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputValue,closures.setVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'set values {string} to list of variables {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValues,tgtVariableNames){
			var srcValues=await ScStore.processExpression(inputValues);
			var names=await ScStore.processExpression(tgtVariableNames);
			for (var i=0;i<srcValues.length;i++){
				return await closures.fncVariableOperation(names[i],srcValues[i],closures.setVar);
			}
		}
	},{ stepMethod: 'Given', stepPattern: 'set values {string} to list {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(inputValues,tgtVariableName){
			debugger;
			var srcValues=await ScStore.processExpression(inputValues);
			var arrValues=srcValues.split(",");
			var theList=await ScStore.processExpression(tgtVariableName);
			return await closures.fncVariableOperation(theList,arrValues,closures.assignOrCreateVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'set list {string} with the values', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVariableName,inputValues){
			debugger;
			var srcValues=await ScStore.processExpression(inputValues);
			var arrValues=srcValues.split(",");
			arrValues.forEach(function doTrim(value,index){
				if (typeof value==="string"){
					var auxVal=value.trim();
					arrValues[index]=auxVal;
				}
			});
			var theList=await ScStore.processExpression(tgtVariableName);
			return await closures.fncVariableOperation(theList,arrValues,closures.assignOrCreateVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'extract attributes of {string}', 
		stepFunction: async function(varName){
			debugger;
			var srcObject=await ScStore.processExpression(varName);
			return await closures.fncVariableOperation(srcObject,closures.extractAttributes);
		}
	},{ stepMethod: 'Given', stepPattern: 'create a new empty object global variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVariableName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,{},0,closures.addVarAtLevel);
		}
	},{ stepMethod: 'Given', stepPattern: 'create a new empty object variable {string}', 
		stepFunction: async function(tgtVariableName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,{},closures.addVar);
		}                                  
	},{ stepMethod: 'Given', stepPattern: 'create a new object variable {string} with json', 
		stepFunction: async function(tgtVariableName,sJsonText){
			debugger;
			var oObject=await ScStore.processExpression(sJsonText);
			return await closures.fncVariableOperation(tgtVariableName,oObject,closures.addVar);
		}
		
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the value of object attribute {string} of var {string}', 
		stepFunction: async function(tgtVariableName,attName,varName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,attName,varName,closures.attributeToVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the number of elements in array {string}', 
		stepFunction: async function(tgtVariableName,arrValues){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,arrValues,closures.countToVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the element {string} of list {string}', 
		stepFunction: async function(tgtVariableName,arrIndex, arrValues){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,arrIndex,arrValues,closures.arrElementToVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the value of object function {string} of var {string}', 
		stepFunction: async function(tgtVariableName,fncName,varName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,fncName,varName,closures.functionToVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'assign to variable {string} the clone of variable {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(tgtVariableName,inputVariableName){
			debugger;
			return await closures.fncVariableOperation(tgtVariableName,inputVariableName,closures.cloneVar);
		}	
	},{ stepMethod: 'Given', stepPattern: 'assign {string} to the object attribute {string} of var {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(vValue,attName,varName){
			debugger;
			return await closures.fncVariableOperation(varName,attName,vValue,closures.setAttribute);
		}	
	},{ stepMethod: 'Given', stepPattern: 'push {string} to the object array attribute {string} of var {string}', stepTimeout: maxScenarioTimeout,
		stepFunction: async function(vValue,attName,varName){
			debugger;
			return await closures.fncVariableOperation(varName,attName,vValue,closures.pushAttribute);
		}	
		
		
	}
];

module.exports = stepDefinitions;
