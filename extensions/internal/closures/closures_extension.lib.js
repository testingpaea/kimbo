const ScStore = require('../../../scenarioStore');
const lodash = require('lodash');

var ScenarioVariable=class ScenarioVariable{
	constructor(name,value){
		var self=this;
		self.name=name;
		self.value=value;
		self.stackValues=[];
	}
	isUndefined(){
		var self=this;
		if (typeof self.value==="undefined"){
			return true;
		}
		return false;
	}
	set(value){
		var self=this;
		self.value=value;
	}
	pop(){
		var self=this;
		var vReturn=self.value;
		self.value=self.stackValues.pop();
		return vReturn;
	}
	push(value){
		var self=this;
		self.stackValues.push(self.value);
		self.value=value;
	}
	clone(){
	}
	
}
var ScenarioClosure=class ScenarioClosure{
	constructor(name){
		var self=this;
		self.name=name;
		self.variables=new Map();
	}
	getVar(varName,bEnsureExists){
		var self=this;
		if (self.existsVar(varName)) {
			return self.variables.get(varName);
		}
		if ((typeof bEnsureExists!=="undefined")&&(bEnsureExists)){
			console.log("ERROR!! Variable "+varName+" is not defined in scenario closure");
			throw "Variable "+varName+" is not defined in scenario closure";
		}
		return undefined;
	}
	addVar(varName,varValue,isStack){
		var self=this;
		//console.log("Adding Variable");
		if (self.existsVar(varName)){
			console.log("ERROR!! Variable "+varName+" is yet defined in scenario closure");
			throw "Variable "+varName+" is yet defined in scenario closure";
		}
		var newVar=new ScenarioVariable(varName,varValue);
		self.variables.set(varName,newVar);
	}
	setVar(varName,varValue){
		var self=this;
		var auxVar=self.getVar(varName,true);
		auxVar.set(varValue);
	}
	pushVar(varName,varValue){
		var self=this;
		var auxVar=self.getVar(varName,true);
		auxVar.push(varValue);
	}
	popVar(varName){
		var self=this;
		var auxVar=self.getVar(varName,true);
		return auxVar.pop();
	}
	
	existsVar(varName){
		//console.log("Exists " + varName + ":"+this.variables.has(varName));
		return this.variables.has(varName);
	}
	trace(iDeepLevel){
		var self=this;
		var iDeep=0;
		if (typeof iDeepLevel !=="undefined"){
			iDeep=iDeepLevel;
		}
		var prefix="";
		for (var i=0;i<iDeep;i++){
			prefix+="   ";
		}
		console.log(prefix+self.name);
		console.log("-------------------");
		for (const theVar of self.variables){
			console.log(prefix+theVar.name+":"+JSON.stringify(theVar.value));
		};
	}
}
/*const defineVariable = function (varName, varValue){
  OtherWorld.varStore[varName] = JSON.parse(varValue);
  if (OtherWorld.showStepdefFiles) { console.log(`Kimbo says: ${varName} = ${varValue}`) }
  return;
}
*/
var ScenarioClosureManager=class ScenarioClosureManager{
	constructor(){
		var self=this;
		self.closures=[];
		self.runtimeClosures=[];
		self.newClosure("Root Closure");
	}
	
	getClosuresDeep(){
		var self=this;
		return self.closures.length;
	}

	pushClosure(closure,internal=false){
		var self=this;
		if (!internal) self.closures.push(closure);
		else self.runtimeClosures.push(closure);
		return closure;
	}
	pushClosures(closures){
		var self=this;
		self.closures.push(closures.closure);
		self.runtimeClosures.push(closures.internal);
	}
	newClosure(name,bWithNewExecutionNode){
		var self=this;
		//console.log("New Closure("+self.closures.length+"):"+ name);
		var closure=new ScenarioClosure(name);
		var rtmClosure=new ScenarioClosure(name);
		if ((typeof bWithNewExecutionNode!=="undefined")&&bWithNewExecutionNode){
			ScStore.newExecutionNode(name);
		}
		self.pushClosure(closure);
		self.pushClosure(rtmClosure,true);
		//console.log("Closure Created:"+self.closures.length); 
		//self.traceClosures();
		return closure;
	}
	popClosure(bWithDoneExecutionNode){
		var self=this;
		debugger;
		var internalClosure=self.runtimeClosures.pop();
		var closure=self.closures.pop();
		//console.log("Closure Destroyed:"+self.closures.length); 
		//self.traceClosures();
		if ((typeof bWithDoneExecutionNode!=="undefined")&&bWithDoneExecutionNode) {
			//console.log("Pop Closure With Execution Node("+self.closures.length+")");
			ScStore.doneExecutionNode();
		} else {
			//console.log("Pop Closure ("+self.closures.length+")");
		}
		return {closure:closure,internal:internalClosure};
	}
	clear(bResetExecutionNodes){
		var self=this;
		debugger;
		self.closures=[];
		self.runtimeClosures=[];
		if ((typeof bResetExecutionNodes!=="undefined")&&bResetExecutionNodes){
			ScStore.clearExecutionTree();
		}
	}
	getClosure(internal=false,fromBottomIndex=0){
		var self=this;
//		console.log("Closures status:"+self.closures.length+"/"+self.runtimeClosures.length);
		if (!internal) return self.closures[self.closures.length-(1+fromBottomIndex)];
		return self.runtimeClosures[self.runtimeClosures.length-(1+fromBottomIndex)];
	}
	getUpperClosure(internal=false){
		var self=this;
		return self.getClosure(internal,1);
	}
	getRootClosure(internal=false){
		var self=this;
		if (!internal) return self.closures[0];
		return self.runtimeClosures[0];
	}
	existsLocalVar(varName,internal=false){
		var self=this;
		var closure=self.getClosure(internal);
		return closure.existsVar(varName);
	}
	getLocalVar(varName,internal=false){
		var self=this;
		var closure=self.getClosure(internal);
		return closure.getVar(varName);
	}
	getVar(varName,internal=false){
		var self=this;
		var sVarName=varName;
		if (varName.charAt(0)!="$" ){
			sVarName="$"+varName;
		}
		for (var i=0;i<self.closures.length;i++){
			var closure=self.getClosure(internal,i);
			//console.log("closure:"+closure.name +"  --- finding:"+varName);
			//closure.trace(i);
			if (closure.existsVar(sVarName)){
				//console.log("Variable:" + varName+" exists in closure:"+ closure.name);
				return closure.getVar(sVarName);
			}
		}
		console.log("ERROR!!! Variable "+varName+"/"+sVarName+" is not defined anywhere");
		throw "Variable "+varName+"/"+sVarName+" is not defined anywhere";
	}
	setVar(varName,varValue,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		var auxVar=self.getVar(sVarName,internal);
		auxVar.set(varValue);
	}
	pushVar(varName,varValue,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		var auxVar=self.getVar(sVarName,internal);
		auxVar.push(varValue);
	}
	popVar(varName,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		var auxVar=self.getVar(sVarName,internal);
		return auxVar.pop();
	}
	cloneObject(tgtVarName,objValue){
		var self=this;
		debugger;
		var theClon=lodash.cloneDeep(objValue);
		self.assignOrCreateVar(tgtVarName,theClon);
		return theClon;
	}
	cloneVar(tgtVarName,varName){
		var self=this;
		debugger;
		var theVar=self.getVar(varName);
		return self.cloneObject(tgtVarName,theVar.value);
	}
	
	attributeToVar(tgtVarName,attName,srcVarName){
		var self=this;
		debugger;
		var theVar=self.getVar(srcVarName);
		var auxValue="";
		if (typeof theVar.value[attName]!=="undefined") {
			auxValue=theVar.value[attName];
		}
		return self.assignOrCreateVar(tgtVarName,auxValue);
	}

	functionToVar(tgtVarName,fncName,srcVarName){
		var self=this;
		debugger;
		var theVar=self.getVar(srcVarName);
		var auxValue="";
		if (typeof theVar.value[fncName]!=="undefined") {
			auxValue=theVar.value[fncName]();
		}
		return self.assignOrCreateVar(tgtVarName,auxValue);
	}
	countToVar(tgtVarName,theArray){
		var self=this;
		debugger;
		//var theVar=self.getVar(srcVarName);
		var auxValue="";
		if (typeof theArray.length!=="undefined") {
			auxValue=theArray.length;
		}
		return self.assignOrCreateVar(tgtVarName,auxValue);
	}
	arrElementToVar(tgtVarName,index,theArray){
		var self=this;
		debugger;
		//var theVar=self.getVar(srcVarName);
		var auxValue="";
		if (typeof theArray.length!=="undefined") {
			auxValue=theArray.length;
			if (auxValue>index){
				auxValue=theArray[index];
			} else if (index<0){
				throw "Index "+index+" <0";
			} else {
				throw "Index "+index+" > "+ auxValue +" the length of array";
			}
		}
		return self.assignOrCreateVar(tgtVarName,auxValue);
	}
	
	
	setAttribute(varName,attName,vValue){
		debugger;
		var self=this;
		var theVar=self.getVar(varName);
		theVar.value[attName]=vValue;
	}
	pushAttribute(varName,attName,vValue){
		debugger;
		var self=this;
		var theVar=self.getVar(varName);
		if (typeof theVar.value[attName]==="undefined"){
			theVar.value[attName]=[];
		}
		theVar.value[attName].push(vValue);
	}
	
	existsVariable(varName,internal=false){
		var self=this;
		debugger;
		var bExists=self.existsVar(varName,internal);
		self.setReturnValue(bExists);
		return bExists;
	}
	
	existsVar(varName,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		for (var i=0;i<self.closures.length;i++){
			var closure=self.getClosure(internal,i);
			if (closure.existsVar(sVarName)){
				return true;
			}
		}
		return false;
	}
	addVar(varName,varValue,internal=false){
		var self=this;
		var closure=self.getClosure(internal);
		//if (internal) {console.log("added internal var "+varName+" value:"+varValue)}
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		closure.addVar(sVarName,varValue);
	}
	addVarAtLevel(varName,varValue,iLevel,internal=false){
		var self=this;
		var closure="";
		var maxLevel=self.closures.length-1;
		if (internal){
			maxLevel=self.runtimeClosures.length-1;
		}
		if (iLevel<0){
			var fromBottom=(-1)*iLevel;
			closure=self.getClosure(internal,fromBottom);
		} else {
			var fromUp=iLevel;
			if (iLevel>maxLevel){
				fromUp=maxLevel;
			}
			if (internal){
				closure=self.runtimeClosures[fromUp];
			} else {
				closure=self.closures[fromUp];
			}
		}
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;		
		closure.addVar(sVarName,varValue);
	}
	assignOrCreateVar(varName,varValue,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName;
		if (!self.existsVar(sVarName,internal)){
			var closure=self.getClosure(internal);
			closure.addVar(sVarName,varValue);
		} else {
			self.setVar(sVarName,varValue,internal);
		}
	}
	assignOrCreateUpperVar(varName,varValue,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+varName.trim();
		if (!self.existsVar(sVarName,internal)){
			var closure=self.getUpperClosure(internal);
			closure.addVar(sVarName,varValue);
		} else {
			self.setVar(sVarName,varValue,internal);
		}
	}
	extractAttributes(obj,internal){
		var self=this;
		//console.log(JSON.stringify(obj));
		for (const attrib of Object.keys(obj)){
			if (typeof obj[attrib]!=="function"){
				self.addVar((attrib.charAt(0)=="$"?"":"$")+attrib,obj[attrib],internal);
			}
		};
	}
	getDeepListOfVar(varName,internal=false){
		var self=this;
		var sVarName=(varName.charAt(0)=="$"?"":"$")+(varName.trim());
		var theClosure;
		var result=[];
		if (internal){
			theClosure=	self.runtimeClosures;
		} else {
			theClosure=	self.closures;
		}
		for (var i=0;i<theClosure.length;i++){
			if (theClosure[i].existsVar(sVarName)){
				result.push(theClosure[i].getVar(sVarName));
			}
		}
		return result;
	}
	setReturnValue(returnValue){
		var self=this;
		debugger;
		//console.log("Set the return value:"+returnValue);
		//var srcValue=await ScStore.processExpression(returnValue);
		var srcValue=returnValue; //the value must be processed before this call
		//var upperClosure=ScStore.getUpperClosure();
		var closure=self.getClosure();
		var tgtVarName="$LastReturnedVar";
		if (!closure.existsVar(tgtVarName)){
			closure.addVar(tgtVarName,srcValue);
		} else {
			closure.setVar(tgtVarName,srcValue);
		}
	}
	getReturnedValue(){
		var self=this;
		var closure=self.getClosure();
		var tgtVarName="$LastReturnedVar";
		if (!closure.existsVar(tgtVarName)){
			return undefined;
		} else {
			return closure.getVar(tgtVarName).value;
		}
	}
	assignReturnedToVariable(tgtVariableName){
		var self=this;
		//console.log("Assigning returned value");
		var srcValue=self.getReturnedValue();
		self.assignOrCreateVar(tgtVariableName,srcValue);
	}
	setDefaultVariables(defaultVars){
		var self=this;
		//console.log("---setDefaultVariables:"+JSON.stringify(defaultVars));
		//console.log("Closure Data (SDV)");
		//ScStore.traceClosures();

		var tgtVarName="$ScenarioDefaultVars";
		
		//var varValue=await ScStore.processExpression(defaultVars);
		var varValue=defaultVars; // de defaultVars come in processed....
		//console.log("Accesing ScStore:"+ScStore.closures.length);
		self.assignOrCreateVar(tgtVarName,varValue);
		var defaultParams=varValue;
		//var closure=ScStore.getUpperClosure();
		var closure=self.getClosure();
		//console.log("Filling variables with ScenarioDefaultVars:"+JSON.stringify(defaultParams)+" closureName:"+closure.name);
		var arrProperties=Object.getOwnPropertyNames(defaultParams);
		  //console.log("properties names array"+JSON.stringify(arrProperties));
		for (var i=0;i<arrProperties.length;i++){
			//console.log("prop "+i+":"+arrProperties[i]);
			var vPropName=arrProperties[i];
			if (vPropName!=="constructor"){
				if (!closure.existsVar(vPropName)){
					if (!ScStore.existsVar(vPropName)){
						var vPropValue=defaultParams[vPropName];
						closure.addVar(vPropName,vPropValue);
		//				console.log("Created var "+vPropName+" with value:" + JSON.stringify(vPropValue));
					} else {
						var vPropValue=self.getVar(vPropName).value;
						closure.addVar(vPropName,vPropValue);
					}
				}
			}
		} 
	}
	setDefaultVariable(varName,varValue){
		var self=this;
		var jsonObject={};
		var vName=varName;
		if (vName.charAt(0)!="$"){
			vName="$"+vName;
		}
		jsonObject[vName]=varValue;
		self.setDefaultVariables(jsonObject);
	}
	async fncVariableOperation(multipleArgs){
		var self=this;
		var nArgs=arguments.length;
		var mgrMethod=arguments[nArgs-1];
		var arrParams=[];
		var bckClosures=self.popClosure(false); // the step automatically pushed a closures.... playing with variables not need aditional closure
		for (var i=0;i<(nArgs-1);i++){
			arrParams.push(await ScStore.processExpression(arguments[i]));
		}
		var vReturn=await mgrMethod.apply(closureManager,arrParams);
		self.pushClosures(bckClosures);// the step automatically pushed a closures.... for uniform management of closures in steps
		return vReturn;
	}
}	

ScStore.existsLocalVar=function(varName,internal=false){
	return closureManager.existsLocalVar(varName,internal);
	}
ScStore.getLocalVar=function(varName,internal=false){
	return closureManager.getLocalVar(varName,internal);
	}
ScStore.getVar=function(varName,internal=false){
	return closureManager.getVar(varName,internal);
	}
ScStore.setVar=function(varName,varValue,internal=false){
	return closureManager.setVar(varName,varValue,internal);
	}
ScStore.pushVar=function(varName,varValue,internal=false){
	return closureManager.pushVar(varName,varValue,internal);
	}
ScStore.popVar=function(varName,internal=false){
	return closureManager.popVar(varName,internal)
	}
ScStore.existsVar=function(varName,internal=false){
	return closureManager.existsVar(varName,internal);
	}
ScStore.addVar=function(varName,varValue,internal=false){
	return closureManager.addVar(varName,varValue,internal);
	}
ScStore.assignOrCreateVar=function(varName,varValue,internal=false){
	return closureManager.assignOrCreateVar(varName,varValue,internal);
	}
ScStore.assignOrCreateUpperVar=function(varName,varValue,internal=false){
	return closureManager.assignOrCreateUpperVar(varName,varValue,internal);
	}
	
ScStore.setReturnValue=async function(returnValue){
	return await closureManager.fncVariableOperation(returnValue,closureManager.setReturnValue);
	}
ScStore.getClosuresDeep=function(){
	return closureManager.getClosuresDeep();
	}
	


var closureManager=new ScenarioClosureManager();
module.exports=closureManager;