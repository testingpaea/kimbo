const ScStore = require('../../scenarioStore');
const { execSync } = require( 'child_process' );
const { spawn } = require( 'child_process' );
const { exec } = require( 'child_process' );
const fs = require('fs');


var MultiScreenManager=class MultiScreenManager{
	constructor(){
		var self=this;
		self.totalScreens=0;
		self.actualScreen=0;
		self.enabled=false;
		self.progressBox="";
		self.inputBox="";
		self.bTakeScreenshots=false;
		self.sendScreenshotFreq=5000;
		self.lastPeriod=0;
	}
	screenshot(){
		var self=this;
		var numDisplay=(self.actualScreen+2);
		var filename="/tmp/screenshot_"+numDisplay+".png";
		var theCommand="import -display :"+numDisplay+" -window root "+filename;
		var result= execSync(theCommand);
		fs.readFile(filename, async function(err, data) {
			if (err) throw err; // Fail if the file can't be read.
			//console.log(data);
			await ScStore.getEvents().call("screenshot",{screenshot:data});
		});
	}
	setTotalScreens(totalScreens){
		this.totalScreens=totalScreens;
	}
	setActualScreen(actualScreen){
		this.actualScreen=actualScreen;
	}
	ensureVNC(){
		var self=this;
		var sDisplay="";
		var nDisplay=self.actualScreen;
		sDisplay=":"+(self.actualScreen+2);
		var sCommand='ps -ef | grep vncserver | grep "passwd '+sDisplay+'"';
		console.log("is vnc loaded:"+sCommand);
		var result=execSync(sCommand);
		console.log("result :"+result);
		if (result.indexOf("/usr/bin/perl /usr/bin/vncserver -PasswordFile /home/"+process.env.ENV_USER+"/.vnc/passwd "+sDisplay)>=0){
			console.log("The VNC is loaded");
		} else {
			//sCommand="su -c 'vncserver -PasswordFile /home/"+process.env.ENV_USER+"/.vnc/passwd "+sDisplay+"' "+process.env.ENV_USER;
			//sCommand="vncserver -PasswordFile /home/"+process.env.ENV_USER+"/.vnc/passwd "+sDisplay ;
			sCommand="vncserver -localhost no -PasswordFile /home/"+process.env.ENV_USER+"/.vnc/passwd "+sDisplay;
			var resolution=process.env.withResolution;
			if ((typeof resolution!=="undefined")&&(resolution!=="")){
				sCommand+=" -geometry "+resolution+" ";
			}
			//sCommand="sudo --user="+process.env.ENV_USER+" vncserver -PasswordFile /home/"+process.env.ENV_USER+"/.vnc/passwd "+sDisplay;

			console.log("launching vnc:"+sCommand);
			try {
				result=execSync(sCommand);
			} catch (error){
				console.log ("Error launching VNC in display "+error);
				return false;
			}
		}
		return true;
	}
	async closeInputBox(sResult){
		var self=this;
		var ibData=self.inputBox;
		self.inputBox="";
		var fncResolve=ibData.resolve;
		await self.closeZenityProcess(ibData.process);
		if (ScStore.isExtensionLoaded("events")){
			ScStore.getEvents().call("inputBox",{action:"CLOSE"});
		}
		return fncResolve(sResult);
	}
	async inputWindow(sText,bPassword){
		var self=this;
		var sDisplay="";
		sDisplay=" --display=:"+(self.actualScreen+2)+" ";
		var sCommand='zenity '+sDisplay+' --entry --text="'+sText+'"';
		var isPassword=false;
		if ((typeof bPassword!=="undefined")&&bPassword){
			isPassword=true;
			sCommand+=" --hide-text";
		}
		
		var fncResolve="";
		var fncReject="";
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		
		var bError=false;
		var sResult="";
		try {
			var zenityProcess = exec(sCommand, (err, stdout, stderr) => {
				if (err) {
					// node couldn't execute the command
					if (!err.killed){
						bError=true;
						console.log(err);
					}
					return;
				}
				// the *entire* stdout and stderr (buffered)
				console.log(`stdout: ${stdout}`);
				console.log(`stderr: ${stderr}`);
				sResult=stdout.toString('utf8').replace("\n","").trim();
				self.closeInputBox(sResult);
			});			
			self.inputBox={process:zenityProcess,reject:fncReject,resolve:fncResolve};
		} catch(error){
			debugger;
			bError=true;
			console.log(error);
			sMessage=sText;
		}
		
	//	console.log("Paramenters:"+parameters+" Typeof Result:"+typeof sResult + " json:"+JSON.stringify(sResult));
		if (ScStore.isExtensionLoaded("events")){
			ScStore.getEvents().call("inputBox",{action:"OPEN",password:isPassword,message:sText});
		}
		return prmResult;
	}
	async openPeriodProgressBox(sText,iPeriod,bConfirm=true){
		debugger;
		var self=this;
		var tsNow=Date.now();
		var tsLast=self.lastPeriod;
		var tsDiff=(tsNow-tsLast)/1000;
		var tsPeriod=parseInt(iPeriod);
		if (tsPeriod<tsDiff){
			await self.openProgressBox(sText,bConfirm);
			self.lastPeriod=tsNow;
		}
	}
	async openProgressBox(sText,bConfirm=true){
		debugger;
		var self=this;
		var sDisplay="";
		sDisplay=" --display=:"+(self.actualScreen+2)+" ";
		//zenity --title="Waiting for System Lock"  --progress --pulsate  --no-cancel --text="Waiting for system lock" 
		var sCommand='zenity '+sDisplay+' --progress --pulsate --title="Running..." --text="'+sText+'" --no-cancel --auto-close';
		var arrParams=[];
		arrParams.push(sDisplay);
		arrParams.push("--progress");
		arrParams.push("--pulsate");
		arrParams.push('--no-cancel');
		arrParams.push('--title="Running..."');
		arrParams.push('--text="'+sText+'"');

	    var bckProgressBoxZenityProcess=self.progressBox;		
		var bError=false;
		var sMessage=sText;
		try {
			var zenityProcess = exec(sCommand, (err, stdout, stderr) => {
				if (err) {
					// node couldn't execute the command
					bError=true;
					sMessage=err;
					return;
				}
			  // the *entire* stdout and stderr (buffered)
			  //console.log(`stdout: ${stdout}`);
			  //console.log(`stderr: ${stderr}`);
			});			
			self.progressBox=zenityProcess;
			if (bckProgressBoxZenityProcess!==""){
				await self.closeZenityProcess(bckProgressBoxZenityProcess,false);
			}
			
		} catch(error){
			debugger;
			bError=true;
			console.log("Error:"+error.toString());
			sMessage=sText;
		}
		if (ScStore.isExtensionLoaded("events")){
			ScStore.getEvents().call("progressBox",{action:"OPEN",message:sText});
		}
		if (bConfirm) {
			var fncResolve="";
			var fncReject="";
			var prmResult=new Promise((resolve, reject) => {
				fncResolve=resolve;
				fncReject=reject;
			});
			setTimeout(function(){
				if (!bError){
					fncResolve("Progress Box Opened with text:"+sMessage);
				} else {
					fncReject("Error opening Progress Box:"+sMessage);
				}
			},1000);
			return prmResult;
		} else {
			return "Progress Box Opened with text:"+sMessage;
		}
	}
	async closeProgressBox(bWaitForClose=true){
		var self=this;
		debugger;
		if (self.progressBox!==""){
			await self.closeZenityProcess(self.progressBox,bWaitForClose);
			self.progressBox="";
			if (ScStore.isExtensionLoaded("events")){
				ScStore.getEvents().call("progressBox",{action:"CLOSE"});
			}
		}
	}
	async closeZenityProcess(childprocess,bWaitForClose=true){
		debugger;
		var self=this;
		var fncResolve="";
		var fncReject="";
		var prmResult=new Promise((resolve, reject) => {
			fncResolve=resolve;
			fncReject=reject;
		});
		if (childprocess!==""){
			try {
				execSync('pkill -9 -P '+childprocess.pid );
			} catch (theException){
			}
			try {
				childprocess.kill('SIGKILL');
			} catch (theException){
			}
		}
		
		if (bWaitForClose){
			setTimeout(function(){
				fncResolve("Zenity process Killed");
			},1000);
			return prmResult;
		} else {
			return "Zenity process Killed";
		}
	}
	async showMessage(sMessage,seconds){
		debugger;
		var self=this;
		var sText=await ScStore.processExpression(sMessage);
		var nSeconds=3;
		if (typeof seconds==="string"){
			nSeconds=await ScStore.processExpression(seconds);
			nSeconds=parseFloat(nSeconds);
		}
		await self.openProgressBox(sText);
		console.log("Waiting "+nSeconds+" secs");
		var fncTickCallback=async function(tRemaining,tSpent,tTotal){
			//await screenManager.closeProgressBox();
			var zenityProcess=self.progressBox;
			await self.openProgressBox(sText+" ("+(tRemaining/1000).toFixed(2)+"s /"+(nSeconds.toFixed(2))+"s) ");
			if (zenityProcess!==""){
				await self.closeZenityProcess(zenityProcess);
			}
		}
		await ScStore.waitSecs(nSeconds,fncTickCallback);
		console.log("End of Waiting "+nSeconds+" secs");
		await self.closeProgressBox();
	}

}


var multiScreenManager=new MultiScreenManager();
module.exports=multiScreenManager;