const ScStore = require('../../scenarioStore');

const maxScenarioTimeout = 10000000;

const stepDefinitions = [
	{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'show progress box {string}', 
		stepFunction: async function(sMessage){
			debugger;
			var sText=await ScStore.processExpression(sMessage);
			var screenManager=ScStore.getMultiscreen();
			await screenManager.openProgressBox(sText);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'show progress box', 
		stepFunction: async function(sMessage){
			debugger;
			var sText=await ScStore.processExpression(sMessage);
			var screenManager=ScStore.getMultiscreen();
			await screenManager.openProgressBox(sText,false);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'show periodic {string} progress box', 
		stepFunction: async function(period,sMessage){
			debugger;
			var sText=await ScStore.processExpression(sMessage);
			var iPeriod=await ScStore.processExpression(period);
			var screenManager=ScStore.getMultiscreen();
			await screenManager.openPeriodProgressBox(sText,iPeriod,false);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'show progress box {string} for {int} seconds', 
		stepFunction: async function(sMessage,nSeconds){
			var screenManager=ScStore.getMultiscreen();
			await screenManager.showMessage(sMessage,nSeconds);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'show progress box {string} for {string} seconds', 
		stepFunction: async function(sMessage,sSeconds){
			var screenManager=ScStore.getMultiscreen();
			await screenManager.showMessage(sMessage,sSeconds);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'wait {int} seconds showing {string}', 
		stepFunction: async function(nSeconds,sMessage){
			var screenManager=ScStore.getMultiscreen();
			await screenManager.showMessage(sMessage,nSeconds);
		}
	},{ stepMethod: 'Given', stepTimeout: maxScenarioTimeout,
		stepPattern: 'close progress box', 
		stepFunction: async function(){
			debugger;
			var screenManager=ScStore.getMultiscreen();
			await screenManager.closeProgressBox();
		}
	},{ stepMethod: 'Given', stepPattern: 'user input window {string} to variable {string}', stepTimeout: 40000,
		stepFunction: async function(qText,tgtVariableName){
			var sText=await ScStore.processExpression(qText);
			var qResult=await ScStore.inputWindow(sText);
			console.log("["+qResult+"]");
			return await ScStore.getClosures().fncVariableOperation(tgtVariableName,qResult,ScStore.getClosures().assignOrCreateVar);
		}
	},{ stepMethod: 'Given', stepPattern: 'user input password window {string} to variable {string}', stepTimeout: 40000,
		stepFunction: async function(qText,tgtVariableName){
			var sText=await ScStore.processExpression(qText);
			var tgtVarName="$"+(await ScStore.processExpression(tgtVariableName));
			var qResult=await ScStore.inputWindow(sText,true);
			console.log("["+qResult+"]");
			return await ScStore.getClosures().fncVariableOperation(tgtVariableName,qResult,ScStore.getClosures().assignOrCreateVar);	  
		}
	}

];

module.exports = stepDefinitions;
