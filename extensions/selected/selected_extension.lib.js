const ScStore = require('../../scenarioStore');

var ScenarioSelectionManager=class ScenarioSelectionManager{
	constructor(){
		var self=this;
		self.dynamic=false;
		self.dynActualScenario="";
		self.list=[];
		self.excludeTags=[];
		self.mapList="";
		self.mapExcludeTags="";
		self.mapScenariosByTag=new Map();
		self.tagSelector="";
	}
	addScenario(scenario){
		var self=this;
		self.list.push(scenario);
		self.mapList.set(scenario,self.list.length-1);
	}
	refresh(){
		var self=this;
		self.mapList=new Map();
		self.mapExcludeTags=new Map();
		var bAll=false;
		self.list.forEach(function(item,index){
			if (item=="DYNAMIC"){
				self.dynamic=true;
				bAll=true;
			} else if(item!="ALL TESTS") {
				self.mapList.set(item,index);
			} else {
				bAll=true;
			}
		});
		if (bAll) {
			self.list=[];
			self.mapList=new Map();
		} 
		if ((!bAll)||(self.dynamic)) {
			self.excludeTags.forEach(function(item,index){
				self.mapExcludeTags.set(item,index);
			});
		}
	}
	setTagSelector(tagSelector){ // the tag will be allways in @TAG:VALUE format
		this.tagSelector=tagSelector;
	}
	setLists(includedList,excludedTags){
		var self=this;
		if (typeof excludedTags==="undefined"){
			self.excludeTags=[];
		} else {
			self.excludeTags=excludedTags;
		}
		if (typeof includedList==="undefined"){
			self.list=[];
		} else {
			self.list=includedList;
		}
		self.refresh();
	}
	getSelectorTagValue(tags){
		var self=this;
		var tagSel=self.tagSelector;
		for (var i=0;i<tags.length;i++){
			var theTag=tags[i];
			var tagPart=theTag.name.substring(0,tagSel.length);
			//console.log(tagSel +" vs "+ tagPart);
			if (tagSel==tagPart){
				var tagValue=theTag.name.substring(tagSel.length+1,theTag.name.length);
				return tagValue;
			}
		}
		return "";
	}
	isExcludedRootScenario(tags){
			// evaluate tags to omitt not selected scenarioStore
		//console.log("isExcludedRootScenario");
		var self=this;
		if (self.list.length==0){
			return false;
		}
		var lstExcludedTags=self.excludeTags;
		var mapExcludedTags=self.mapExcludeTags;
		var isRootScenario=true;
		var tagSel=self.tagSelector;
		for (var i=0;(isRootScenario)&&(i<tags.length);i++){
			var theTag=tags[i];
			if (theTag.name=="@Interface"){
				return false;
			} else if (theTag.name=="@subScenario"){
				return false;
			} else if (mapExcludedTags.has(theTag.name)){
				return false;
			} else if (self.dynamic){
				console.log("dynamic "+self.dynActualScenario);
				if (self.dynActualScenario!=""){
					return false;
				} else {
					return true;
				}
			} else {
				var tagPart=theTag.name.substring(0,tagSel.length);
				//console.log(tagSel +" vs "+ tagPart);
				if (tagSel==tagPart){
					var tagValue=theTag.name.substring(tagSel.length+1,theTag.name.length);
					//console.log(tagValue +" vs "+ JSON.stringify(self.list));
					if (self.mapList.has(tagValue)){
						console.log("Match not Excluded");
						return false;
					} else {
						console.log("Does not Match. Excluded!");
						return true;
					}
				}
			}
		}
	}
}


var scenarioSelectionManager=new ScenarioSelectionManager();
module.exports=scenarioSelectionManager;