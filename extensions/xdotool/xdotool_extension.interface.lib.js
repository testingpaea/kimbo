const {interfaceManager, AppInterface , InterfaceElement} = require('../interfaces/interfaceManager');
const XdotoolInterfaceElement=require('./xdotool_extension.interface_element.lib');
var XdotoolInterface=class XdotoolInterface extends AppInterface {
	constructor(manager,tags){
		super(manager,tags);
	}
	newElement(id){
		var self=this;
		return new XdotoolInterfaceElement(id,self);
	}

}

module.exports=XdotoolInterface;