const {interfaceManager, AppInterface , InterfaceElement} = require('../interfaces/interfaceManager');
const ScStore = require('../../scenarioStore');

var XdotoolElement=class XdotoolElement extends InterfaceElement{
	constructor(id,parentInterface){
		super(id,parentInterface);
		var self=this;
		self.fromPrevElement=[{action:"TAB",count:1,position:{x:0,y:0}}];
		self.clickPosition=false;
		self.position={x:0,y:0}
		self.caption="";
	}
	loadFromJson(jsonObject){
		super.loadFromJson(jsonObject);
		var self=this;
		self.caption=jsonObject.caption;
		self.setSelector(jsonObject.selector);
		var fncMovement=function(inputMove){
			var oResult={count:1};
			if (typeof inputMove==="string"){
				oResult.action=inputMove;
			} else {
				oResult.action=inputMove.action;
				if (inputMove.action=="CLICK"){
					oResult.position=inputMove.position;
				} 
				if (typeof inputMove.count!=="undefined") {
					oResult.count=inputMove.count;
				}
			}
			return oResult;
		}
		if (typeof jsonObject.fromPrevElement!=="undefined"){
			self.fromPrevElement=[];
			
			if (typeof jsonObject.fromPrevElement==="string"){
				self.focusable=true;
				self.fromPrevElement.push(fncMovement(jsonObject.fromPrevElement));
			} else if (!Array.isArray(jsonObject.fromPrevElement)) {
				self.focusable=true;
				self.fromPrevElement.push(fncMovement(jsonObject.fromPrevElement));
			} else {
				self.focusable=false;
				for (const fromPrev of jsonObject.fromPrevElement){
					self.focusable=true;
					self.fromPrevElement.push(fncMovement(fromPrev));
				};
			}
		}
		if (typeof jsonObject.focusable!=="undefined"){
			self.focusable=jsonObject.focusable;
		}
		if (typeof jsonObject.position!=="undefined"){
			self.position=jsonObject.position;
		}
		if (typeof jsonObject.clickPosition!=="undefined"){
			self.clickPosition=jsonObject.clickPosition;
		}
	}
	async waitFor(arrConditions){
		console.log("waiting for:"+JSON.stringify(arrConditions));
		var self=this;
		var bResult=true;
		if (self.selector==="") return bResult;
		var selector=self.selector;
		console.log("Selector:"+JSON.stringify(selector));
		var xdoManager=ScStore.getXdotool();
		if (selector.type=="classname"){
			console.log("Wait for element:"+arrConditions.length+" "+arrConditions[0]);
			var arrWindows=xdoManager.searchWindowsByClass(selector.expression);
			console.log("Windows "+selector.expression+":"+JSON.stringify(arrWindows));
			for (var i=0;(i<arrConditions.length)&&(bResult);i++){
				var cond=arrConditions[i];
				if (cond=="ACTIVE"){
					try {
						var bLocated=false;
						var wId="";
						for (var j=0;(j<arrWindows.length)&&(!bLocated);j++){
							var wId=arrWindows[j];
							var windowName=xdoManager.getWindowName(wId);
							windowName=windowName.trim();
							//console.log("Window Name:###"+windowName+"### vs Caption:###"+self.caption+"###");
							if (self.caption!==""){
								if (windowName==self.caption){
									bLocated=true;
								}
							} else {
								bLocated=true;
							}
						}
						if (bLocated){
							xdoManager.activateWindow(wId);
						} else {
							bResult=false;
						}
					} catch(error){
						//console.log("Active....Error:"+error);
						bResult=false;
					}
				}
			}
			return bResult;
		} else if (selector.type=="name"){
			debugger;
			console.log("Wait for element:"+arrConditions.length+" "+arrConditions[0]);
			var arrWindows=xdoManager.searchWindowsByCaption(selector.expression);
			console.log("Windows "+selector.expression+":"+JSON.stringify(arrWindows));
			for (var i=0;(i<arrConditions.length)&&(bResult);i++){
				var cond=arrConditions[i];
				if (cond=="ACTIVE"){
					try {
						var bLocated=false;
						var wId="";
						for (var j=0;(j<arrWindows.length)&&(!bLocated);j++){
							var wId=arrWindows[j];
							var windowName=xdoManager.getWindowName(wId);
							windowName=windowName.trim();
							//console.log("Window Name:###"+windowName+"### vs Caption:###"+self.caption+"###");
							if (self.caption!==""){
								if (windowName==self.caption){
									bLocated=true;
								}
							} else {
								bLocated=true;
							}
						}
						if (bLocated){
							xdoManager.activateWindow(wId);
						} else {
							bResult=false;
						}
					} catch(error){
						//console.log("Active....Error:"+error);
						bResult=false;
					}
				}
			}
			return bResult;
		};
		return bResult;
	}
	async click(){
		var self=this;
		var xdoManager=ScStore.getXdotool();
		console.log("xdotool click:"+self.id);
		if (typeof self.clickPosition==="undefined"){
			
		} else {
			console.log("xdotool sending space");
			xdoManager.sendKeys("space");
		}
		console.log("Clicked:"+self.id);
	}
	async focus(){
		var self=this;
		var xdoManager=ScStore.getXdotool();
		console.log(JSON.stringify(self.fromPrevElement));
		for (const actionDef of self.fromPrevElement){
			console.log("Action for focus:"+(typeof actionDef)+ " json:"+JSON.stringify+" --> "+ actionDef);
			if (actionDef.action=="CLICK"){
			} else if (actionDef.action=="TAB"){
				xdoManager.sendKeys("Tab",actionDef.count);
			}
		};
		
	}
	async selectOptionNumber(iItem){
		var xdoManager=ScStore.getXdotool();
		await xdoManager.sendKeys("Home");
		await xdoManager.sendArrowKey("DOWN",iItem);
	}
	
	async writeText(sText){
		console.log("XDO Interface writeText");

		console.log("Writting ["+sText+"]");
		var xdoManager=ScStore.getXdotool();
		await xdoManager.writeText(sText);
		console.log("Writted ["+sText+"]");
	}
}

module.exports=XdotoolElement;