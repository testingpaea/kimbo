const { execSync } = require( 'child_process' );
const ScStore = require('../../scenarioStore');

const XdotoolInterface = require('./xdotool_extension.interface.lib');
const XdotoolInterfaceElement = require('./xdotool_extension.interface_element.lib');

// stderr is sent to stderr of parent process
// you can set options.stdio if you want it to go elsewhere
var xdotoolDriver=class xdotoolDriver{
	constructor(){
		var self =this;
		self.lastError=0;
		self.lastResult="";
	}
	wait(nSecs){
		try {
			console.log("Sleeping");
			execSync ( 'sleep '+nSecs);
			console.log("Sleeping End");
		} catch (error){
			console.log("Error in Sleep");
		}
	}
	execCommand(parameters){
		var sDisplay="";
		var theCommand="";
		if (ScStore.isExtensionLoaded("multiscreen")&&ScStore.getMultiscreen().enabled){
			var screenMgr=ScStore.getMultiscreen();
			theCommand=process.cwd()+'/xdotooldisp '+(ScStore.getMultiscreen().actualScreen+2)+" "+parameters;
		} else {
			theCommand='xdotool '+parameters;
		}
		console.log(theCommand);
		var result= execSync(theCommand);
		var sResult=result.toString('utf8');
	//	console.log("Paramenters:"+parameters+" Typeof Result:"+typeof sResult + " json:"+JSON.stringify(sResult));
		return sResult;
	}
	getDisplayParam(){
		var sDesktop;
		return sDesktop;
	}
	searchWindowsByClass(className){
		var sList="";
		try {
			sList= this.execCommand("search --onlyvisible -classname "+className);
			console.log("{"+sList+"}");
		} catch (error){
			return [];
		}
		return sList.trim().split("\n");
	}
	searchWindowsByCaption(windowCaption){
		var sList="";
		try {
			sList= this.execCommand("search --onlyvisible -name \""+windowCaption+"\"");
			console.log("{"+sList+"}");
		} catch (error){
			return [];
		}
		return sList.trim().split("\n");
	}
	getActiveWindowName(){
		var sWindowName="";
		try {
			sWindowName= this.execCommand("getactivewindow getwindowname");
		} catch (error){
			return error;
		}
		return sWindowName;
	}
	getWindowName(wId){
		var sWindowName="";
		try {
			sWindowName= this.execCommand("getwindowname "+wId);
		} catch (error){
			return error;
		}
		return sWindowName;
	}
	activateWindow(wId){
		try {
			this.execCommand("windowactivate --sync "+wId);
		} catch (error){
			return error;
		}
	}

	closeAllWindowsOfClass(className){
		var self=this;
		var arrWindows=self.searchWindowsByClass(className);
		for (var i=0;i<arrWindows.length;i++){
			var wId=arrWindows[i];
			if (wId!==""){
				console.log("Closing window:["+wId+"]");
				self.execCommand("windowactivate --sync "+wId+" key --clearmodifiers --delay 100 alt+F4");
			}
		}
	}
	async sendArrowKey(key,nTimes){
		var self=this;
		var sKey="";
		var sAuxKey=key.toUpperCase();
		if (sAuxKey=="DOWN"){
			sKey="Down";
		} else if (sAuxKey=="UP"){
			sKey="Up";
		} else if (sAuxKey=="LEFT"){
			sKey="Left";
		} else if (sAuxKey=="RIGHT"){
			sKey="Right";
		}
		await self.sendKeys(sKey,nTimes);
	}
	sendKeys(keys,nTimes){
		var self=this;
		var nKeys=1;
		if (typeof nTimes!=="undefined"){
			if (typeof nTimes=="string"){
				nKeys=parseInt(nTimes);
			} else {
				nKeys=nTimes;
			}
		} 
		console.log("Sending Keys ["+keys+"] "+ nTimes +"->"+nKeys+" times");
		for (var i=0;i<nKeys;i++){
			self.execCommand("key --clearmodifiers "+keys);
		}
	}
	writeText(sText,nTimes){
		var self=this;
		var nKeys=1;
		if (typeof nTimes!=="undefined"){
			if (typeof nTimes=="string"){
				nKeys=parseInt(nTimes);
			} else {
				nKeys=nTimes;
			}
		} 
		console.log("Writing Text ["+sText+"] "+ nTimes +"->"+nKeys+" times");
		for (var i=0;i<nKeys;i++){
			self.execCommand("type --clearmodifiers "+sText);
		}
	}
	newInterface(itf,tags){
			var self=this;
			var baseInterface=new XdotoolInterface(self,tags);
			return baseInterface;
	}
}
xdoDriver=new xdotoolDriver();

module.exports = xdoDriver;
