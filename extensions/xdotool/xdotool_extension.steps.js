const xdoDriver = require('./xdotool_extension.lib');
const ScStore = require('../../scenarioStore');

//const fncGet=require('./run.steps.js').getExecuteFeatures;
//Defining more steps (has to be in a .stepdef.js file)
const stepDefinitions = [{
    stepMethod: 'Given',
    stepPattern: 'close all browsers',
    stepTimeout: 40000,
    stepFunction: async function(){
	  xdoDriver.closeAllWindowsOfClass("chrom");
      return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'arrow {string}',
    stepTimeout: 40000,
    stepFunction: async function(KEY){
	  await xdoDriver.sendArrowKey(await ScStore.processExpression(KEY));
      return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'arrow {string}, repeat {string} times',
    stepTimeout: 40000,
    stepFunction: async function(KEY,nTimes){
	  await xdoDriver.sendArrowKey(KEY,parseInt(nTimes));
      return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'send enter',
    stepTimeout: 40000,
    stepFunction: async function(){
	  await xdoDriver.sendKeys("KP_Enter");
      return;
    }
  },{
    stepMethod: 'Given',
    stepPattern: 'send keys {string}',
    stepTimeout: 40000,
    stepFunction: async function(KEYS){
	  var sKeys=await ScStore.processExpression(KEYS);
	  console.log("Sending Keys:["+sKeys+"]");
	  await xdoDriver.sendKeys(sKeys);
      return;
    }
  }
];


module.exports = stepDefinitions;
