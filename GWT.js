const GWTsteps = (function(){
  const cucumber = require("@cucumber/cucumber");
  const { stepFunctions, stepDefinitions } = require('./stepFunctionsStore');
  const sHelper = require('./stepHelper.lib');
  var arity = require('util-arity');
  const OtherWorld = require('./OtherWorld');
  const ScStore = require('./scenarioStore');
  debugger;
  console.log("GWTsteps start");
  var fn = function(thisStepDef){
    return async function () {
		debugger;
      var stepFunctionName = sHelper.toFunctionName(thisStepDef.stepPattern);

	  //console.log(stepFunctionName+ " Arguments:"+JSON.stringify(arguments));
      OtherWorld.appendStepFunction(stepFunctionName, arguments,thisStepDef.stepTimeout);
	  //console.log("GWT ("+OtherWorld.isCompiling+")call:"+stepFunctionName);
	  var bIsCompiling=OtherWorld.getCompiling();
	  var ifVar=true;
	  //console.log(stepFunctionName+" Existe IfResult?"+ScStore.getClosures().getUpperClosure(true).existsVar("$IfResult"));
	  if (ScStore.getClosures().getUpperClosure(true).existsVar("$IfResult")){
		 // console.log("Exists internal local var ifresult:"+ScStore.getClosures().getUpperClosure(true).existsVar("$IfResult"));
//		  ScStore.waitSecs(10);
		  ifVar=ScStore.getClosures().getUpperClosure(true).getVar("$IfResult").value;
		//  console.log("Execute if branch:"+ifVar + " json:"+JSON.stringify(ifVar));
	  } else {
		//  console.log("The variable IfResult does not exists in upper closure");
	  }
	  if ((stepFunctionName=="else")||(stepFunctionName=="end_if")||(stepFunctionName=="elseif_STRING")
		  ){
		  ifVar=true;
		//  console.log(stepFunctionName+".... executing:"+ifVar);
	  }
	  
	  
	  
	  if (OtherWorld.forceExecution){
		//  console.log("Forcing Execution:"+OtherWorld.forceExecution);
		  bIsCompiling=false;
	  }
	  /*console.log("Step Name:"+stepFunctionName
						+" Will be Executed:"+((!bIsCompiling) && (ifVar) && (!OtherWorld.hardExcluded) &&
										(OtherWorld.currentScenarioTag && !OtherWorld.isScenarioFunction))
						+" is Compiling:"+bIsCompiling+" ifVar:"+ifVar+" hard excluded:"+OtherWorld.hardExcluded
						+" currentScenarioTag:"+OtherWorld.currentScenarioTag +" isScenarioFunction:"+OtherWorld.isScenarioFunction
		);
		*/
      if ((!bIsCompiling) && (ifVar) 
		  && (!OtherWorld.hardExcluded) &&
		(OtherWorld.currentScenarioTag && !OtherWorld.isScenarioFunction)){
        try{
			await stepFunctions[stepFunctionName].bind(OtherWorld.worldToBind)(...sHelper.replaceVariables(arguments));
        }catch(err){throw new Error(err.stack);}
      } else if (!ifVar) {
//		  console.log("Skipped branch in if");
	  }
      return;
    };
  };
  cucumber.AfterAll(async function(){
	  // Asynchronous Promise
	  if (ScStore.isExtensionLoaded("events")){
		  await ScStore.getEvents().finishedAll();
		  var rootList=ScStore.getRootScenarios();
		  console.log("================= ROOT SCENARIOS ===========");
		  console.log(JSON.stringify(rootList));
		  console.log("============================================");
	  }
	  OtherWorld.setCompiling(false);
//	  return Promise.resolve();
  });

  cucumber.Before(function(scenario){
    var tags = scenario.pickle.tags; //get tags from this scenario
	//console.log("GWT before:"+JSON.stringify(scenario.pickle.name)+" tags:"+JSON.stringify(tags));
	var bHardExcluded=false;
	//console.log("extension selected loaded:"+ScStore.isExtensionLoaded("selected"));
	if (ScStore.isExtensionLoaded("selected")){
		bHardExcluded=ScStore.getSelected().isExcludedRootScenario(tags);
		if (bHardExcluded){
			//console.log("HARD EXCLUDING");
		}
	}

    OtherWorld.resetProperties();
	OtherWorld.hardExcluded=bHardExcluded;
    if(tags.length>0) { 
	  debugger;
      //var scenarioTag = sHelper.firstScenarioTag(tags);    // select first tag in the last line 
	  var scenarioTag = tags[0];
	  ScStore.getClosures().newClosure(scenarioTag.name);
      OtherWorld.setCurrentScenarioTag(scenarioTag.name);  // set current scenario Tag in CustomWorld
	  OtherWorld.setCurrentScenarioName(scenario.pickle.name);
	  OtherWorld.setCurrentScenarioObject(scenario);
	  OtherWorld.allTags=tags;
    }
//    console.log('-------------NEW SCENARIO-----------',tags[0].name);
    OtherWorld.worldToBind = this;
    return;
  });
  cucumber.Before("@startRunning",function (theScenario,callback) {
	//console.log("Before Start Running");
	var bProcess=true;
	if (ScStore.isExtensionLoaded("selected")){
		if (ScStore.getSelected().dynamic){
			bProcess=false;
		}
	}
	if (bProcess){
		OtherWorld.setCompiling(false);
	}
	return callback();
  })

  cucumber.Before("@Interface",function (theScenario,callback) {
	//console.log("It is Interface");
	debugger;
    OtherWorld.forceExecution=true;
	return callback();
  })
  cucumber.After("@Interface",function (theScenario,callback) {
	//console.log("Close Interface");
	debugger;
    OtherWorld.forceExecution=false;
	return callback();
  })
  cucumber.Before("@InterfaceNavigation",function (theScenario,callback) {
	//console.log("It is Interface Navigation JSON");
	debugger;
    OtherWorld.forceExecution=true;
	return callback();
  })
  cucumber.After("@InterfaceNavigation",function (theScenario,callback) {
	//console.log("Close Interface Navigation JSON");
	debugger;
    OtherWorld.forceExecution=false;
	return callback();
  })
  
  cucumber.Before("@QUERY",function (theScenario,callback) {
    OtherWorld.forceExecution=true;
	return callback();
  })
  cucumber.After("@QUERY",function (theScenario,callback) {
    OtherWorld.forceExecution=false;
	return callback();
  })
  cucumber.Before("@SQL",function (theScenario,callback) {
    OtherWorld.forceExecution=true;
	return callback();
  })
  cucumber.After("@SQL",function (theScenario,callback) {
    OtherWorld.forceExecution=false;
	return callback();
  })
  cucumber.Before("@Provider",function (theScenario,callback) {
    OtherWorld.forceExecution=true;
	return callback();
  })
  cucumber.After("@Provider",function (theScenario,callback) {
    OtherWorld.forceExecution=false;
	return callback();
  })

  for (const fileName of Object.keys(stepDefinitions)){
  	for (const stepDef of Object.keys(stepDefinitions[fileName])){
	  debugger;
      const thisStepDef = stepDefinitions[fileName][stepDef];
      const cucumberMethod = thisStepDef.stepMethod;
	  //console.log("Step:"+stepDef+" :"+JSON.stringify(thisStepDef));
      const fnCucumber = arity(thisStepDef.stepFunction.length, fn(thisStepDef)); //sets number of arguments to function returned from "fn(worldMethod)"
      cucumber[cucumberMethod](
		thisStepDef.stepPattern,
		{ timeout: thisStepDef.stepTimeout || sHelper.defaultTimeout},
		fnCucumber);
    };
  };

  cucumber.After(function(scenario){
//	console.log("<<<<<<<<<<<<<GWT after:"+JSON.stringify(scenario)+">>>>>>>>>>>>>>>>>");
	if (OtherWorld.getCompiling()){
		if(OtherWorld.currentScenarioTag) {  
			OtherWorld.saveInScStore(); 
			ScStore.getClosures().popClosure();
		}
	} else {
		ScStore.getEvents().processed(scenario);
	}
    return;
  });
  
})();

module.exports = GWTsteps;


