const OtherWorld = require('./OtherWorld');
const ScStore = require('kimbo/scenarioStore');

console.log("=====CUCUMBER ALL ENVIRONMENT VARIABLES======");
console.log(process.env);
console.log("===========");
console.log("ENV_USER:"+process.env.ENV_USER);
console.log("NUM_AGENTS:"+process.env.withNumAgents);
console.log("Env is Controller:"+process.env.withIsController);

var enableScenarioCalls = function (showStepdefFiles=false,arrExtensions) {
  var StepFunctionsStore = require('./stepFunctionsStore');
  OtherWorld.showStepdefFiles = showStepdefFiles;
  ScStore.enableExtensions(arrExtensions);  
  ScStore.mapNoRootTags.set("@subScenario","subScenario");
  ScStore.mapNoRootTags.set("@startRunning","startRunning");
  ScStore.mapNoRootTags.set("@showResults","showResults");
  var {stepDefinitions,stepFunctions} = require('./stepFunctions')(showStepdefFiles);
  for (const fileName of Object.keys(stepDefinitions)){
	StepFunctionsStore.stepDefinitions[fileName]=stepDefinitions[fileName];
  };
  for (const stepFunction of Object.keys(stepFunctions)){
	StepFunctionsStore.stepFunctions[stepFunction]=stepFunctions[stepFunction];
  };
  var GWTsteps = require('./GWT');
  debugger;
  console.log("Cucumber Loaded");
  return;
};

module.exports = enableScenarioCalls;