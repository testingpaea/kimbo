const OtherWorld = require('./OtherWorld');

const stepHelper = {
  defaultTimeout: 5000,

/*  firstScenarioTag(tags){
	debugger;
    var lines = tags.map(tag=>tag.location.line);
    var maxLine = Math.max.apply(null, lines);
    var columns = tags.map(tag=>tag.location.column);
    var minColumn = Math.min.apply(null, columns);
    var tagMaxLineMinColumn = tags.filter(tag=> tag.location.line === maxLine && tag.location.column == minColumn);
    return tagMaxLineMinColumn[0];
  },
 */ 
  toFunctionName(stepPattern){  //converts stepName to functionName
    //stepPattern = 'the user {int} is on {string} page';  returns 'the_user_INT_is_on_STRING_page'
    var paramArray = stepPattern.match(/\{(.*?)\}/g);  // this regex: https://stackoverflow.com/a/1493071/7491858
    var stepPatternUpperParam;
    if(paramArray!==null){
      stepPatternUpperParam = paramArray.reduce((a,b)=>{
        var c = b.slice(1,-1).toUpperCase();
        return a.replace(b,c);
      },stepPattern);
    }
    else{
      stepPatternUpperParam = stepPattern;
    }
    return stepPatternUpperParam.replace(/[^a-zA-Z0-9 ]/g, "").replace(/\s/g, "_");  //keep only letters, numbers and spaces then replace spaces with underscores
  },

  toStepFunctions(stepDefinitions){
	var self=this;
    stepFunctions = {};
    function addStepDef(stepDef){
		  var stepPattern = stepDef.stepPattern;
		  var stepTimeout = stepDef.stepTimeout || self.defaultTimeout;
		  var funcName = self.toFunctionName(stepPattern);
		  var stepFunction = stepDef.stepFunction;
		  stepFunction.stepPattern = stepPattern;
		  stepFunction.stepTimeout = stepTimeout;
		  
		  Object.keys(stepDef).map((key)=>{
			  if ((key!=="stepPattern") &&
			  		(key!=="stepTimeout") &&
			  		(key!=="stepFunction")){
				var attr=stepDef[key]; 
				 if (typeof stepDef!=="function"){
					 stepFunction[key]=attr;
				 }
			  }
		  });
		  stepFunctions[funcName] = stepFunction;
	}
	if (Array.isArray(stepDefinitions)){
		for (const stepDef of stepDefinitions){
		  //console.log("Processing:"+self.toFunctionName(stepDef.stepPattern));
		  addStepDef(stepDef);
		};
	} else {
		Object.keys(stepDefinitions).map((key)=>{
		  addStepDef(stepDefinitions[key]);
		});
	}
    return stepFunctions;
  },

  async promiseTimeout(inMS, promise,sArguments){
    // Create a promise that rejects in <ms> milliseconds
	//console.log("Will error in "+ms+" milliseconds");
	debugger;
	var ms=inMS;
	if ((typeof ms==="undefined")||(isNaN(inMS))){
		console.log("ERROR.... THE FUNCTION TIMEOUT IS NOT SET OR ERRONEOUS");
		ms=1000*60*3;
	}
	var bNotTimeOut=false;
	var idTimeout="";

    let timeout = new Promise((resolve, reject) => {
      idTimeout= setTimeout(() => {
        debugger;
        clearTimeout(idTimeout);
        if (bNotTimeOut){
			console.log("Init: Kimbo says: Scenario was resolved before timeout. Everything OK");
			console.log(sArguments);
        	resolve("Kimbo says: Scenario ended before timeout. Everything OK");
		} else {
        	reject('Kimbo says: ERROR Function timed out after '+ ms + ' milliseconds.')
			console.log(sArguments);
        }
      }, ms)
    });
    // Returns a race between our timeout and the passed in promise
    return Promise.race([
      promise,
      timeout
     ]).then((value)=>{ 
		bNotTimeOut=true;
        clearTimeout(idTimeout);
        //console.log("Scenario finished before timeout:");
		//console.log("Arguments:"+sArguments);
		//console.log("Scenario Result:<<"+JSON.stringify(value)+">>");
		return {value:value,comment:"only for test race conditions",args:sArguments};
	 });
  },

  replaceVariables(argsArray, varObj) {
    //console.log('arguments',arguments);
    return Object.keys(argsArray).map(key => {
      var arg = argsArray[key];
      var replacedArg;
      if(typeof(arg)==='string')
      {
        replacedArg = this.replaceVariable(arg, varObj);
      }
      else
      {replacedArg = arg;}
      //console.log('replacedArg',replacedArg);
      return replacedArg;
    });
  },
  
  replaceVariable(str_arg_0, varObj ={}) {
    //console.log('ARGUMENTS',arguments);
    var str_arg = str_arg_0.toString();
    var deepVarsArray = this.getDeepest(str_arg);
    //console.log('deepVarsArray',deepVarsArray);
    if (deepVarsArray.length === 0) {
      var deepZero = str_arg;
      var replaced = this.getNested(varObj,deepZero) || this.getNested(OtherWorld.varStore,deepZero) || deepZero;
      while(replaced!==deepZero){
        deepZero = replaced;
        replaced = this.getNested(varObj,deepZero) || this.getNested(OtherWorld.varStore,deepZero) || deepZero;
      }
      return replaced;
    }
    else {
      var str_arg_1 = this.replaceDeepest(str_arg, varObj);
      return this.replaceVariable(str_arg_1, varObj);
    }
  },

  replaceDeepest (str_arg, varObj){
    var newStr_arg='';
    var deepVarsArray = this.getDeepest(str_arg);
    var parts=[];
    var posInit = 0;
    for (var i=0;i<deepVarsArray.length;i++) {
      var [pos, last, depth, str_deepest] = deepVarsArray[i];
      parts.push(str_arg.slice(posInit,pos-1));
      posInit = last+1;
    }
    parts.push(str_arg.slice(posInit));
  
    for (var i=0;i<deepVarsArray.length;i++) {
      var replacedValue = this.getNested(varObj,deepVarsArray[i][3]) || this.getNested(OtherWorld.varStore,deepVarsArray[i][3]);
      newStr_arg+=parts[i]+'.'+replacedValue;
    }
    newStr_arg+=parts[parts.length-1];
    return newStr_arg;
  },

  getDeepest (str_arg) {
    var depth = 0;
    var maxDepth = 0;
    var match, pos, last, bracketsObj = [];
    var regex = /(\[\$|\])/g;
    var brackets = str_arg.match(regex);
  
    for (var i=0;i<(brackets||[]).length;i++) {
      bracket=brackets[i];
      if (bracket === "[$") 
      {
        depth++;
        if (depth > maxDepth) 
        { 
          maxDepth = depth; 
        }
      }
      if (bracket === "]") 
      {
        depth--; 
      }
      match = regex.exec(str_arg);
      pos = regex.lastIndex - match[0].length;
      bracketsObj.push({ bracket, depth, pos});
    }
  
    var maxDepthArray = bracketsObj.map((bracketObj,i)=>{
      if(bracketObj.depth===maxDepth)
      {return i;}
    }).filter(elem=>(elem!=null)?true:false);
  
    return maxDepthArray.map(maxI=>{
      last = bracketsObj[maxI+1].pos;
      pos = bracketsObj[maxI].pos+1;
      str_deepest = str_arg.slice(pos,last);
      return [pos, last, maxDepth, str_deepest];
    });
  },

  getNested (theObject, path, separator='.') {
    try {
        return path.
                replace('[', separator).replace(']','').
                split(separator).
                reduce(
                    function (obj, property) { 
                        return obj[property];
                    }, theObject
                );
                    
    } catch (err) {
        return undefined;
    }   
  },
}

module.exports = stepHelper;