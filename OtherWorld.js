const ScStore = require('./scenarioStore');
debugger;
var OtherWorld = {
  functionList: [],
  functionTimeout: 0,
  currentScenarioTag: undefined,
  currentScenarioName: undefined,
  currentScenario:undefined,
  allTags:[],
  saved: false,
  worldToBind:{},
  isScenarioFunction: false,
  forceExecution:false,
  StepFunctionsStore: undefined,
  varStore: {},   //cucumber variables Storage
  isCompiling:true,
  getCompiling(){
	return this.isCompiling;
  }, 
  setCompiling(bNewCompiling){
	this.isCompiling=bNewCompiling;
  },
  
  setCurrentScenarioTag(currentScenarioTag){
    this.currentScenarioTag = currentScenarioTag.replace('@','');
    if(/\$/.test(this.currentScenarioTag))
    {this.isScenarioFunction=true;}
  },
  setCurrentScenarioName(currentScenarioName){
    this.currentScenarioName = currentScenarioName;
  },
  setCurrentScenarioObject(currentScenario){
	this.currentScenario=currentScenario;
  },

  appendStepFunction(functionName,functionArguments,functionTimeout) {
    var newFunctionElem = {functionName,functionArguments,functionTimeout};
    debugger;
	var step=this.currentScenario.pickle.steps[this.functionList.length];
	var stepLocation={};
	stepLocation.sourceFile=this.currentScenario.gherkinDocument.uri;
//	stepLocation.stepLine=step.locations[0];
//	newFunctionElem.functionLocation=stepLocation;
    this.functionList = this.functionList.concat(newFunctionElem);
	var auxTimeout=functionTimeout;
	if ((typeof auxTimeout==="undefined")||(isNaN(auxTimeout))||(auxTimeout==="")){
		auxTimeout=40000;
	}
    this.functionTimeout += auxTimeout;
    //console.log("this.functionList",JSON.stringify(this.functionList, null, 4));
  },

  saveInScStore() {
	//console.log("Saving "+this.currentScenarioTag+"... it was saved before:"+this.saved +" FunctionList:"+JSON.stringify(this.functionList));
    if(!this.saved)
    {
		ScStore.saveActualScenario();
		this.saved = true;
    }
  },
  getScStore(){
	  return ScStore;
	  
  },

  resetProperties(){
    this.functionList = [];
    this.functionTimeout = 0;
    this.currentScenarioTag = undefined;
    this.saved = false;
    this.worldToBind = {};
    this.isScenarioFunction = false;
  }
}
ScStore.setWorld(OtherWorld);

module.exports = OtherWorld;