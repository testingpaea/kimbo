# kimbo

Enable developers create complex automated test coding only Gherkin files.

Based on the virgomax�s great library https://github.com/Virgomax/reuse-cucumber-scenarios (RCS onwards) this library adds to RCS a "extension system" to increase the functionality with useful Testing and RPA tools/steps.
 
The project includes all the RCS files (code and documentation) to keep the Virgomax project coherence. Some files like parameter are not used in the kimbo�s model because Kimbo has replaced by his own (extended) system.
 
**Testing/RPA model**

The human interaction with the applications is based on three concepts:
 
- the **Interfaces** with the user receives and sends information to the application
- the information or **Data** that the human receives/sends to the application through the interfaces
- the **Sequence** of interactions that the human uses through different interfaces and sending or receiving information from the application.

The kimbo�s model consideres that there is a lot of functional test to run and it�s necessary to parallelize the test execution in multiple kimbo agents that have be controlled by an application (controller or host application)
 

**Conceptual Example**
 
*Considering the user is in front of a resetted computer*

1. *Given* in "desktop"
2. *Given* click in "firefox" browser
3. *Given* wait for "firefox" appears
4. *Given* write "https://bitbucket.org" in "url" inputbox
5. *Given* send "ENTER" key
6. *Given* wait for load of interface "bitbucket login"
7. *Given* write "$username" to input "username"
8. *Given* write "$password" to input "password"
9. *Given* click in button "Login" and wait for load of interface "repositories" 
10. *Given* click in "$kimbo" and wait for load of interface "repository detail"

This example has:

 - Sequence: the example is a pseudogherkin that represents a simple sequence of interactions 
 - Interfaces: "Desktop", "Firefox", "bitbucket login","repositories" and "repository detail". Each interface can be "selenium" or "xdotool" type and includes the information for kimbo interacts with the elements (xpath for selenium, secuence of tabs for xdotool, etc)
 - Data: "$username","$password" to enable do login with diferent users and "$kimbo" to enter in diferent repositories.

If some interface changes (example: "bitbucket login" selenium interface), the developer only need to change the "interface file" that includes the list of elements that compounds it and  set, if necessary, the new xpath to the button "Login". 

Specially in web interfaces, if the interface is well designed (with an ID setted in each interactive element) the changes of interfaces only are needed when new elements are added and will be used in new testing/rpa sequences. In linux desktop interfaces only are allowed "tab" navigation o "X, Y" click to selection of elements. 

**Approach**

The kimbo�s approach is based on the creation of a lot of reusable steps classified on:

   - Simple actions: "click", "write", "wait", "set var",  ...
   - Complex actions: "execute scenario", "default var", "if", "while", "create object", "create interface",... 
   
With all of this steps can write complex test and RPA�s using only gherking languaje reusing scenarios to simplify maintenance


**Kimbo includes steps to:**

   - Call to another scenarios passing variables, define default variables/objects to use if no variables passed, etc.
   - Manage variables with scope visibility and expression evaluation (simple and complex)
   - Control structures: IF (elseif, else), LOOP (while,foreach)
   - Create and Manage custom objects in "scene execution time" and in "compile time" with method/steps and dinamic attributes.
   - Selection of scenarios and interfaces with same name depending of environment variables (APP, ENV, custom) using scenario tags
   - Selenium automation tests interaction
   - Xdotool commands 
   - SQL connection and query execution
   - Manage multiscreen tests in linux multi-desktop systems. Activate window, check if exists, etc.
   - Create (selenium and xdotool) interface objects to simplify the interaction steps and isolate future changes in the user interfaces.
   - Event manager to link the kimbo/cucumber processor with a controller app
   - Extension mechanism to simplify creation of new custom elements and steps in the applications
   
**TO DO:**
  
   - add hotkey commands ("Ctrl^a") steps to xdotool extension
   - add autohotkey steps for windows interfaces equivalent to xdotool�s steps 