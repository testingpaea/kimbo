const crypto = require('crypto');
const fs = require('fs');




/*
 * Class to store the information of the execution of the steps and subscenarios...
 * 
 * The execution node is a tree node includes all the childs execution info 
 * 
 */

var arrPersistNodes=[];
var bPersisting=false;
var tsPersistInit=0;
var tsPersistEnd=0;
var tsPersistTotal=0;
var tsPersistAcumulated=0;
var tsPersistCount=0;

async function persistPending(){
	debugger;
	tsPersistCount++;
	tsPersistInit=Date.now();
	var node=arrPersistNodes[0];
	
	node.tsPersist=tsPersistInit;
	node.timePrevPersist=tsPersistTotal;
	node.accumTimePersisting=tsPersistAcumulated;
	node.medTimePersist=tsPersistAcumulated/tsPersistCount;

	node.pendingPersist=arrPersistNodes.length;
	node.totalPersist=tsPersistCount+node.pendingPersist;
	node.actualPersist=tsPersistCount;
/*
	var persistInfo=node.ScStore.persistExecutionNodes;
	var storableManager=node.ScStore.getStorable().manager;
	var oAdded=await storableManager.step_addNewTypedObject(persistInfo.objectType,node);
	await node.ScStore.getPersist().step_persistObject(oAdded);
	await storableManager.step_removeTypedObject(oAdded.type,oAdded.id);
	*/
	
	var sNode="";
	if (tsPersistCount==1){
		sNode="[";
	}else {
		sNode=",";		
	}
	var auxObj={}
	for (let att in node){
		var val=node[att];
		if (typeof val!=="function"){
			auxObj[att]=val;
		}
	}
	delete auxObj.parent;
	delete auxObj.childs;
	delete auxObj.params;
	delete auxObj.ScStore;
	delete auxObj.state;
	
	sNode+=JSON.stringify(auxObj, null, "  ");
	fs.appendFileSync('/usr/src/app/executionNodes.json', sNode);
	
	arrPersistNodes.shift();
	tsPersistEnd=Date.now();
	tsPersistTotal=tsPersistEnd-tsPersistInit;
	tsPersistAcumulated+=tsPersistTotal;
	
}
function persist(node){
	if (typeof node.ScStore.persistExecutionNodes!=="undefined"){
		debugger;
		arrPersistNodes.push(node);
		if (!bPersisting){
			bPersisting=true;
			setTimeout(async function(){
				debugger;
				while (arrPersistNodes.length>0){
					await persistPending();
				}
				bPersisting=false;
			});
		}
	}
}
async function clearPersistance(node){
	if (typeof node.ScStore.persistExecutionNodes!=="undefined"){
		var persistInfo=node.ScStore.persistExecutionNodes;
		var storableManager=node.ScStore.getStorable().manager;
		await storableManager.step_clearStorage(persistInfo.objectType);
	}
}


var ExecutionNode= class ExecutionNode {
	constructor(name,params){
		var self=this;
		if (typeof name==="undefined"){
			self.name="unnamed";
		} else {
			self.name=name;
		}
		self.id=crypto.randomUUID();
		self.idParent="";
		self.deep=0;
		self.isLeaf=true;
		self.nChilds=0;

		self.parent="";
		self.childs=[];
		self.params=[];
		if (typeof params!=="undefined"){
			for (const param of params){
				self.params.push(param);
			};
		}
		self.state="Error";
		self.initTime=Date.now();
		self.endTime=0;
	}
	setState(newState){
		this.state=newState;
	}
	done(){
		debugger;
		var self=this;
		self.endTime=Date.now();
		self.totalTime=self.endTime-self.initTime;
		var sPre=""+(self.totalTime/1000);
		while (sPre.length<10){
			sPre=" "+sPre;
		}
		sPre+="  ";
		for (var i=0;i<self.deep;i++){
			sPre+="  |  ";
		}
		self.nameWithFormat=sPre+self.name;
		self.setState("Passed");
		self.childs=[];
		persist(self);			
		
	}
	async clearPersistance(){
		await clearPersistance(self);
	}
	addChild(execNode){
		var self=this;
		self.isLeaf=false;
		self.nChilds++;
		execNode.deep=self.deep+1;
		execNode.idParent=self.id;
		self.childs.push(execNode);
		execNode.parent=self;
		
	}
	toJSON(actDeep,maxDeep){
		var self=this;
		debugger;
		var oResult={};
		if (typeof self.name==="undefined"){
			oResult.name="unnamed";
		} else if (typeof self.name==="string"){
			oResult.name=self.name;
		} else {
			oResult.name=self.name.id;
		}
		oResult.state=self.state;
		oResult.time=((self.endTime-self.initTime)/1000).toFixed(3);
		oResult.params=[];
		for (var i=0;i<self.params.length;i++){
			oResult.params.push(self.params[i]);
		}
		oResult.actions=[];
		var dAct=0;
		if (typeof actDeep!=="undefined"){
			dAct=actDeep;
		}
		if ((typeof maxDeep!=="undefined") && (dAct<=maxDeep)) {
			for (var i=0;i<self.childs.length;i++){
				oResult.actions.push(self.childs[i].toJSON(dAct+1,maxDeep));
			}
		} else {
			for (var i=0;i<self.childs.length;i++){
				oResult.actions.push(self.childs[i].toJSON());
			}
		}
		return oResult;
	}
	trace(actDeep){
		var self=this;
		var iDeep=actDeep;
		if (typeof iDeep==="undefined"){
			iDeep=0;
		}
		var sPrefix="";
		for (var i=0;i<iDeep;i++){
			sPrefix+="   ";
		}

		var sResult="..."+self.state + " ("+((self.endTime-self.initTime)/1000).toFixed(2)+"s)";
		if (typeof self.name==="undefined"){
			sResult=sPrefix+"unnamed"+sResult;
		} else if (typeof self.name==="string"){
			sResult=sPrefix+self.name+sResult;
		} else {
			sResult=sPrefix+self.name.id+sResult;
		}
		
		console.log(sResult);
		for (var i=0;i<self.childs.length;i++){
			sResult+=("\n"+self.childs[i].trace(iDeep+1));
		}
		return sResult;
	}
}
module.exports=ExecutionNode;