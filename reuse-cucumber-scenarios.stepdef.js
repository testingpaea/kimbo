/*
 * Extended/adapted by kimbo
 * 
 */
const sHelper = require('./stepHelper.lib');
const ScStore = require('./scenarioStore');
const OtherWorld = require('./OtherWorld');
const { stepFunctions } = require('./stepFunctionsStore');
var arity = require('util-arity');
const maxScenarioTimeout = 10000000;

const processScenarioParameters=async function(scenarioName,str_paramsObj){
	  //console.log("Process Scenario Parameters:"+str_paramsObj);
	  //ScStore.getClosures().newClosure(scenarioName.id);
	  if (typeof str_paramsObj==="undefined") return;
	  var paramsObj; 
	  //console.log("ParamsOb:"+typeof str_paramsObj+" .... " + JSON.stringify(str_paramsObj));
	  paramsObj=await ScStore.processExpression(str_paramsObj);
	  if ((typeof paramsObj!="string")&&(typeof paramsObj!="number")&&(!Array.isArray(paramsObj))){
		  //console.log("it isn't a simple string");
		  var arrProperties=Object.getOwnPropertyNames(paramsObj);
		  //console.log("properties names array"+JSON.stringify(arrProperties));
		  for (var i=0;i<arrProperties.length;i++){
			//console.log("prop "+i+":"+arrProperties[i]);
			var vPropName=arrProperties[i];
			if (vPropName!=="constructor"){
				var vPropValue=paramsObj[vPropName];
				//console.log("value "+i+":"+vPropValue);
				debugger;
				var pPropValue=await ScStore.processExpression(vPropValue);
				ScStore.addVar(vPropName,pPropValue);
				//console.log("Var "+vPropName+" added with value:"+pPropValue);
			}
		  }
	  } else {
		  //console.log("it is a simple string, number or array");
		  paramsObj={"1":paramsObj};
	  }
	  //console.log("**Closure Data**");
	  //ScStore.traceClosures();
	  return paramsObj;
}
const runScenarioStep=async function(stepDefinition,scenarioArguments) {
      try{
		debugger;
		var theVariables=sHelper.replaceVariables(stepDefinition.functionArguments, scenarioArguments);
		var fncId=stepDefinition.functionName+" with:"+JSON.stringify(theVariables);
		var stepFunctionName=stepDefinition.functionName;
		var bExecute=true;
		var bNotExecutedByLoop=false;
		if (ScStore.getClosures().getClosure(true).existsVar("$IfResult")) { // inner evaluated if....
			var ifResult=ScStore.getClosures().getClosure(true).getVar("$IfResult");
			if (ifResult.value){ // execute 
				bExecute=true;
			} else {
				bExecute=false;
			}				
			//console.log("If inner..... will Execute:"+bExecute);
			if (!bExecute){
				//console.log("Not execute..... reviewing If-then-else-endif structure");
				if ((stepFunctionName=="else")||(stepFunctionName=="else_if_STRING_then")){
					debugger;
					//console.log("else or else if.... Level:"+ScStore.ifStackLevelRunning+" Stack Level:"+ScStore.ifStackLevel); 
					if (ScStore.ifStackLevel==ScStore.ifStackLevelRunning){
						bExecute=true;
						//console.log("else or else if of actual if.... Execute!"); 
					}
					//console.log("else or else if will execute "+bExecute+".... Level:"+ScStore.ifStackLevelRunning+" Stack Level:"+ScStore.ifStackLevel); 
				}
			}
		} 
		if (ScStore.existsVar("$LoopInfo",true)) { // inner on loop evaluated where....
			var loopInfo=ScStore.getVar("$LoopInfo",true).value; // gets the top Loop Info
 			if (loopInfo!==""){
				debugger;		
				//console.log(loopInfo.ClosuresDeep + " -- " + ScStore.getClosuresDeep());			 
				var condResult=loopInfo.LoopResult.value;
				if (condResult){ // execute 
					bNotExecutedByLoop=false;
				} else {
					bNotExecutedByLoop=true;
				}
				var bInClycles=loopInfo.LoopInCycles.value;
				if (/*(stepFunctionName!="end_loop")&&*/(!bInClycles)){
					var fncObject={
						stepDefinition:stepDefinition,
						scenarioArguments:scenarioArguments
					}
					var arrFunctions=loopInfo.LoopInternalSteps.value;
					arrFunctions.push(fncObject);
				}
			}				
		}
		
		var stepFunctionAttribs=stepFunctions[stepDefinition.functionName];
		if (stepFunctionAttribs.isIfStart) {
			if (!bExecute) {
				ScStore.ifStackLevel++;
			}	
		} else if (stepFunctionAttribs.isIfEnd){
			if (ScStore.ifStackLevel==ScStore.ifStackLevelRunning){
				bExecute=true;
			} else {
				ScStore.ifStackLevel--;
			}
		} 
		if (stepFunctionAttribs.isLoopStart){
			if ((bNotExecutedByLoop)||(!bExecute)){
				ScStore.loopStackLevel++;
			} 
		} else if (stepFunctionAttribs.isLoopEnd){
			if (ScStore.loopStackLevel==ScStore.loopStackLevelRunning){
				bExecute=true;
				bNotExecutedByLoop=false;
			} else {
				ScStore.loopStackLevel--;
			}
		}
		
		if (bExecute && (!bNotExecutedByLoop)) {
			//console.log("to run Function:"+fncId);
			ScStore.getClosures().newClosure(fncId,true);
			var fncObject={stepDefinition:stepDefinition,scenarioArguments:scenarioArguments};
			ScStore.assignOrCreateVar("$runningStepDefinition",fncObject,true);
			//ScStore.newExecutionNode("[Step] "+fncId);

			var stepNext=stepFunctions[stepDefinition.functionName];
			await stepNext.bind(OtherWorld.worldToBind)(...sHelper.replaceVariables(stepDefinition.functionArguments, scenarioArguments));

			//ScStore.doneExecutionNode();
			ScStore.getClosures().popClosure(true);
		} else {
			//console.log("SKIPPED.... "+fncId);
		}
      }catch(err){
	    console.log("Error:" + err);
		console.log(err);
		throw new Error(err.stack);
	  }
}
const runScenario = function (funcArgs = {},createClosure=true) { //func=(a=>a) : returns first argument
  return async function (at_scenarioTag) {
	//console.log("runScenario Without Parameters ["+at_scenarioTag +"] with id:["+at_scenarioTag.id+"]");
    //console.log('at_scenarioTag',at_scenarioTag);
//    var scenarioTag = at_scenarioTag.replace('@','');
	var scenarioTag =""; 
	if (typeof at_scenarioTag.id!=="undefined"){
		scenarioTag =at_scenarioTag.id;
	} else {
		scenarioTag =ScStore.getScenario(at_scenarioTag);
	}
	if (ScStore.isExtensionLoaded("events")){
		await ScStore.getEvents().started(scenarioTag,ScStore.scenarios[scenarioTag]);
	}

	//console.log("Create Closure (NEW):"+createClosure);
	if (createClosure) {
		ScStore.getClosures().newClosure(scenarioTag,true);
		var theInternalClosure=ScStore.getClosures().getClosure(true);
		theInternalClosure.addVar("$LoopInfo","");
	}
    var functionList = ScStore.scenarios[scenarioTag].functionList;

    if (OtherWorld.showStepdefFiles) { 
		//console.log('Reuse-cucumber-scenarios says: Starting execution of scenario "@' + scenarioTag + '"'); 
	}
    for (var i = 0; i < functionList.length; i++) {
		await runScenarioStep(functionList[i],funcArgs);
	}
	//console.log("Create Closure(POP):"+createClosure);
	if (createClosure) ScStore.getClosures().popClosure(true);
	if (ScStore.isExtensionLoaded("events")){
		await ScStore.getEvents().finished(scenarioTag,ScStore.scenarios[scenarioTag]);
	}
    if (OtherWorld.showStepdefFiles) { 
		//console.log('(Run Scenario) Reuse-cucumber-scenarios says: Scenario "@' + scenarioTag + '" was succefully executed.'); 
	}
    return;
  };
}
const runScenarioWithParameters = function (func = (a => a), funcArgs = {}) {
  return async function (at_scenarioTag, str_paramsObj){
	var scenarioTag =""; 
	if (typeof at_scenarioTag.id!=="undefined"){
		scenarioTag =at_scenarioTag.id;
	} else {
    	scenarioTag = at_scenarioTag.replace('@','');
		scenarioTag =ScStore.getScenario(scenarioTag);
	}	  
	//console.log("runScenarioWithParameters "+scenarioTag +" "+str_paramsObj);
	var paramsObj=await processScenarioParameters(scenarioTag,str_paramsObj);
    const { stepFunctions } = require('./stepFunctionsStore');
    var functionList = ScStore.scenarios[scenarioTag].functionList;
	if (ScStore.isExtensionLoaded("events")){
		await ScStore.getEvents().started(scenarioTag,ScStore.scenarios[scenarioTag]);
	}
    //console.log('functionList',functionList);
    if (OtherWorld.showStepdefFiles) { 
		//console.log('Reuse-cucumber-scenarios says: Starting execution of scenario "@' + scenarioTag + '"'); 
	}

    for (var i = 0; i < functionList.length; i++) {
      let defaultArgs = functionList[i].functionArguments;
      let len = Object.keys(defaultArgs).length;
      //console.log('paramsObj[i+1]',paramsObj[i+1]);
      let thisParams = (paramsObj[i + 1] || []);
      let args = thisParams.concat(defaultArgs[len - 1]);
      //console.log("args step"+(i+1).toString(),args);
      //console.log("functionArguments step"+(i+1).toString(), defaultArgs)
      let thisFunc = stepFunctions[functionList[i].functionName];
      if (thisParams.length >= (thisFunc.length - 1)) { 
        try{
          await thisFunc.bind(OtherWorld.worldToBind)(...func(args, funcArgs)); 
        } catch(err){
			console.log("Error:" + err);
			console.log(err);
			throw new Error(err.stack);
		}
      }
      else {
        throw new Error(`Reuse-cucumber-scenarios says: The step pattern "${thisFunc.stepPattern}" got a wrong number 
        of parameters: ${thisParams.length} parameters.`);
      }
    }
	if (ScStore.isExtensionLoaded("events")){
		await ScStore.getEvents().finished(scenarioTag,ScStore.scenarios[scenarioTag]);
	}
    if (OtherWorld.showStepdefFiles) { 
	//	console.log('(Run with params) Reuse-cucumber-scenarios says: Scenario "@' + scenarioTag + '" was succefully executed.'); 
	}
    return;
  };
};
const runScenarioUsingVariable = async function (at_scenarioTag, varName, varValue){  
  //console.log("runScenarioUsingVariable("+at_scenarioTag+"). Variable:"+varName+" "+varValue);
  await runScenario( { [varName]: sHelper.replaceVariable(varValue) })(at_scenarioTag);
  return;
}
const runScenarioUsingVariables = async function (at_scenarioTag, str_varObj){
  //console.log("runScenarioUsingVariables"+JSON.stringify(at_scenarioTag)+". Variables:"+str_varObj);
  //console.log("Type of Var:"+ typeof str_varObj);
  //console.log("START SCENARIO");
  if (typeof at_scenarioTag.id!=="undefined"){
  	ScStore.getClosures().newClosure(at_scenarioTag.id);
  } else {
	ScStore.getClosures().newClosure(at_scenarioTag);  
  }
  var varObj=await processScenarioParameters(at_scenarioTag,str_varObj);
  try {
	await runScenario( varObj,true)(at_scenarioTag); 
	//console.log("END SCENARIO SUCESSFULLY");
	ScStore.getClosures().popClosure();
  } catch (err){
	console.log("END SCENARIO ("+JSON.stringify(at_scenarioTag)+")WITH ERROR:" + err);
	console.log(err);
	await ScStore.getEvents().finishedAll();
  }
  return;
}
const runScenarioUsingVariableFromTo = async function (at_scenarioTag, varName, startValue, endValue){
  var startVal = sHelper.replaceVariable(startValue);
  var endVal = sHelper.replaceVariable(endValue);
  for(var thisValue=parseInt(startVal);thisValue<=parseInt(endVal);thisValue++){
    //console.log("thisValue",thisValue);
    await runScenario({ [varName]: thisValue })(at_scenarioTag);
  }
  return;
}


const executeIf=async function(theExpression){
	debugger;
	//console.log("Executing the If part");
	var evalResult=await ScStore.processExpression(theExpression);
	
	//console.log("IF condition:"+evalResult);
	var localClosures=ScStore.getClosures().popClosure(false);
	ScStore.getClosures().newClosure("If ("+evalResult +"["+theExpression+"])",true);
	ScStore.addVar("$IfResult",evalResult,true);
	ScStore.addVar("$IfResult_branch_executed",evalResult,true);
	ScStore.getClosures().pushClosures(localClosures);
	ScStore.ifStackLevel++;
	ScStore.ifStackLevelRunning=ScStore.ifStackLevel;
	//ScStore.traceClosures();
	//console.log("Will Execute the IF branch:"+ScStore.getClosures().getUpperClosure(true).getVar("$IfResult").value);
}
const executeElseIf=async function(theExpression){
	debugger;
	//console.log("Executing the Else If part");
    //ScStore.traceClosures();
	var ifVar=ScStore.getClosures().getUpperClosure(true).getVar("$IfResult");
	var ifBranchExecuted=ScStore.getClosures().getUpperClosure(true).getVar("$IfResult_branch_executed");
	if (ifBranchExecuted.value){
		ifVar.value=false;
	} else {
		var evalResult=await ScStore.processExpression(theExpression);
		ifVar.value=evalResult;
		ifBranchExecuted.value=evalResult;
	}
	//console.log("Execute the Else IF branch:"+ScStore.getClosures().getUpperClosure(true).getVar("$IfResult").value);

}
const executeElse=function(){
	debugger;
	//console.log("Executing the ELSE part");
    //ScStore.traceClosures();	
	var ifVar=ScStore.getClosures().getUpperClosure(true).getVar("$IfResult");
	var ifBranchExecuted=ScStore.getClosures().getUpperClosure(true).getVar("$IfResult_branch_executed");
	if (ifBranchExecuted.value){
		ifVar.value=false;
	} else {
		ifVar.value=true;
		ifBranchExecuted.value=true;
	}	
	//console.log("Execute the ELSE branch:"+ScStore.getClosures().getUpperClosure(true).getVar("$IfResult").value);
}
const executeEndIf=function(){
	debugger;
	//console.log("Executing the ENDIF part .... destroying closures");
    //ScStore.traceClosures();	 
	ScStore.getClosures().popClosure(true);
	//console.log("After destroy closures");
	ScStore.ifStackLevel--;
	ScStore.ifStackLevelRunning=ScStore.ifStackLevel;
	//ScStore.traceClosures();
}

const executeLoop=async function(initLoopAction,theExpression,preCycleAction,postCycleAction,endLoopAction){
	debugger;
	//console.log("Executing the Loop Start step");
	//console.log("while condition:"+theExpression);
	var localClosures=ScStore.getClosures().popClosure(false);
	ScStore.getClosures().newClosure("Loop ("+theExpression+")",true);

	ScStore.loopStackLevel++;
	ScStore.loopStackLevelRunning=ScStore.loopStackLevel;

	
	ScStore.addVar("$LoopExpression",theExpression,true);
	ScStore.addVar("$LoopInternalSteps",[],true);
	ScStore.addVar("$LoopInCycles",false,true);
	ScStore.addVar("$LoopCycleIndex",0);
	ScStore.addVar("$LoopPreCycleAction",preCycleAction,true);
	ScStore.addVar("$LoopPostCycleAction",postCycleAction,true);
	ScStore.addVar("$LoopEndLoopAction",endLoopAction,true);
	ScStore.addVar("$LoopCycleElement","");
	if (typeof initLoopAction!=="undefined"){
		await initLoopAction();
	}
	var evalResult=await ScStore.processExpression(theExpression);
	ScStore.addVar("$LoopResult",evalResult,true);
	var auxLoopInfo={
				LoopExpression:ScStore.getVar("$LoopExpression",true),
				LoopInternalSteps:ScStore.getVar("$LoopInternalSteps",true),
				LoopInCycles:ScStore.getVar("$LoopInCycles",true),
				LoopCycleIndex:ScStore.getVar("$LoopCycleIndex"),
				LoopPreCycleAction:ScStore.getVar("$LoopPreCycleAction",true),
				LoopPostCycleAction:ScStore.getVar("$LoopPostCycleAction",true),
				LoopCycleElement:ScStore.getVar("$LoopCycleElement"),
				LoopResult:ScStore.getVar("$LoopResult",true),
				ClosureDeep:ScStore.getClosuresDeep()
			}			
	ScStore.addVar("$LoopInfo",auxLoopInfo,true); // the start of loop implies a new variable loopinfo			
	
	ScStore.getClosures().pushClosures(localClosures);
	if (evalResult){
		if (typeof preCycleAction!=="undefined") {
			var cycleElement=ScStore.getVar("$LoopCycleElement").value;
			preCycleAction(0,cycleElement);
		}
	}
	//ScStore.traceClosures();
	//console.log("Will Execute the While sentences:"+ScStore.getClosures().getUpperClosure(true).getVar("$LoopResult").value);
}



const executeEndLoop=async function(){
	debugger;
	//console.log("Executing the END WHILE part .... destroying closures and retry");
    //ScStore.traceClosures();
	// evaluate while condition
	
	var theExpression=ScStore.getVar("$LoopExpression",true).value;
	var theFunctionList=ScStore.getVar("$LoopInternalSteps",true).value;
	var postCycleAction=ScStore.getVar("$LoopPostCycleAction",true).value;
	var preCycleAction=ScStore.getVar("$LoopPreCycleAction",true).value;
	var loopEnd=ScStore.getVar("$LoopEndLoopAction",true).value;

	var cycleIndex=ScStore.getVar("$LoopCycleIndex").value;
	var cycleElement=ScStore.getVar("$LoopCycleElement").value;
	
	var evalResult=ScStore.getVar("$LoopResult",true).value;
	
	// if is inner loop it have to add all steps to the parent loop and add a endloop step
	var closureManager=ScStore.getClosures();
	var deepVars=closureManager.getDeepListOfVar("$LoopInfo",true);
	if (deepVars.length>1){ // 1 is this loop
		var parentInfo=deepVars[deepVars.length-2].value;
		if (parentInfo===""){
			// do nothing... the parent is the start of subscenario
		} else if (!parentInfo.LoopInCycles.value) { //if parent is in cycles the steps were added
			for (var i=0;i<theFunctionList.length;i++){
				parentInfo.LoopInternalSteps.value.push(theFunctionList[i]);
			}
/*			var stepEndLoop=ScStore.getVar("$runningStepDefinition",true);
			parentInfo.LoopInternalSteps.value.push(stepEndLoop.value);*/
		}
	}
	var stepEndLoop=theFunctionList.pop(); //once is cloned to parent .. the end loop step is not necesary (the while implies it)
	
			
	if (typeof postCycleAction!=="undefined"){
		postCycleAction(cycleIndex,cycleElement);
	}

	ScStore.setVar("$LoopInCycles",true,true);
	
	var endLoopClosure=ScStore.getClosures().popClosure(true);
	// do the rest of cycles
	while (evalResult) {
		debugger;
		//console.log("Next Cycle");
		cycleIndex++;
		ScStore.setVar("$LoopCycleIndex",cycleIndex);
		evalResult=await ScStore.processExpression(theExpression);
		if (evalResult){
			ScStore.getClosures().newClosure("While Next Cycle "+cycleIndex+" ("+evalResult +"["+theExpression+"])",true);

			if (typeof preCycleAction!=="undefined") {
				cycleElement=ScStore.getVar("$LoopCycleElement").value;
				preCycleAction(cycleIndex,cycleElement);
			}
			
			// run next cycle
			for (var i=0;i<theFunctionList.length;i++){
				var theFunction=theFunctionList[i];
				await runScenarioStep(theFunction.stepDefinition,theFunction.scenarioArguments)
			}
			ScStore.getClosures().popClosure(true);
			
			if (typeof postCycleAction!=="undefined"){
				postCycleAction(cycleIndex,cycleElement);
			}
		}
	}
	if (typeof loopEnd!=="undefined"){
		loopEnd(cycleIndex,cycleElement);
	}
	
	//the top closure is "loop"... when finish the step will pop closure automatically
	
	//ScStore.getClosures().popClosure(true); // pop the end loop closure to remove the aditional closure of loop start
	//ScStore.getClosures().pushClosures(endLoopClosure); // put the end loop closure at the end
	ScStore.loopStackLevel--;
	ScStore.loopStackLevelRunning=ScStore.loopStackLevel;
	
	//console.log("After all cycles");
	//ScStore.traceClosures();
}

const executeLoopFor=async function(sVarName,vInitValue,vEndValue){
	var varName=await ScStore.processExpression(sVarName);
	var iInitValue=vInitValue;
	if (typeof vInitValue==="string"){
		iInitValue=parseInt(await ScStore.processExpression(vInitValue));
	}
	var iEndValue=vEndValue;
	if (typeof vEndValue==="string"){
		iEndValue=parseInt(await ScStore.processExpression(vEndValue));
	}
	var nElements=iEndValue-iInitValue;
	var fncInitLoop=function(){
		debugger;
		//console.log("Start of loop");
		ScStore.setVar("$LoopCycleElement","");
		ScStore.assignOrCreateVar(varName,iInitValue);
	}
	var fncEndLoop=function(lastIndex){
		debugger;
		//console.log("End of loop arrived... last index:"+lastIndex);
	}
	var fncCycleStart=function(index){
		debugger;
		//console.log("Start of Cycle:"+index);
	}
	var fncCycleEnd=function(index){
		debugger;
		//console.log("End of Cycle:"+index+"+1");
		var theElement="";
		var newIndex=(index+1)+iInitValue;
		ScStore.setVar(varName,newIndex);
	}
	await executeLoop(fncInitLoop,"(#( $"+varName+"<= "+iEndValue+" )#)",fncCycleStart,fncCycleEnd,fncEndLoop);
}

const executeLoopForEach=async function(sVarName,elementsList){
	await executeLoopForEachExtended(sVarName,elementsList);
}

const executeLoopForEachExtended=async function(sVarName,elementsList,fncInit,fncEnd,fncInitCycle,fncEndCycle){
	var varName=await ScStore.processExpression(sVarName);
	var arrElements=await ScStore.processExpression(elementsList);
	var nElements=arrElements.length;
	var fncInitLoop=function(){
		debugger;
		//console.log("Start of loop");
		if (arrElements.length>0){
			var theElement=arrElements[0];
			ScStore.setVar("$LoopCycleElement",theElement);
			ScStore.addVar(varName,theElement);			
		}
		if (typeof fncInit!=="undefined"){
			fncInit();
		}
	}
	var fncEndLoop=function(lastIndex,lastElement){
		debugger;
		if (typeof fncEnd!=="undefined"){
			fncEnd(lastIndex,lastElement);
		}
		//console.log("End of loop arrived... last index:"+lastIndex);
	}
	var fncCycleStart=function(index,actElement){
		debugger;
		//console.log("Start of Cycle:"+index);
		ScStore.setVar(varName,actElement);
		if (typeof fncInitCycle!=="undefined"){
			fncInitCycle(index,actElement);
		}
	}
	var fncCycleEnd=function(index,actElement){
		debugger;
		var newIndex=(index+1);
		var bWillRun=(arrElements.length>newIndex);
		if (bWillRun){
			var theElement=arrElements[newIndex];
			ScStore.setVar("$LoopCycleElement",theElement);
			ScStore.setVar(varName,theElement);
		}
		if (typeof fncEndCycle!=="undefined"){
			fncEndCycle(index,actElement);
		}
		//console.log("End of Cycle:"+index+" prepared Cycle "+newIndex + " will run?" +bWillRun);
	}
	await executeLoop(fncInitLoop,"(#( $LoopCycleIndex< "+nElements+" )#)",fncCycleStart,fncCycleEnd,fncEndLoop);
}

ScStore["BranchingSteps"]={};
ScStore.BranchingSteps.executeIf=executeIf;
ScStore.BranchingSteps.executeElseIf=executeElseIf;
ScStore.BranchingSteps.executeElse=executeElse;
ScStore.BranchingSteps.executeEndIf=executeEndIf;
ScStore.BranchingSteps.executeLoop=executeLoop;
ScStore.BranchingSteps.executeEndLoop=executeEndLoop;
ScStore.BranchingSteps.executeLoopFor=executeLoopFor;
ScStore.BranchingSteps.executeLoopForEach=executeLoopForEach;
ScStore.BranchingSteps.maxScenarioTimeout=maxScenarioTimeout; 


const throwError=async function(sErrorDescription){
	var sError=await ScStore.processExpression(sErrorDescription);
	console.log(sError);
	throw sError;
}

const showExecutionTree=function(thisTestStatus){
	ScStore.changeExecutionState(thisTestStatus);
	ScStore.traceExecutionTree();
}
const withDefaultTimeout = function (rawFunction) {
  //console.log("WithDefaultTimeout:"+rawFunction);
  var fn = async function () {
	//console.log("WithDefaultTimeout:"+JSON.stringify(arguments));
	debugger;
    var scenarioTag = ScStore.getScenario(arguments[0]);
    var thisScenario = ScStore.scenarios[scenarioTag];
    if (thisScenario) {
      var timeout = ScStore.scenarios[scenarioTag].functionTimeout;
      try {
		var sArgs=JSON.stringify(["With Default Timeout",arguments]);
		sArgs=await ScStore.processExpression(sArgs);
		sArgs=JSON.stringify(sArgs);
        await sHelper.promiseTimeout(timeout, rawFunction(...arguments),sArgs);
      } catch (err) { 
	    console.log("Error:" + err);
		console.log(err);	  
		throw new Error(err); 
	  }
      return;
    } else {  
		var actApp=ScStore.appManager.get();
		var arrSelector=[];
		if (typeof ScStore.scenarioSelectorHook!=="undefined"){
			arrSelector=ScStore.scenarioSelectorHook();
		}
		var sSelector="";
		for (const selector of arrSelector){
			sSelector+=(sSelector!==""?" ":"")+selector.name+":"+selector.value;
		}		
		throw new Error(`Reuse-cucumber-scenarios says: The scenario "${arguments[0]}" compatible with tags "${sSelector}" has to be defined and executed before calling it.`); 
	}
  };

  return arity(rawFunction.length,fn);
}
const withCustomTimeout = function (rawFunction) {
  //console.log("WithCustomTimeout:"+rawFunction);
  var fn = async function () {
	debugger;
	//console.log("WithCustomTimeout: ");
    //console.log('all arguments:',JSON.stringify(arguments));
    //var scenarioTag = arguments[0].replace('@','');
    var scenarioName=await ScStore.processExpression(arguments[0]);
    arguments[0]=scenarioName;
    var scenarioTag = ScStore.getScenario(arguments[0]);
	//console.log("----- " + scenarioTag + " ------");
    //console.log('arguments1',arguments);
    var str_paramsObj =  arguments[arguments.length-2];

    var thisScenario = ScStore.scenarios[scenarioTag];
    if (thisScenario) {
	  var paramsObj; 
	  //console.log("ParamsOb:"+typeof str_paramsObj+" .... " + JSON.stringify(str_paramsObj));
	  debugger;
	  paramsObj=await ScStore.processExpression(str_paramsObj);
	  if ((typeof paramsObj!="string")&&(typeof paramsObj!="number")&&(!Array.isArray(paramsObj))){
		  //console.log("it isn't a simple string");
	  } else {
		  //console.log("it is a simple string, number or array");
		  paramsObj={"1":paramsObj};
	  }
	  //console.log("**Closure Data**");
	  //ScStore.traceClosures();
	  /*console.log('paramsObj',JSON.stringify(paramsObj,undefined,3));
	  console.log('timeout pObj:'+paramsObj['timeout']);
	  console.log('timeout pTag:'+ScStore.scenarios[scenarioTag].functionTimeout);
	  console.log('timeout pMax:'+maxScenarioTimeout);
	  */
      var timeout = paramsObj['timeout'] 
						|| ScStore.scenarios[scenarioTag].functionTimeout 
						|| maxScenarioTimeout;
      try {
		  if ((typeof timeout!=="undefined")&&(!isNaN(timeout))) {
			var sArgs=JSON.stringify(["With Custom Timeout",arguments]);
			sArgs=await ScStore.processExpression(sArgs);
			sArgs=JSON.stringify(sArgs);
			await sHelper.promiseTimeout(timeout, rawFunction(...arguments),sArgs);
		  } else {
			console.log("the timeout is undefined or NaN:["+timeout+"]");
			await rawFunction(...arguments);
		  }
      } catch (err) { 
	    console.log("Error:" + err);
		console.log(err);
		throw new Error(err); 
	  }
      return;
    } else {  
		var actApp=ScStore.appManager.get();
		var arrSelector=[];
		if (typeof ScStore.scenarioSelectorHook!=="undefined"){
			arrSelector=ScStore.scenarioSelectorHook();
		}
		var sSelector="";
		for (const selector of arrSelector){
			sSelector+=(sSelector!==""?" ":"")+selector.name+":"+selector.value;
		}
		throw new Error(`Reuse-cucumber-scenarios says: The scenario "${arguments[0]}" compatible with tags "${sSelector}" has to be defined and executed before calling it.`); 
	}
  };
  return arity(rawFunction.length,fn);
}

ScStore.indirectScenarioCall=async function(scenarioName,params){
	debugger;
	//console.log("indirect Calling:"+scenarioName+" params:"+JSON.stringify(params));
	//ScStore.getClosures().newClosure(scenarioName,true);
	await runScenarioUsingVariables(scenarioName,params); 
	//console.log("indirect Calling End");
	//ScStore.getClosures().popClosure(true);

}

const stepDefinitions=[
  { stepMethod: 'Given', stepPattern: 'Here We Go!', stepTimeout: 40000,
    stepFunction: async ()=>{
		var bProcess=true;
		if (ScStore.isExtensionLoaded("selected")){
			if (ScStore.getSelected().dynamic){
				bProcess=false;
			}
		}
		if (bProcess){
			//console.log("Here We Go!... running the steps");
		}
		return;
    }
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string}', stepTimeout: maxScenarioTimeout, isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenario())
  },{ stepMethod: 'When', stepPattern: 'the scenario {string} happens', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenario())
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} with parameters {string}', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioWithParameters())
  },{ stepMethod: 'When', stepPattern: 'the scenario {string} happens with parameters {string}', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioWithParameters())
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} with parameters', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioWithParameters())
  },{ stepMethod: 'When', stepPattern: 'the scenario {string} happens with parameters', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioWithParameters())
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} where variable {string} is {string}', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: async function(scenarioName,varName,varValue){
		debugger;
		var jsonObject={};
		var vName=await ScStore.processExpression(varName);
		if (vName.charAt(0)!="$")vName="$"+vName;
		var vValue=await ScStore.processExpression(varValue);
		jsonObject[vName]=vValue;
		var	fncCall=withCustomTimeout(runScenarioUsingVariables);
		await fncCall(scenarioName,jsonObject);
	}	
	
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} where variables', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioUsingVariables)
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} for variable {string} from {string} to {string}', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withDefaultTimeout(runScenarioUsingVariableFromTo)
  },{ stepMethod: 'Given', stepPattern: 'the scenario {string} where variable(s) {string}', stepTimeout: maxScenarioTimeout,isScenarioCall:true,
    stepFunction: withCustomTimeout(runScenarioUsingVariables)
    },{ stepMethod: 'Given', stepPattern: 'show the execution tree whith this test {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: showExecutionTree
  },{ stepMethod: 'Given', stepPattern: 'show trace {string}', stepTimeout: 40000,
    stepFunction: async (sText)=>{
		debugger;
		var txtAux=sText;
		try {
			txtAux=await ScStore.processExpression(sText);
		} catch (err){
			var oAnalisys=await ScStore.getExpressions().processExpressionVariables(sText);
			txtAux=ScStore.getExpressions().replaceRefs(oAnalisys.rtnValue,oAnalisys.arrVarsValues);
		}
		console.log(txtAux);
		return;
    }
  },{ stepMethod: 'Given', stepPattern: 'show trace {string} indented', stepTimeout: 40000,
    stepFunction: async (sText)=>{
		debugger;
		var txtAux=sText;
		var nSpaces=0;
		try {
			txtAux=await ScStore.processExpression(sText);
		} catch (err){
			var oAnalisys=await ScStore.getExpressions().processExpressionVariables(sText);
			txtAux=ScStore.getExpressions().replaceRefs(oAnalisys.rtnValue,oAnalisys.arrVarsValues);
		}
		nSpaces=ScStore.getClosures().closures.length;
		nSpaces=nSpaces*3;
		console.log(" ".repeat(nSpaces)+txtAux);
		return;
    }
  },{ stepMethod: 'Given', stepPattern: 'show closures', stepTimeout: 40000,
    stepFunction: function(){
		ScStore.traceClosures();
    }
  },{ stepMethod: 'Given', stepPattern: 'debugger', stepTimeout: 40000,
    stepFunction: function(){
		debugger;
    }
  },{ stepMethod: 'Given', stepPattern: 'execute in command line {string}', stepTimeout: 40000,
    stepFunction: async function(sCommandToExecute){
		var theCommand=await ScStore.processExpression(sCommandToExecute);
		return await ScStore.executeInCommandLine(theCommand);
    }
  },{ stepMethod: 'Given', stepPattern: 'wait {int} seconds', stepTimeout: 300000,
    stepFunction: async (nSeconds)=>{
		//console.log("Waiting {int} "+nSeconds+" secs");
		var nSecs=nSeconds;
		if (typeof nSeconds==="string"){
			nSecs=await ScStore.processExpression(nSeconds);
		}
		//console.log("Waiting "+nSecs+" secs");
		await ScStore.waitSecs(nSecs);
		//console.log("End of Waiting "+nSecs+" secs");
		return;
    }
  },{ stepMethod: 'Given', stepPattern: 'wait {string} seconds', stepTimeout: 300000,
    stepFunction: async (sSeconds)=>{
		//console.log("Waiting {string} "+sSeconds+" secs");
		var nSecs=await ScStore.processExpression(sSeconds);
		//console.log("Waiting "+nSecs+" secs");
		await ScStore.waitSecs(nSecs);
		//console.log("End of Waiting "+nSecs+" secs");
		return;
    }
  },{ stepMethod: 'Given', stepPattern: 'if {string} then', stepTimeout: maxScenarioTimeout,isIfStart:true,
    stepFunction: executeIf
  },{ stepMethod: 'Given', stepPattern: 'else', stepTimeout: maxScenarioTimeout,
    stepFunction: executeElse
  },{ stepMethod: 'Given', stepPattern: 'else if {string} then', stepTimeout: maxScenarioTimeout,
    stepFunction: executeElseIf
  },{ stepMethod: 'Given', stepPattern: 'end if', stepTimeout: maxScenarioTimeout,isIfEnd:true,
    stepFunction: executeEndIf	
  },{ stepMethod: 'Given', stepPattern: 'loop intialize with {string} while condition {string}, do {string} before cycle, {string} after cycle and {string} at the end', stepTimeout: maxScenarioTimeout,isLoopStart:true,
    stepFunction: executeLoop
  },{ stepMethod: 'Given', stepPattern: 'loop while {string} do', stepTimeout: maxScenarioTimeout,isLoopStart:true,
    stepFunction: async function(theExpression){
		await executeLoop(undefined,theExpression);
	}
  },{ stepMethod: 'Given', stepPattern: 'loop for each element {string} in {string} do', stepTimeout: maxScenarioTimeout, isLoopStart:true,
    stepFunction: executeLoopForEach
  },{ stepMethod: 'Given', stepPattern: 'loop for var {string} from {int} to {int} do', stepTimeout: maxScenarioTimeout,isLoopStart:true,
    stepFunction: executeLoopFor
  },{ stepMethod: 'Given', stepPattern: 'loop for var {string} from {string} to {string} do', stepTimeout: maxScenarioTimeout,isLoopStart:true,
    stepFunction: executeLoopFor
  },{ stepMethod: 'Given', stepPattern: 'end loop', stepTimeout: maxScenarioTimeout, isLoopEnd:true,
    stepFunction: executeEndLoop	
  },{ stepMethod: 'Given', stepPattern: 'throw error {string}', stepTimeout: maxScenarioTimeout,
    stepFunction: throwError
  }
];
	

module.exports = stepDefinitions;
