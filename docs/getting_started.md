#GETTING STARTED#

The Kimbo�s _enableScenarioCalling_ function is an extension of the Virgomax�s original function. The array indicates the list of extension enabled for use in scenarios. The object {name:"",path:""} references a _custom extension_ detailed in other help file.

	const enableScenarioCalling = require('kimbo');
	enableScenarioCalling(true,["applications"
                           	   ,"selenium"
						   ,"xdotool"
						   ,"interfaces"
						   ,"selected"
						   ,"multiscreen"
						   ,"events"
						   ,"providers/database/oracledb"
						   ,"smartcard"
						   ,"exclusions"
						   ,{name:"zzzEXT",path:"../../features/extensions/zzzEXT"}
					        ]);


##SIMPLE SINGLE THREAD##

The simplest sample runs only _npm start_.

The app is based on the file _run.steps.js_ it includes the _enableScenarioCalling_ import and call.

Inner the _run.steps.js_ the developer can include all needed elements/control (a http server if wants a user interface) and the event handler register actions to manage the behavior of Kimbo.

An example of manage the inputBox kimbo event that is raised simultaneously when a step "inputbox" shows a zenity input window.

	ScStore.getEvents().addEvent("inputBox",async function(oParams){
		/*
		this function, for example, to avoid the user interaction by getting the nested value from 		database, could do: 
		  1 call to the database with the params 
		  2 get the value asociated
		  3 assign the value to a variable
		  4 close the active input window
		*/
	});


##A CONTROLLER/HOST APPLICATION WITH MULTIPLE KIMBO THREADS##

A traditional testing/RPA system uses to phantomjs, chrome headless or another screen-less interaction system with browser. The Kimbo�s approach merges web browser and system or desktop app interaction to handle cryptographic actions, xdg-open'ed applications or directly desktop applications.

To do this hybrid interaction, kimbo supports to be launched linked to a one vnc server (:1,:2,:3...) isolating the user interfaces to a this vnc server desktop and launchs a full web browser maximized or desktop applications. This behavior and the event managing model one application can launch and control a lot of Kimbo instances running different tests.

The unique limit of this approach is the physical resources of the host machine specially the amount of Memory available when tests a heavy web pages or applications.

The host application may have a class/object "agent" that manages the interactions with each kimbo instances.

	console.log("====>>>> LAUNCHING CUCUMBER <<<<=====");
	const child = fork('node_modules/cucumber/bin/cucumber-js',args);
	agentController.setProcess(child);

_agentController is a example name of an object that controls the forked cucumber process that runs kimbo._

At launch time of each kimbo instance,in the _run.steps.js_, is the register actions of the functions to manage the different events that Kimbo can raise. So, in this model, _run.steps.js_ acts as a configuration of a _"gateway"_ to the controller application.

An gateway model of registering events:
 

*IN run.steps.js REGISTERS THE EVENT HANDLE FUNCTION*

	
	ScStore.getEvents().addEvent("inputBox",async function(oParams){
		/*
		this function redirects the event to the app that forked the cucumber.js execution
		*/
		process.send({msgType:"inputBox",params:oParams});
	});



*IN the app agentController MANAGES THE MESSAGES SENDED BY CUCUMBER INSTANCE*

	
		setProcess(process){
			var self=this;
			self.process=process;
			//REDIRECT ALL MESSAGES TO THE "processMessage" method
			process.on('message',received=>{
				//console.log("message received:"+JSON.stringify(received));
				self.processMessage(received);
				}
			);
			//CONTROL THE CUCUMBER/KIMBO DEAD
			process.on('exit', code => {
				console.log(`Exit code is: ${code}`);
				self.busy=false;
				self.status="UNLOADED";
				self.procesing="";
			});
		}
		async processMessage(received){
			var self=this;
			switch (received.action) {
				case "INPUTBOX":
					/* actions.... */
					break;
				case ....
			}
		} 

This model is called "agent node controller". The app controls a "node" of multiple agents.  