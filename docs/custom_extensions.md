#Custom extensions

The developers can include custom extensions in the array of the _enableScenarioCalling_
	
	enableScenarioCalling(true,[
			{
			name:"zzzEXT"
			,path:"../../features/extensions/zzzEXT"
			}
		]);_
	
Indicating the custom extension as an object _{name:"zzzEXT",path:"yyy"}_ stored in the path as two files "**name**\_extension.lib.js" and "**name**\_extension.steps.js"

the "**name**\_extension.steps.js" includes the array of step definitions 

	const stepDefinitions = [
	    		{   
	    			stepMethod: 'Given', 
	    			stepTimeout: 40000,
				stepPattern: 'sample custom step with simple param {string} and a multiline',
				stepFunction: async function(simpleParam,multiLineParam){
					return;
				}
			}
		];
	module.exports=stepDefinitions;

The step function may be normal or async. If the extension not adds custom steps the array must be empty but the file "**name**\_extension.steps.js" have to exists.

The _stepTimeout_ is optional.

The file "**name**\_extension.lib.js" generates the manager of the extension.

	var zzzEXT=class zzzEXT {
		constructor(){
		}
	}
	var zzzEXTManager=new zzzEXT();
	module.exports=zzzEXTManager;

This manager can be an a extension of a _StorableObjectListClass_ that has a lot of basic normal functions.

The custom extension manager is accesible by the kimbo/scenarioStore using the function get**Name**() with the name capitalized (in the sample: ScStore.getZzzEXT())

The ScStore object can check for an extension is loaded using the method _isExtensionLoaded(extensionName)_

