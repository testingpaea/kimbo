#SCENARIOS AND SUBSCENARIOS

Kimbo extends the _https://github.com/Virgomax/reuse-cucumber-scenarios_ functionality for use the variables, closures and expressions engine.

Some steps of virgomax library have been deleted because Kimbo uses named variables to pass parameters to one scenario to another.

To reuse all scenarios without the obligation of order files by filename, Kimbo introduces a _dynamic_ execution of scenarios. First pass is the _"compilation"_ phase when Kimbo loads all scenarios and only executes the scenarios tagged with @interface, @DataObject and another tags the developer added to a compiling exclusion array.  

The scenarios designed to be reused by another scenarios are identified by the tag @subScenario

The _Top Level Scenarios_ hasn�t the @subScenario, @interface, @DataObject tags.

When the compilation phase ends the app can init the execution of one top level scenario with or without execution parameters

**Example that defines a new subscenario**
	
	Feature: Some reusable subscenario
	@subScenario @APP:ZZZAPP @ENV:PRE
	Scenario: a reusable subscenario
		Given if not exists variable "inputVariable" initialize it with the clone of object "aTypeOfObject" which id is "reusable example default variable"
		Given debugger

The tag @subScenario says that is a reusable scenario. The first step indicates than the subscenario needs a variable named "inputVariable" and if the caller not passed it the subscenario will clone a stored object "aTypeOfObject" with id "reusable example default variable"

In the example, the subscenario has APP tag setted to "ZZZAPP" and ENVIRONMENT to "PRE" (PREPRODUCTION).

**Example that defines a new top level scenario**

	Feature: One top level scenario example
	@APP:ZZZAPP @ISSUE:ZZZ-112
	Scenario: Top level scenario example
		Given in app "ZZZAPP" 
		Given in environment "PRE"
		Given the scenario "a reusable subscenario" where variable "inputVariable" is "some test value passed to subscenario"
		
The top level scenario example has diferent tags values (@APP for theoretical selection actions and @ISSUE as custom tag example). It�s relevant that not has @subScenario tag.

The first steps sets environment variables APP and ENV that are equals to the environment setted in subScenario example.

The last steps calls the subScenario named "a reusable subscenario" and passed a string with some text to the variable "inputVariable".

There is another way to pass variables to a Scenario ussing special tags "PARAM" wich is detailed in his own section.

	the scenario {STRING}

Execute the subScenario named without passing variables

	the scenario {STRING} where variable {STRING} is {STRING}

Execute the subScenario passing one value for a named variable 
	
	the scenario {STRING} where variables
		"""
		json
		"""

Execute the subScenario passing one or more values for a named variables in a json object where each attribute is a passed variable name with value.

	scenario default variable {STRING} If not exits set the value {STRING}

In a subScenario body check if the variable name exists or was passed and, if not exists, initialize one with the specified value.

	scenario default variables 
		"""
		json
		"""

In a subScenario body, for each attribute of json object check if exists or was passed and initialize one if necessary.

	if not exists variable {STRING} initialize it with the clone of object {STRING} which id is {STRING}
	
Check if caller scenario has passed the needed variable. If not passed creates a clone of a stored object.
 
# With multiple agents

Kimbo is prepared to paralelize execution of lists of tests. Maybe some tests will require a special elements or resources like smartcard, printer.... 

This requires an Agent Controller wich distribute the execution of test and receive the events of "using another agent".

The object "agent selector" identifies the attributes of the agent that can run the indirect scenario call.

Creating an agent selector

	Feature: A simple agent selector definition
	@AgentSelector  <-- will run at start
	Scenario: A simple agent selector definition
		Given add agent selector "Simple" with
		"""
		return (
			agentProperties.has("PropertyNameAux")
			&&
			(
				(agentProperties.get("PropertyNameAux").value==true)
				||
				(agentProperties.get("PropertyNameAux").value=="true")
				||
				(agentProperties.get("PropertyNameAux")==true)
				||
				(agentProperties.get("PropertyNameAux")=="true")
			)
		);
    		"""
The sample definition includes the javascript code to evaluate if an agent matchs with the selector.

The steps definitions launches an event to start execution and waits for a "finish execution" event is received

	the scenario {STRING} using another agent where
		"""
		json
		"""
Dispatch an event to execute in another agent (not discrimines) the subscenario with specified name and passing as variables the attributes of the json object.

	the scenario {STRING} using agent selector {STRING} where
		"""
		json
		"""
Dispatch an event to execute the scenario in one agent wich match with agent selector. Using passed variables in json.

	the scenario {STRING} in environment {STRING} using agent selector {STRING} where
		"""
		json
		"""

Same as above but specifies a environment previous to selection.

	the scenario {STRING} in environment {STRING} and application {STRING} using agent selector {STRING} where
		"""
		json
		"""
Same as above but specifies a environment and an APP previous to selection.
