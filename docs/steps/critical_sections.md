#Critical Sections / Exclusions

Kimbo is designed to run concurrently more than one test in a machine.

Normally, in a race conditions, the test don�t interferes each others because the desktop screen isolates big part of the execution (browser, xdotool commands, etc)

But, sometimes, the test scenarios need a shared resource like a smartcard (pcscd process) or a printer or specific file in the shared folders.

Kimbo try to prevents deadlocks but is a very complex and the test developer have to be carefull with this requeriments.

	start critical section

Raised an event to controller/host app to pause all other running Kimbos and wait for a "OK continue" message from the controller

	end critical section

Raised an event to controller/host app to free all other running Kimbos.


