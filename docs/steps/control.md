# Steps to control execution (if,loop,exception and wait)

Kimbo apports steps to conditional execution and loops. This steps are coded like another steps but Kimbo manages to execute or repeat a group of steps.

The usage of control structure steps are like in popular programming languages with the ability of include some inside others without deep limits.


**IF**

	Given if {STRING} then
	   ...
	Given else if {STRING} then
	   ...
	Given else
	   ...
	Given end if

The {STRING} in the _if then _ or _else if then_ could reference a "$varName" or a javascript expression "(#(    )#)"


**LOOPS**

All the loops in Kimbo ends with _Given end loop_

	Given loop while {STRING} do
		...
	Given end loop

Evaluate the {String} (can be a variable or a javascript expression) and execute the next steps until the result will be _false_

	Given loop for each element {STRING<-itemVariableName} in {STRING<-array} do
		...
	Given end loop

The second string is a expression that returns an array (an array variable or a javascript expression) and run the next steps for each element in the array setting the iteration item in a variable name

	loop for var {STRING} from INT to INT do
		...
	Given end loop

Execute the next steps from int to int setting the iteration value in the variable named {string}

	Given loop for var {STRING} from {STRING} to {STRING} do
		...
	Given end loop


Execute the next steps from {STRING} expression result to {STRING} expression result setting the iteration value in the variable named {string}


**EXCEPTION**

	Given throw error {STRING}

Ends the execution of the scenario throwing a exception.

**WAIT**

Sometimes the sequence need to wait a number of seconds to ensure a window is loaded or a change in the interface.

	wait_INT_seconds
	wait_STRING_seconds

This steps includes a loop of system out until reach the time specified to ensure the system not locks during the wait.
