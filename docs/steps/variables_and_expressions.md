# Steps to manage variables

The variable management engine of Kimbo creates closures in each scenario call and another one in a step call.

As in usual program language, the variables are accessible from inner closures but are deleted when the corresponding block of code is finished.

One {STRING} parameter can use a variable values and process complex expressions.

Examples:

	_{STRING}: "Hello. I�m a simple string"_
	_{STRING}: "Hello. I�m a string with a concatenated value of variable $varName" <-- it will concatenate the value stored in variable "varName"_
	_{STRING}: "$varName" <-- it will return the value stored in variable "varName"_
	_{STRING}: "(#( 'Hello. I�m a string concatenated evaluating javascript '+$varName+' more text' )#)" <-- it will evaluate the expression as javascript code replacing references to variables._


**STEPS**

	return value {STRING}

Sets the value of a special variable used to return values from subscenarios or steps.

	assign returned value to {STRING}

Assign to the variable named {string} the value of the special variable wich has the returned value of a subscenario or step call.
 
	exists variable {STRING}

Check if the variable with name exists in another closure and store true or false in the "return" internal variable

	set {STRING} to new variable {STRING}

Creates a new variable in the actual closure and sets the value or expression result.

	assign {STRING} to variable {STRING}

Sets the value or expression result to the variable name

	set {STRING} to variable {STRING}

Sets the value or expression result to the variable name

	set to new variable {STRING} multiline text
		"""
		multiline text
		"""

Sets a multiline text to the new variable 

	set to variable {STRING} multiline text
		"""
		multiline text
		"""

Sets a multiline text to the variable 

	set values {STRING} to list of variables {STRING}

uses a array of values and array of variable names and set each value (v_i) to the variable (var_i) 

	assign to variable {STRING} the clone of variable {STRING}

Clones a existing variable (simple, array or object) into a existing variable.

This clonning enables scenarios to modify attributes of an object mantaining the original object for anoder scenarios---