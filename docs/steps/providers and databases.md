#Providers

Kimbo is designed to be extended in all ways that developers need.

One line of future development is to call a database, a webservice or a rest API. All of three can be represented by an interface and the assignment of values to parameters represents a data introduction of a general test case.

To simplify this extension, Kimbo includes a provider concept. One provider is a type of object that extends from  Provider  and has a method like  .call(sMethodName,parameters) 

The sample implementation creates a three level provider extension
	Provider->database->oracledb
simplifying the creation of new databases like mariadb or mongo.

The trick is extends _ProviderListClass_ rewriting the method _newInstance(tags,jsonObject)_ to instantiate a customized object that extends _Provider_

The code is simple and self-explained

The provider base steps not appears to be very usefull and it�s better to create specified ones for diferente providers

	call to method STRING of provider type STRING with id STRING and parameters

	save in variable STRING the result of call to method STRING of provider type STRING with id STRING with parameters


**EVENTS TO THE APP/HOST**

To avoid store all database or provider credentials in a feature files (in plain text) Kimbo raised an event "GET CREDENTIALS" with the provider type and credential id as parameter to the controller/host application.

Sample event case branch in a app/host

			case "GET CREDENTIALS":
				if (received.value.id=="orclTestDirect"){
					self.sendMessage("CREDENTIALS",{
							user          : "testing",
							password      : "testing",
							connectString : "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = X.X.X.X)(PORT = 1521))(CONNECT_DATA =(SID= XE)))"
						},received);
				} else if (received.value.id=="orclTestIndirect"){ 
					self.sendMessage("CREDENTIALS",{
							indirect     : true,
							credentials  : "NOT NEEDED"
						},received);
				}
				break;

This sample puts the credential info into the case code but it must be stored in secure place (database, ciphered, etc)

The sample manages a Oracle Direct connection credentials (It returns in a send message the full connect string) with id _orclTestDirect_ and a indirect connection (wich will execute centralized in the app/host) with id _orclTestIndirect_. The indirect credentials not needs return connectionstring on user/pass to Kimbo.

When a Kimbo Provider is configured as "indirect", it will try to execute the calls in the controller/host application and get the result to the scenario. It�s done by raising a _"CALL TO PROVIDER METHOD"_ event with some parameters including _parameters_ wich is a json with call specific params.

	call("CALL TO PROVIDER METHOD",
		{
			providerType:self.providerType,
			providerId:self.id,
			providerMethod:methodId,
			parameters:parameters
		});	
					

#database provider

To simplify the creation of diferente data sources the base provider object is extended to a database provider and include the basic SQL steps.

Because the database provider is an abstract class, there is not a step to create a database provider. Extensions of database (like oracledb) adds the specific steps to create a provider at compilation time.

The test/RPA developer can create at "compilation time" diferent SQL objects to be used in runtime by name or by TAG/environment

	Feature: Simple SQL sentence
	@SQL @APP:ZZZAPP @ENV:PRE
	Scenario: Simple SQL for get list of tables 
		Given add sql id "get list of all tables in the database" with
			"""
			{
			"description":"get list of all tables in the database",
			"sql":"SELECT * FROM TABS"
			}
			"""

The sample feature adds a SQL object wich the database steps can use in all of scenarios or subscenarios.

	add sql id STRING with
		***
		json {"description":"","sql":""}
		***

Creates a object SQL. The sql string can include variables and javascript expression will be evaluated in the execution steps (not during the creation of the sql object variable)

	execute in database {STRING} the SQL {STRING}
	
Executes in the specified database (kimbo can interact with multiple databases simultaneously) the SQL identified by second string
	
	execute in database STRING the SQL STRING with
		"""
		json
		"""

Executes in the specified database (kimbo can interact with multiple databases simultaneously) the SQL identified by second string and parameters in json object

	save in variable STRING the result of execute in database STRING the SQL STRING

Executes in the specified database (kimbo can interact with multiple databases simultaneously) the SQL identified by second string.

The result is stored in the variable named with first string as an array of Maps. One map per row.

	save in variable STRING the result of execute in database STRING the SQL STRING with params

Executes in the specified database (kimbo can interact with multiple databases simultaneously) the SQL identified by second string and parameters in json object

The result is stored in the variable named with first string as an array of Maps. One map per row.

#Oracle Database provider

The oracle database provider manages the actions associated with the connection and the connection pool.

Extends _DatabaseProviderList_ and _DatabaseProvider_

	Feature: a sample of oracle database direct provider
	@Provider
	Scenario: create a sample oracle database direct provider
		Given add oracle database direct provider with id "orclTestDirect"
	
Creates a "direct" provider to a oracle database	

	Feature: a sample of oracle database direct provider
	@Provider
	Scenario: create a sample oracle database direct provider
		Given add oracle database indirect provider with id "orclTestIndirect"

Creates a "indirect" provider to a oracle database	

**Steps Note:** _ If you reuse the same id of database with different database providers (very bad way!) you can specify the usage of oracle provider in the database call steps_

	execute in oracle database STRING the SQL STRING
	execute in oracle database STRING the SQL STRING with
	save in variable STRING the result of execute in oracle database STRING the SQL STRING
	save in variable STRING the result of execute in oracle database STRING the SQL STRING with params
