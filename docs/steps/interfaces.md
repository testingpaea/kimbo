#INTERFACES

The interfaces steps provides the low level interaction functionality.
The interface definitions are loaded on compile time and the program can�t change them in runtime.

	Feature: Example of interface definition
	@Interface @APP:ZZZAPP
	Scenario: Example of interface definition
		Given add "selenium" interface "first interface example" with 
		"""
		{
		    "testLoaded": {
		        "element": "ElementName",
		        "testWay": "VISIBLE"
		    },
		    "elements": [
		        {
		            "id": "ElementName",
		            "selector": {
		                "type": "xpath",
		                "expression": "//*[@id='connection']/div[1]/div[1]/a[1]",
		                "frameNumbers": []
		            },
		            "focusable": true,
		            "caption": "Example",
		            "HTMLtag": "a",
		            "innerHTML": "Example"
		        }
		    ]
		}
		"""
This example creates a web (**Selenium**) interface with id **"first interface example"** for app **ZZZAPP**.

When the scenario needs to wait for load, the system will check the "testLoaded" attribute of json where identifies a element id **"ElementName"** and a kind of comprobation (testway) **"VISIBLE"**.

The detail to access to this element or another element in the interface are in the array **"elements"** of the json.

Each element in this array has at least one id, one selector (or a way to focus the element), and info about if element is focusable and order of focus.

	add {STRING} interface {STRING} with
		"""
		json
		"""
Create a interface described above

	wait for load of interface {STRING}
	
Wait for load of specified interface. When the load test matchs the actual interface changes to specified interface.

	click on {STRING}
	
Click on element id of actual interface

	click on {STRING} and wait for load of interface {STRING}
	
Click on element id of actual interface and wait for load another interface

	click on {STRING} non blocking

Click on element of actual interface but not block the scenario execution... another step will block or wait.

	click on {STRING} non blocking and wait for load of interface {STRING}

Click on element of actual interface but not block the scenario execution... 
The wait interface will block the scenario 
	
	write {STRING} in {STRING}

Write text in specified element of actual interface

	press arrow {STRING} in {STRING}
Send arrow (UP,DOWN,LEFT,RIGHT) to specified element of actual interface

	press arrow {STRING} {STRING} times in {STRING}
	
Send arrow (UP,DOWN,LEFT,RIGHT) multiple times to specified element of actual interface

	press enter in {STRING}

Press ENTER key in specified element of actual interface

	press enter {STRING} times in {STRING}
Press ENTER key multiple times in specified element of actual interface

	select item number {STRING} of {STRING}
Select item by number in a selector element of actual interface

	select {STRING} option of {STRING}
Select item by option value in a selector element of actual interface

	select {STRING} of {STRING}
Select item by text in a selector element of actual interface

	get value of element {STRING}
set return value with the value of a specified element of actual interface

	is visible element {STRING}
set return value with the "visible" attribute of a specified element of actual interface

	is visible element {STRING} with same caption
set return value with true if the "visible" attribute is true and the caption is equal to the attribute in element definition of a specified element of actual interface

