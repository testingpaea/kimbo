#Command line steps

The Kimbo engines can run CLI commands. It�s not the recommended way to build a bunch of test but, sometimes, it�s very usefull to include in one scenario a system call.

	execute in command line {STRING}
	
Executes the command passed in the string
