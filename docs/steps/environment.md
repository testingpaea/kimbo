# Environment variables or tag management

Scenarios, Interfaces and Data Objects has tags in the definition scenarios

	Feature: some interface definition
	@Interface @APP:XXXAPP @ENV:PREPRODUCTION
	Scenario: some interface definition
	Given add "selenium" interface "interface name to use in scenario executions" with 
		"""
		json
		"""

	@subScenario @APP:YYYAPP @ENV:DEVELOPMENT
	Scenario: subscenario name to be called
	Given ....

	@DataObject @APP:ZZZAPP @ENV:PREPRODUCTION
	Scenario: A Dataobject definition
	Given add object "ObjectType" with id "Identificator" and values from json
		"""
		json
		"""
	
Usually the tags APP and ENV are common to all testing applications but Kimbo can manage unlimited kind of tags in format  @TAG:VALUE 

When a scenario is running some tags are setted. If no tags are setted in runtime, Kimbo will select the first interface, scenario or object in the list with the same name or identifier.

If some tags are setted, kimbo will take a list of interfaces, scenarios or objects with the providen name and match the list of tags assigned to each elements with the list of active tags. Kimbo will remove all elements with same tag name but diferent value and will select the element with more tags with same name and value.

Uses the variable and expression engine to evaluate al parameters passed to the steps.

	in app {STRING}

Sets the name of the application actually running. This value will be checked versus "@APP" tag value in the elements.
 
Useful when browser opens a system window to select a file to upload or to set the smartcard PIN or another app via xdg.

The app list is a stack. "in app" step pushes a new value at the top of the stack.

	out of actual app

Removes the top application name value and activates the new top value in the stack.

	out of app {STRING}

Removes all top application names in the stack include the app name passed in {STRING}

	in environment {STRING}

Sets the environment (@ENV) value. This is not an stack. Usually the environment is only setted at start top level scenario/test

	in app {STRING} and environment {STRING}

Push a app name and sets the environment value

	in app {STRING} and environment {STRING} and tags
		"""
		json
		"""
Push a app name, sets the environment value and set a list of other custom tags.

	in app {STRING} and tags

Push a app name and set a list of other custom tags.

	add tag {STRING} with value {STRING}
	
Set a new tag with a specified value 

	set {STRING} to application attribute {STRING}

Sets or replace a value of the application attribute

	set application attributes
		"""
		json
		""
Sets a list of application attributes
 





	