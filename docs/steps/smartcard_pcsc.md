#PCSCD - SMARTCARD READERS

Testing web apps that uses x509v3 certs to login sometimes needs the ability of use browser cert storage and smartcard cert storages.

	enable smartcard and reload browser

Locks the system to ensure one test running simultaneously, run _"service start pcscd"_ and reload the selenium controlled browser (the browser needs to be restarted to "detect" the smartcard key storage)

The system getting locked after this step execution to enable the scenarios to execute another steps with the smartcard (like write the pin)

The release the system lock, the scenario have to call one of the "free" steps: 
	
	Given end smartcard use section
	Given free smartcard system lock

This two steps executes the same "free system lock" process, are synonymous, to better scenario descriptions

	disable smartcard and reload browser

If the scenario will use a browser key storage it have to disable the smartcard (or pcscd will ask for the card PIN) and reload the browser to ensure the smartcard key store is removed in browser.

The system getting locked after this step execution to enable the scenarios to execute another steps without the smartcard (like write the browser key store pin)

The scenario have to end or free the smartcard lock.

	enable smartcard

runs service pcscd start equivalent

	disable smartcard

runs service pcscd stop equivalent

