#Object Variables

Kimbo can manage object variables like a structured types to contains multiple attributes

The object variables can be "dynamic" or "storable".

A Dynamic object is created by the scenario calling a  "create a new empty object variable"  step. The scenario have to add the attributes and values.

A Storable object is created in "compiling time" with a @dataObject scenario definition like

	@DataObject @APP:ZZZAPP @ENV:PREPRODUCTION
	Scenario: A Dataobject definition
	Given add object "ObjectType" with id "Identificator" and values from json
	    """
    	    json
    	    """
 
It will create a list of objects of typ "ObjectType" and create one with id "Identificator" and take the attributes from the passed json.

The usage of a dynamics or storable objects is equal. 

	create a new empty object variable {STRING}

Creates a new empty dynamic object variable with specified name without type and initial attributes

	add storable object {STRING} with 
		"""
		json
		"""

Creates a storable object of type specified in first string and take the attribute values from json

The json includes an id for the new object

	add object {STRING} with id {STRING} and values from json
	     """
	     json
	     """
Creates a storable object of type passed in first String, id specified in the second strings and fill the attributes with the json

	add object {STRING} with id {STRING} as extend of id {STRING} and values from json
	     """
	     json
	     """
Creates a new storable object of type passed in first String, id specified in the second strings and fill the attributes with the ones in the object with id third string and the specified json

	add object {STRING} as extend of id {STRING} and values from json
	     """
	     json
	     """
Creates a new storable object of type passed in first String and fill the attributes with the ones in the object with id second string and the specified json.

The json includes an id for the new object

	assign {STRING} to the object attribute {STRING} of var {STRING}

This step gets a variable named (third string) that is an object and sets or create a attribute with name (second string) with the value specified in the first string.
 
	push {STRING} to the object array attribute {STRING} of var {STRING}

Same of above but the attribute is an array ([]) and the specified value will be pushed into.	
	
	extract attributes of {STRING}
	
Create local variables for all attributes of a object variable with specified name. Each local variable will have the name and the value of each attribute of the object specified

	assign to variable {STRING} the clone object {STRING} which id is {STRING}

Create a new object clonning all the attributes of the source (type and id) and assign the cloned to a variable.

The scenarios can modify a clone mantaining in the source the initial/compile time values that will be used for another test scenarios.

	assign to variable {STRING} the clone object {STRING} which {STRING} is {STRING}

Same of above but select the source object by another attribute diferent of ID.

	assign to variable {STRING} the value of object attribute {STRING} of var {STRING}

Assign to a variable with name specified the value of one attribute of object variable 
 
	assign to variable {STRING} the value of object function {STRING} of var {STRING}

Execute a object method and assign the result to a variable

	assign to variable {STRING} the value of object {STRING} attribute {STRING} which id is {STRING}
	
Assign to a variable with name specified the value of one attribute of an storable object identified by type and id

	assign to variable {STRING} the value of object {STRING} attribute {STRING} which {STRING} is {STRING}

Assign to a variable with name specified the value of one attribute of an storable object identified by type and another attribute is equal to passed value.
	
	assign to variable {STRING} the result of object {STRING} function {STRING} which id is {STRING}

Assign to a variable with name specified the result of the execution of one method of an storable object identified by type and id

	assign to variable {STRING} the result of object {STRING} function {STRING} which {STRING} is {STRING}
Assign to a variable with name specified the result of the execution of one method of an storable object identified by type and another attribute is equal to passed value.
