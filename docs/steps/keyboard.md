#KEYBOARD

Kimbo is focused in the sequence->interface->data model but sometimes is necesary to send some keys without especify one element of interface.

Actually the keyboard steps uses xdotool to send the keys to one desktop or another.

Do not abuse of the "pure keyboard approach" or your tests will be very difficult of maintain.

	send enter

Sends a ENTER key to active window without select window or check what window is active actually

	arrow {STRING}

Send a arrow key (UP, DOWN, LEFT, RIGHT) to the active window

	arrow {STRING} repeat {STRING} times

Send a arrow key (UP, DOWN, LEFT, RIGHT) to the active window the number of times as result of the repeat expresion 

	send keys {STRING}

Send a custom sequence of keys 