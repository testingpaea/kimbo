#Coding tools

Kimbo includes a little group of steps to help developers to code a scenarios

	debugger
	
Executes a javascript "debugger" instruction in the nodejs that�s is running kimbo.

	show trace {STRING}

Sends the {STRING} to the console.log output stream.

	show closures

Shows the contents of the closures tree (including the internal closures) to locate variables in one execution point.

	show the execution tree whith this test {STRING}

Shows the tree with the scenario-subscenario-steps names

	