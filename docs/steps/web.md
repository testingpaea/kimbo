#Web Browser steps

The Kimbo is prepared to manage a Selenium driver and can interact with the browser that selenium controls

	launch browser to {STRING} maximized

Starts a browser (chromium-browser), maximize, and load the specified url.

	goto url {STRING}

Change the url in then browser 

	goto url {STRING} and wait for load of interface {STRING}
	
Change the url in the browser and wait for load the specified interface.

	get url param {STRING}
	
Assign to the return special variable the value of specified param in the actual browser loaded url

	close browser
	
Close the selenium managed browser	
	
	close all browsers

Close all the browser loaded in the computer (pkill chrome)