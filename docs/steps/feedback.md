# Feedback. Scenario shows information or ask for info to the test user

Kimbo is designed to run the test unattended but sometimes (specially during the coding of test) its good idea to show some info to the user or register the info in the test log to be readed later.

In other scenarios like to use a personal smartcard key store the PIN can�t be writted in plain text in the scenario.

	user input window {STRING} to variable {STRING}
	
Show a "zenity" (https://en.wikipedia.org/wiki/Zenity) inputbox where the user can write a text that will be stored in the named variable

Kimbo raised a "inputBox" event to the controller/host application to manage another user interface for show a different way to input text.

	user input password window {STRING} to variable {STRING}

Show a "zenity" (https://en.wikipedia.org/wiki/Zenity) "password inputbox" where the user can write a text (hidden with ***) that will be stored in the named variable

Kimbo raised a "inputBox" event (with password param set to _true_) to the controller/host application to manage another user interface for show a different way to input password value.

	wait INT seconds showing {STRING}
	
Generates a wait cycle (defined in _control.md_) showing a message window with a text "string expression".

The step will be registered in the scenario log/analisys data so it�s very useful to do this information wait in specific parts of scenario.
	
	show progress box {STRING} for INT seconds

Same of above (its a synonymous) generates a wait cycle (defined in _control.md_) showing a message window with a text "string expression".

The type of the window is "progress box" to show a progress bar but it isn�t manage the progress percent only a "changing" progress because there is not so much control of the progress activity.

The step will be registered in the scenario log/analisys data so it�s very useful to do this information wait in specific parts of scenario.

	show progress box {STRING}

A unblocking generation of a message window with the "string expression".

The type of the window is "progress box" to show a progress bar but it isn�t manage the progress percent only a "changing" progress because there is not so much control of the progress activity.

The step is unblocking, so the scenario will be stay running next steps. Be careful with the interaction action if the message windows is at the top of the desktop. 

	close progress box

Closes the active progress box (window). 

The type of the window is "progress box" to show a progress bar but it isn�t manage the progress percent only a "changing" progress because there is not so much control of the progress activity.

