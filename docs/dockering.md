#Docker model (evolution)

The "agents node controller" may be easy replicated using docker to multiply the number of concurrent execution test threads.

With docker technology the testing system can be distributed along a multiple machines that host one or more agent node containers. 

https://hub.docker.com/r/saearagon/ubuntuvncnodejs includes all elements to launch (_executing "nodejs main.js"_) a app stored in /usr/src/app (this path may be shared by a host as a _volume_)

With this model it�s needed a global controller that manages all the agent nodes. This global controller acts as dispatcher of Test to the different individual agents and store the results of the execution of each test.