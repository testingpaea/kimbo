const ExecutionNode=require('./executionNodes');
const StepFunctionsStore=require('./stepFunctionsStore');
const { execSync } = require( 'child_process' );
const {resolve} = require('path');


var ScenarioStore = class ScenarioStore {
	constructor(){
		var self=this;
		self.agentName="";
		self.agentProperties=new Map();
		self.world="";
		self.mapScenarios=new Map();
		self.nTests=0;
		self.scenarios={};
		self.executionTree=[];
		self.actExecutionNode="";
		self.ifStackLevel=0;
		self.ifStackLevelRunning=0;
		self.loopStackLevel=0;
		self.loopStackLevelRunning=0;
		self.scenarioSelectorHook=undefined;
		self.mapNoRootTags=new Map();
	}
	capitalize(sText){
		return sText.charAt(0).toUpperCase() + sText.substr(1);
	}
	removeSpecialChars(sText){
		if (typeof sText==="undefined") return sText;
		var noSpecialChars = sText.replace(/[^a-zA-Z0-9 ]/g, '');
		return noSpecialChars;
	} 
	enableExtension(extension){
		var self=this;
		debugger;
		var extensionName;
		var extensionPath;
		if (typeof extension==="string"){
			extensionName=extension;
			extensionPath='./extensions/'+extension;
		} else {
			extensionName=extension.name;
			extensionPath=extension.path;
		}
		var srcRealPath=resolve(extensionPath);
		//console.log("extension "+extensionName+" path:"+srcRealPath);
		var arrParts=extensionName.split("/");
		var extReducedName=arrParts[arrParts.length-1];
		if (self.isExtensionLoaded(extReducedName)) {
			return;
		}
		self[extReducedName+"ExtensionEnabled"]=true;
		//console.log("Adding "+extensionName+" Extensions:"+self[extensionName+"ExtensionEnabled"]);
		var sHelper = require('./stepHelper.lib');
		//console.log("path:"+extensionName+" reduced:"+extReducedName);
		var newExtensionPath=extensionPath+'/'+extReducedName+'_extension.steps';
		srcRealPath=resolve(newExtensionPath);
		//console.log("extension "+extensionName+" new path:"+srcRealPath + " reduced:"+newExtensionPath);
		var arrExtSteps=require(newExtensionPath);
		StepFunctionsStore.stepDefinitions["Cucumber_"+extReducedName+"ExtensionSteps"]=arrExtSteps;
		var lstAux=sHelper.toStepFunctions(arrExtSteps);
		var arrAux=Object.getOwnPropertyNames(lstAux);
		for (const stepFncId of arrAux){
			StepFunctionsStore.stepFunctions[stepFncId]=lstAux[stepFncId];
		};
		var extensionDriver=require(extensionPath+'/'+extReducedName+'_extension.lib');
		var captExtensionName = self.capitalize(extReducedName);
		self['get'+captExtensionName]=function(){
				//console.log("Getting:"+extensionName);
				return extensionDriver;
		};
	}
	getRootScenarios(){
		
		var self=this;
		var arrResult=[];
		var selected=self.getSelected();
		self.mapScenarios.forEach(function(value,key,map){
			if (value.length>0){
				var scenario=value[0];
				var scnName=scenario.name;
				var tags=scenario.tags;
				var paramsTag="@PARAMS:";
				var params=[];
				var vSelector=selected.getSelectorTagValue(tags);
				if (vSelector!==""){
					var isRoot=true;
					for (var i=0;isRoot && (i<tags.length);i++){
						var theTag=tags[i];
						var theTagName=theTag.name;
						if (self.mapNoRootTags.has(theTagName)){
							isRoot=false;
						} else {
							if (theTagName.substring(0,paramsTag.length)==paramsTag){
								var sParams=theTagName.substring(paramsTag.length,theTagName.length);
								var paramsAux=JSON.parse(sParams);
								if (Array.isArray(paramsAux)){
									for (const param of paramsAux){
										params.push(param);
									};
								} else {
									params.push(paramsAux);
								}
							}
						}
					}
					if (isRoot){
						arrResult.push({key:vSelector,name:scnName,params:params});
					}
				}
			}
		});
		return arrResult;
	}
	setNotRootUserTag(sTag,sId){
		var {After, Before} = require("@cucumber/cucumber");	
		const OtherWorld = require('kimbo/OtherWorld');
		var self=this;
		self.mapNoRootTags.set(sTag,sId);
		Before(sTag,function (theScenario,callback) {
		//	console.log("It is a User");
			OtherWorld.forceExecution=true;
			return callback();
		});
		After(sTag,function (theScenario,callback) {
		//	console.log("Close User");
			OtherWorld.forceExecution=false;
			return callback();
		})
	}
	enableExtensions(arrExtensions){
		var self=this;
		self.enableExtension("internal/closures");
		if (typeof arrExtensions==="undefined") return;
		for (const extension of arrExtensions){
			self.enableExtension(extension);
		};
	}
	isExtensionLoaded(extension){
		var self=this;
		var captExtensionName = self.capitalize(extension);
//		console.log("Locating cpt extension:"+captExtensionName);
		return (typeof self['get'+captExtensionName]!=="undefined");
	}
	getManager(extension){
		var self=this;
		var captExtensionName = self.capitalize(extension);
		return self['get'+captExtensionName]();
	}
	setWorld(world){
		this.world=world;
	}
	saveActualScenario(){
		var self=this;
		var world=self.world;
		var sScenarioId=world.currentScenarioName;
		for (const tag of world.allTags){
			sScenarioId+="#"+tag.name;
		};
		var actScenario={
			id:sScenarioId,
			tag:world.currentScenarioTag,
			tags:world.allTags,
			name:world.currentScenarioName,
			srcScenario:world.currentScenario,
			functionList: world.functionList, 
			functionTimeout: world.functionTimeout,
			};
		actScenario.clone=function clone(){
			var self=this;
			var newObj={};
			newObj.id=self.id;
			newObj.tag=self.tag;
			newObj.tags=self.allTags;
			newObj.name=self.name;
			newObj.srcScenario=self.srcScenario;
			newObj.functionList=self.functionList;
			newObj.functionTimeout=self.functionTimeout;
			newObj.srcAgent=self.srcAgent;
			newObj.inAgent=self.inAgent;
			newObj.executionId=self.executionId;
			newObj.planExecutionId=self.planExecutionId;
			newObj.clone=self.clone;
			return newObj;
		}

		self.scenarios[sScenarioId] = actScenario;
		//console.log("Saved scenario:"+sScenarioId+" values:"+JSON.stringify(self.scenarios[sScenarioId]));
		if (!self.mapScenarios.has(actScenario.name)){
			self.mapScenarios.set(actScenario.name,[]);
		}
		var arrScenarios=self.mapScenarios.get(actScenario.name);
		//console.log("Saving in selectable if extension loaded:"+self.isExtensionLoaded("selected"));
		if (self.isExtensionLoaded("selected")){
			var selector=self.getSelected();
			//console.log("DYNAMIC SCENARIO SELECTOR:"+selector.tagSelector);
			var tagValue=selector.getSelectorTagValue(world.allTags);
			debugger;
			if (tagValue!==""){
				//console.log("DYNAMIC SCENARIO CALLER:"+tagValue);
				selector.mapScenariosByTag.set(tagValue,actScenario);
			}
		}
		arrScenarios.push(actScenario);
	}
	
	setScenarioSelectorHook(fncTagGetter){
		this.scenarioSelectorHook=fncTagGetter;
	}
	getScenario(name){
		//console.log("getScenario:"+name);
		var selectedScenario=this.getBestMatch(name,this.mapScenarios);
		var result="";
		try {
			selectedScenario=this.getBestMatch(name,this.mapScenarios);
			result=selectedScenario.id;
		} catch (err){
			console.log("Error. Scenario no Encontrado:'"+name+"' in list of scenarios:");
			for (const mapKey of this.mapScenarios.keys()) {
				console.log("'"+mapKey+"'");
			}
			
		}
		return result;
	}
	getSelectorItems(){
		return [];
	}
	getBestMatch(itemId,theList){
		//console.log("getBestMatch:"+itemId+" - " + theList);
		var self=this;
		if ((typeof itemId==="undefined")&&(typeof theList==="undefined")) return undefined;
		var tagSelectors=self.getSelectorItems();
		//console.log("selectors:"+JSON.stringify(tagSelectors));
		//var arrProperties=Object.getOwnPropertyNames(self.scenarios);
		//console.log("Search for ("+name+") properties names array"+JSON.stringify(arrProperties));
		var points=0;
		var maxPoints=0;
		var bestMatch="";
		var arrSameItems;
		if (Array.isArray(itemId)){
			arrSameItems=itemId;
		} else if (typeof theList!=="undefined"){
			if (Array.isArray(theList)){
				arrSameItems=theList;
			} else if (theList.has(itemId)){
				arrSameItems=theList.get(itemId);
			}
		} else {
			arrSameItems=[];
		}
		for (const oItem of arrSameItems){
			var bSkip=false;
			var points=0;
			//console.log("Options "+JSON.stringify(oItem.tags));
			for (var k=0;(k<oItem.tags.length)&&(!bSkip);k++){
				var tag=oItem.tags[k];
				var tagName=tag.name;
				var tagValue=tag.value;
				if (tagName.indexOf(":")>0){
					var tagParts=tagName.split(":");
					tagName=tagParts[0];
					tagValue=tagParts[1];
				}
				//console.log("Accumulated Points:"+ points +" actual Option:[" + tagName+","+tagValue+"]");
				for (var j=0;(j<tagSelectors.length)&&(!bSkip);j++){
					var tagSelector=tagSelectors[j];
					var tagselName=tagSelector.name;
					var tagselValue=tagSelector.value;
					//console.log("Versus "+JSON.stringify(tagSelector));
					if (tagName==tagselName){
						//console.log("Same name "+tagName);
						if ((typeof tagselValue=="undefined")||(tagValue==tagselValue)){
							//console.log("Same value");
							points++;
						} else {
							//console.log("Diferent value");
							bSkip=true;
						}
					}
				}
			};
			if (!bSkip){
				if ((bestMatch==="")||(maxPoints<points)){
					maxPoints=points;
					bestMatch=oItem;
				}
			}
		};
		if (bestMatch==="") return undefined;
		return bestMatch;
	}

	getExecutionInfo(){
		var self=this;
		var arrResult=[];
		for (const node of self.executionTree){
			arrResult.push(node.toJSON());
		};
		return arrResult;
	}
	getExecutionExtractInfo(maxLevel){
		var self=this;
		var arrResult=[];
		for (const node of self.executionTree){
			arrResult.push(node.toJSON(0,maxLevel));
		}
		return arrResult;
	}
	clearExecutionTree(){
		var self=this;
		self.executionTree=[];
	}
	async clearExecutionPersistance(){
		var newEN=new ExecutionNode(theName,params);
		newEN.ScStore=self;
		newEN.clearPersistance(); 
	}
	newExecutionNode(name,params){
		var self=this;
		//console.log("New Execution Node "+name);
		var bIsCompiling=self.world.getCompiling();
		if (!bIsCompiling){
			
			debugger;
			var theName=name;
			if (typeof name==="undefined"){
				theName="unnamed";
			} else if (typeof name==="string"){
				theName=name;
			} else {
				theName=name.id;
			}			
			var theName=self.processExpressionSync(theName)+" - " +theName;
			var newEN=new ExecutionNode(theName,params);
			newEN.ScStore=self; 
			if (self.actExecutionNode===""){
				self.executionTree.push(newEN);
				self.actExecutionNode=newEN;
			} else {
				self.actExecutionNode.addChild(newEN);
				self.actExecutionNode=newEN;
			}
			return newEN;
		}
	}
	doneExecutionNode(){
		var self=this;
		var bIsCompiling=self.world.getCompiling();
		if ((!bIsCompiling) && (self.actExecutionNode!=="")){
			//console.log("Done Execution Node "+self.actExecutionNode.name);
			self.actExecutionNode.done();
			self.actExecutionNode=self.actExecutionNode.parent;
		}
	}
	changeExecutionState(newState){
		var self=this;
		var bIsCompiling=self.world.getCompiling();
		if ((!bIsCompiling) && (self.actExecutionNode!=="")){
			self.actExecutionNode.setState(newState);
		}
	}
	ensureVNC(){
		var self=this;
		console.log("Ensuring vnc loaded");
		var result=false;
		if (self.isExtensionLoaded("multiscreen")&&self.getMultiscreen().enabled){
			var result=self.getMultiscreen().ensureVNC();
			console.log("Is the VNC loaded: " + result);
		} else {
			console.log("multiscreen extension is not loaded");
		}
		return result;
	}
	async inputWindow(sText,bPassword){
		var self=this;
		var result="";
		if (self.isExtensionLoaded("multiscreen")&&self.getMultiscreen().enabled){
			result=await self.getMultiscreen().inputWindow(sText,bPassword);
		} else {
			var sCommand='zenity --entry --text="'+sText+'"';
			var sResult="";
			if ((typeof bPassword!=="undefined")&&bPassword){
				sCommand+=" --hide-text";
			}
			try {
				sResult= execSync (sCommand);
			} catch(error){
				debugger;
				console.log(error);
			}
			result=sResult.toString('utf8').trim();
		}
	//	console.log("Paramenters:"+parameters+" Typeof Result:"+typeof sResult + " json:"+JSON.stringify(sResult));
		return result;
	}
	async executeInCommandLine(sCommand){
		var result="";
		try {
			result=execSync(sCommand);
		} catch (error){
			debugger;
			console.log(error);
			result=error;
		}
		var sResult=result.toString('utf8');
		return sResult;
		
	}


	
	async waitSecs(nSecs,tickCallback){
//		console.log("Wait "+nSecs+" secs start");
		var self=this;
		var nAuxSecs=3000;
		if (typeof nSecs!=="undefined"){
			if (typeof nSecs!=="string"){
				nAuxSecs=nSecs*1000;
			} else {
				nAuxSecs=parseFloat(nSecs)*1000;
			}
		} else {
				nAuxSecs=3*1000;
		}
		var initTime=(new Date()).getTime();
		var endTime=initTime+nAuxSecs;
		var delay = function(ms){
			return new Promise(function(res){
				//console.log("Promise:" + ms+" Seconds");
				setTimeout(res,ms);
				//console.log("Promise with timeout:" + ms+" Seconds");
			});
		} 
		while ((new Date()).getTime()<endTime){
			var difTime=endTime-((new Date()).getTime());
			//console.log("DifTime:"+difTime);
			var nextWait=1000;
			if (nextWait>difTime){
				nextWait=difTime;
			}
//			console.log("Wait:"+nextWait + " typeof:"+(typeof nextWait));
			if (typeof tickCallback!=="undefined"){
				try {
					await tickCallback(difTime,nSecs-difTime,nSecs);
				} catch (theError) {
					console.log("Error calling tickCallback");
					console.log(theError);
				}
			}
//			execSync ( 'sleep '+(nextWait/1000));
			try {
				await delay(nextWait);
			} catch (theError){
				console.log("Error in Delay:...."+theError);
			}
		}
//		console.log("Wait secs end");
	}
	traceClosures(){
		var self=this;
		var iDeep=0;
		console.log("Scenario");
		console.log("========");
		for (const closure of self.getClosures().closures){
			closure.trace(iDeep);
			iDeep++;
		};
		console.log("Internal");
		console.log("========");
		iDeep=0;
		for (const closure of self.getClosures().runtimeClosures){
			closure.trace(iDeep);
			iDeep++;
		};
	}
	traceExecutionTree(){
		var self=this;
		console.log("");
		console.log("");
		console.log("");
		console.log("EXECUTION TREE");
		console.log("==============");
		var sResult="";
		for (const en of self.executionTree){
			sResult+="\n\n\n" + "EXECUTION TREE";
			sResult+="\n" + "==============";
			sResult+="\n"+en.trace();
		};
		const fs = require('fs'); 
		try { 
			fs.writeFileSync('/usr/src/app/traceExecutionTree.log', sResult); 
			console.log("File has been saved."); 
		} catch (error) { 
			console.error(err); 
		} 
	}
	async processExpression(inputText){
		var self=this;
		if (!self.isExtensionLoaded("expressions")){
			self.enableExtension("expressions");
		}
		return await self.getExpressions().processExpression(inputText);
	}
	processExpressionSync(inputText){
		var self=this;
		if (!self.isExtensionLoaded("expressions")){
			self.enableExtension("expressions");
		}
		return self.getExpressions().processExpressionSync(inputText);
	}
	
	getActualTags(){
		var objTags=[];
		var self=this;
		for (const tag of self.world.allTags){
			//console.log(JSON.stringify(tag));
			//console.log(tag.name.split(":"));
			var arrTagParts=tag.name.split(":");
			if (Array.isArray(arrTagParts)&&(arrTagParts.length==2)){
				var tagName=arrTagParts[0];
				var tagValue=arrTagParts[1];
				//console.log(JSON.stringify(objTags)+" Tag "+tagName +" value:"+tagValue);
				objTags.push({name:tagName,value:tagValue});
			}
		};
		return objTags;
	}
	

	async callScenario(scenarioName,theVariables){
		var self=this;
		//console.log("Run the scenario:"+JSON.stringify(scenarioName));
		var selector=self.getScenario(scenarioName);
		var srcScenario=self.scenarios[selector];
		var scenario=srcScenario.clone();
		console.log("SCENARIO:"+JSON.stringify(scenario));
		var closures=self.getClosures();
		closures.newClosure("Scenario "+ scenarioName + " Parameters");
		if (typeof theVariables!=="undefined"){
			var varProperties=Object.getOwnPropertyNames(theVariables);
			for (const theVar of varProperties){
				closures.addVar(theVar,theVariables[theVar]);
			};
		}
		await self.indirectScenarioCall(scenario); // this method was created by reuse-cucumber....js
		closures.popClosure();
	}
}
var scenarioStore=new ScenarioStore();
module.exports = scenarioStore;
